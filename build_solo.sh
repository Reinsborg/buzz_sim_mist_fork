#!/bin/bash

export EXCLUDED_PACKAGES="dji_sdk_web_groundstation;dji_sdk_read_cam;dji_sdk_lib;dji_sdk_dji2mav;dji_sdk_demo;dji_sdk;dji_sdk_mistlab"

./build.sh "$@"
