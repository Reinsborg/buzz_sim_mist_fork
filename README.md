# ROS/GAZEBO/BUZZ

## Overview
Drone swarm simulation using _ROS/GAZEBO/BUZZ_ __(RGB)__ framework on Ubuntu

## Local installation for Gazebo/buzz/djiros
*tested with Ubuntu 18.04 and ROS Melodic*

1- Install ROS **full-desktop** properly. See the [tutorial](http://wiki.ros.org/melodic/Installation/Ubuntu). Do not forget `source /opt/ros/melodic/setup.bash` !

2- Run the following command to download the required packages
```
sudo apt-get install ros-<ros distro>-mavros*  python-jinja2 ros-<ros distro>-geographic-* libjson-glib-dev
```
where you replace `<ros distro>` with your ROS distribution (tested with Melodic, Kinetic and Indigo).

3 - Install the hector_quadrorotor packages 
From package manager (Melodic)
```
sudo apt-get install ros-<ros distro>-hector-*
```

4- Install [buzz](http://the.swarming.buzz/wiki/doku.php?id=start) globally: 
```
git clone https://github.com/MISTLab/Buzz.git
cd Buzz
mkdir build
cd build
cmake ../src
make
sudo make install
```
On linux, also do: `sudo ldconfig`

4- If you need to simulate the 3DR Solos, install DroneKit-SITL:
```
sudo -H pip install dronekit-sitl -UI
```
**TODO**: Still need to be tested together with ROSBuzz. (crashed 11/08/2017 with solo_gazebo_wbuzz.launch)

5- Install this repository in your home directory:
```
git clone -b sim http://git.mistlab.ca/dasto/drones.git ROS_WS
cd ROS_WS
./add_remotes.sh
```
Export the required Gazebo models and build:
```
source src/mistlab_rosbuzz_sdk/exportmodels.sh
./build_sim.sh
source devel/setup.bash
source src/rosbuzz/misc/cmdlinectr.sh
```
The `source` command lines can also be copied in your ~/.bashrc to make it permanent:
```
echo "source ~/ROS_WS/src/mistlab_rosbuzz_sdk/exportmodels.sh" >> ~/.bashrc
echo "source ~/ROS_WS/devel/setup.bash" >> ~/.bashrc
echo "source ~/ROS_WS/src/rosbuzz/misc/cmdlinectr.sh" >> ~/.bashrc
```
(under the assumption you cloned this repository as "ROS_WS" in your home "~")

## Example
To launch an **example**, run `roslaunch dji_sdk_mistlab startswarmsim.launch` in your terminal. In this example, you will be able to see drones in a default environment. Open up a new terminal window and run `takeoff robot2`, for example, to see drone #2 takeoff. You can run other commands such as `land`, `arm` and `disarm`. The simulator can also be controlled using the [WebControl interface](https://git.mistlab.ca/dasto/drones/tree/sim_melodic/src/webcontrol).

# Additional Information


## Build Command
```
build_sim.sh
```

## Build Options
* -d or --debug: compile in debug
* -c or --clean: delete build folder before compiling
* -t or --test: run the tests
* -cr or --checkrepo: check if the repo is up to date

## Map visualization
Install rviz:
```
sudo apt-get install ros-melodic-rviz
```
then launch it and subscribe to /robotX/grid map topic.

## Post Git Clone
To set your git subtree working environment, cd into your workspace and run the add_remotes.sh script:   
```
chnod +x add_remotes.sh
./add_remotes.sh
```

## Git Subtree
This repository uses git subtree to manage the sub repositories.   

### Adding a sub-repository
To add a sub-repository located at sub_repo_url in the folder src/my_folder (my_folder should not exist), enter the following commands:     

```
git remote add sub_repo_name sub_repo_url    
git subtree add --prefix=src/my_folder sub_repo_name sub_repo_branch
```

To make it easier to know the name of the sub-repository, the sub-repository name is used as the folder name.   

### Pulling change from sub-repository 
```
git subtree pull --prefix=src/my_folder sub_repo_name sub_repo_branch
```

### Pushing change to sub-repository
```
git subtree push --prefix=src/my_folder sub_repo_name sub_repo_branch
```


