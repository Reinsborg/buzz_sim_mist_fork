#!/bin/bash

TEST=NO
DEBUG=NO
SIM=0
CLEAN=NO
BUILD=YES
OMPL=0

if rosversion -d | grep -q 'indigo'; then
   echo "Compiling for INDIGO"
   KINETIC=0
else
   KINETIC=1
fi

BRANCH=ros_drones_ws


#####################################
########## Checking Repo  ###########

function check_repo(){

    git fetch origin $BRANCH
    reslog=$(git log HEAD..origin/$BRANCH --oneline)
    if [[ "${reslog}" != "" ]] 
    then
        echo "Pull needed, updating local branch"
        git merge origin/$BRANCH
    else
        echo "Nothing to build"
        BUILD=NO
    fi

    exit
}

#####################################
######## Building Software ##########

function build_software(){

    echo ""
    echo "Build all with the following parameters"
    echo "Tests enabled: $TEST"
    echo "Debug enabled: $DEBUG"
    echo "Clean before building: $CLEAN"

    # Clean build folder if required
    if [ "$CLEAN" == "YES" ] 
    then 
        rm -rf build 
    fi

    # Main make
    if [ "$DEBUG" == "YES" ] 
    then 
    catkin_make -j1 -DCATKIN_BLACKLIST_PACKAGES=$EXCLUDED_PACKAGES -DSIM=$SIM -DKIN=$KINETIC -DCMAKE_BUILD_TYPE=Debug -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DOMPL_FOUND=$OMPL
    else
    catkin_make -j1 -DCATKIN_BLACKLIST_PACKAGES=$EXCLUDED_PACKAGES -DSIM=$SIM -DKIN=$KINETIC -DOMPL_FOUND=$OMPL
    fi

    # Build and run the tests
    if [ "$TEST" == "YES" ] 
    then 
        catkin_make run_tests -DCATKIN_BLACKLIST_PACKAGES=$EXCLUDED_PACKAGES -DOMPL_FOUND=$OMPL
    fi


    #####################################
    ######## Writing log file ###########

    NOW=$(date +"%d-%m-%Y at %H:%M")

    echo "The last build was run the $NOW" > log.txt
    echo "The options were:" >> log.txt
    echo "    Tests enabled: $TEST" >> log.txt
    echo "    Clean before building: $CLEAN" >> log.txt
}


#####################################
######### Getting Params ############

while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -d|--debug)
    DEBUG=YES
    ;;
    -t|--test)
    TEST=YES
    ;;
    -c|--clean)
    CLEAN=YES
    ;;
    -s|--sim)
    SIM=1
    ;;
    -cr|--checkrepo)
    check_repo
    ;;
    -ompl)
    OMPL=1
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

#####################################
############### MAIN ################


if [ "$BUILD" == "YES" ]
then
    build_software
fi
