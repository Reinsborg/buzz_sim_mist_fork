from __future__ import print_function
import sys
import xml.etree.ElementTree as et

def is_location_right(location):
    file_path = location.attrib.get("file")
    if file_path is not None:
        if (not "/opt/ros" in file_path) and (not "/usr/src" in file_path):
            return True

    return False


def main():
    if len(sys.argv) < 3:
        print("Not enough arguments: %d where given when 2 are needed" % (len(sys.argv) - 1))
        return

    tree = et.parse(sys.argv[1])
    root = tree.getroot()
    errors = root.find("errors")
    if errors is None:
        print("File badly formatted \n Could not find \"errors\" tag")

    for error in errors.findall("error"):
        remove = False

        for location in error.findall("location"):
            if not is_location_right(location):
                remove = True
                break

        if remove:
            errors.remove(error)

    tree.write(sys.argv[2])

if __name__ == '__main__':
    main()
