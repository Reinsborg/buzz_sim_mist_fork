#!/bin/bash


RESULTFOLDER="./cppcheck_results"
RESULTFILE="$RESULTFOLDER/cppcheck.xml"
RESULTFILECLEANED="$RESULTFOLDER/cppcheck_cleaned.xml"

if [ ! -d $RESULTFOLDER ]; then
    mkdir -p $RESULTFOLDER
fi

if [ -f $RESULTFILE ];
then
    echo "removing old cppcheck.xml file"
    rm $RESULTFILE
else
    echo "cppcheck.xml file not found, skipping deletion"
fi 
cppcheck --enable=all --xml-version=2 --force --project=build/compile_commands.json &>> $RESULTFILE
python cppcheck_parser.py $RESULTFILE $RESULTFILECLEANED
cppcheck-htmlreport  --file=$RESULTFILECLEANED --title=LibreOffice --report-dir=$RESULTFOLDER --source-dir=.

firefox $RESULTFOLDER/index.html
    
