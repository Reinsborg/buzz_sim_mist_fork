#! /bin/bash

alias solo2='ssh ubuntu@192.168.12.129'
alias solo3='ssh ubuntu@192.168.12.161'
alias solo4='ssh ubuntu@192.168.12.35'
alias feng='ssh ubuntu@192.168.12.10'
alias rev2='ssh ubuntu@192.168.12.44'
alias catd='ssh ubuntu@192.168.12.87'
alias gizm='ssh ubuntu@192.168.12.34'
alias wedg='ssh ubuntu@192.168.12.20'
alias cent='ssh ubuntu@192.168.12.50'
alias valm='ssh nvidia@192.168.12.64'
alias vill='ssh nvidia@192.168.12.62'
alias vach='ssh nvidia@192.168.12.63'
alias plut='ssh nvidia@192.168.12.66'
alias mars='ssh nvidia@192.168.12.65'

function getlogs {
	scp ubuntu@192.168.12.161:/home/ubuntu/ROS_DRONES_WS2/src/rosbuzz/buzz_scripts/log/* solo3/
	scp ubuntu@192.168.12.35:/home/ubuntu/ROS_DRONES_WS2/src/rosbuzz/buzz_scripts/log/* solo4/
	scp ubuntu@192.168.12.10:/home/ubuntu/ROS_DRONES_WS2/src/rosbuzz/buzz_scripts/log/* feng/
	scp ubuntu@192.168.12.144:/home/ubuntu/ROS_DRONES_WS2/src/rosbuzz/buzz_scripts/log/* rev2/
	scp ubuntu@192.168.12.87:/home/ubuntu/ROS_DRONES_WS2/src/rosbuzz/buzz_scripts/log/* cat/
}
