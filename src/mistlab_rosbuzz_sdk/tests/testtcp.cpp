#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

using namespace std;
boost::property_tree::ptree pt;

main() {
    boost::asio::io_service io_service;

    boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string("127.0.0.1"), 6789);
    boost::asio::ip::tcp::acceptor acceptor(io_service, endpoint);

    std::cout << "Waiting for client..." << std::endl;
    boost::asio::ip::tcp::iostream stream;
    boost::system::error_code ec;
    acceptor.accept(*(stream.rdbuf()), ec);

    std::cout << "Connection accepted, start reading." << std::endl;
    while(1) {
        while (!stream.rdbuf()->available()) {}

        //   std::cout << "bytes available: " << stream.rdbuf()->available() << std::endl;

        char buf[stream.rdbuf()->available()+1]; 
        buf[stream.rdbuf()->available()] = '\0';
        stream.read(buf, stream.rdbuf()->available());
        stream.flush();          
        std::cout << buf << std::endl;
    }
}