#!/usr/bin/env python
from time import sleep
from Tkinter import Tk, Frame, Button

import rospy
from geometry_msgs.msg import PoseStamped
import tf
from tf.transformations import quaternion_slerp
from mavros_msgs.srv import CommandBool, CommandLong, SetMode, \
                            SetModeRequest, CommandBoolRequest, CommandLongRequest
from mavros_msgs.msg import CommandCode

NODE_NAME = "test_tag_solo"

MAX_PUB_QUEUE = 2
MAX_SUB_QUEUE = 2

LOCAL_POSE_PUB_NAME = "/solo1/mavros/setpoint_position/local"
LOCAL_POSE_SUB_NAME = "/solo1/mavros/local_position/pose"


POSITION_FILTER_COEF = 0.5
QUATERNION_FILTER_COEF = 0.7

DELTA_X = 0.2
DELTA_Y = 0.2
DELTA_Z = 0.2

SET_MODE_SRV_NAME = "/solo1/mavros/set_mode"
ARM_SRV_NAME = "/solo1/mavros/cmd/arming"
COMMAND_SRV_NAME = "/solo1/mavros/cmd/command"

SOLO_CAMERA_FRAME = '/solo1/camera_link'
AR_MARKER_FRAME = '/ar_marker_1'
AR_MARKER_FILTERED_FRAME = '/ar_marker_1_filtered'


class DroneCommands(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)

        self._set_mode_srv = rospy.ServiceProxy(SET_MODE_SRV_NAME,
                                                SetMode)
        self._arm_srv = rospy.ServiceProxy(ARM_SRV_NAME,
                                           CommandBool)
        self._command_srv = rospy.ServiceProxy(COMMAND_SRV_NAME,
                                               CommandLong)

        self._local_pose_pub = rospy.Publisher(LOCAL_POSE_PUB_NAME,
                                               PoseStamped,
                                               queue_size=MAX_PUB_QUEUE)

        self._local_pose_sub = rospy.Subscriber(LOCAL_POSE_SUB_NAME,
                                                PoseStamped,
                                                self.local_pose_callback)
        self._listener = tf.TransformListener()
        self._broadcaster = tf.TransformBroadcaster()
        self._marker_frame_pose = None
        self._robot_local_pose = None

        self.pack()
        self.createWidgets()

        self._master = master
        self._master.after(100, self.cyclic_task)

    def takeoff(self):
        print "Take off"
        self.set_mode("LOITER")
        self.arm()
        sleep(2)
        self.set_mode("GUIDED")
        self.send_takeoff_command(3)

    def land(self):
        print "land"
        self.set_mode("LAND")
        self.disarm()
        self.send_land_command()

    def set_mode(self, mode):
        mode_msg = SetModeRequest()
        mode_msg.base_mode = 0
        mode_msg.custom_mode = mode
        self._set_mode_srv.call(mode_msg)

    def arm(self):
        msg = CommandBoolRequest()
        msg.value = True
        self._arm_srv.call(msg)

    def disarm(self):
        msg = CommandBoolRequest()
        msg.value = False
        self._arm_srv.call(msg)

    def send_takeoff_command(self, altitude):
        msg = CommandLongRequest()
        msg.command = CommandCode.NAV_TAKEOFF
        msg.param7 = altitude
        self._command_srv.call(msg)

    def send_land_command(self):
        msg = CommandLongRequest()
        msg.command = CommandCode.NAV_LAND
        self._command_srv.call(msg)

    def createWidgets(self):
        self._takeoff = Button(self)
        self._takeoff["text"] = "Takeoff"
        self._takeoff["command"] = self.takeoff

        self._takeoff.pack({"side": "left"})

        self._land = Button(self)
        self._land["text"] = "Land",
        self._land["command"] = self.land

        self._land.pack({"side": "left"})

        self._move_front = Button(self, text="X+", command=lambda : self.move_delta(DELTA_X, 0, 0))
        self._move_front.pack({"side": "left"})
        self._move_back = Button(self, text="X-", command=lambda : self.move_delta(-DELTA_X, 0, 0))
        self._move_back.pack({"side": "left"})
        self._move_right = Button(self, text="Y+", command=lambda : self.move_delta(0, DELTA_Y, 0))
        self._move_right.pack({"side": "left"})
        self._move_left = Button(self, text="Y-", command=lambda : self.move_delta(0, -DELTA_Y, 0))
        self._move_left.pack({"side": "left"})
        self._move_up = Button(self, text="Z+", command=lambda : self.move_delta(0, 0, DELTA_Z))
        self._move_up.pack({"side": "left"})
        self._move_down = Button(self, text="Z-", command=lambda : self.move_delta(0, 0, -DELTA_Z))
        self._move_down.pack({"side": "left"})

    def move_delta(self, x=0, y=0, z=0):
        if self._robot_local_pose is None:
            return
        pose_stamped = PoseStamped()
        pose_stamped.pose.position.x = self._robot_local_pose[0][0] + x
        pose_stamped.pose.position.y = self._robot_local_pose[0][1] + y
        pose_stamped.pose.position.z = self._robot_local_pose[0][2] + z
        pose_stamped.pose.orientation.x = self._robot_local_pose[1][0]
        pose_stamped.pose.orientation.y = self._robot_local_pose[1][1]
        pose_stamped.pose.orientation.z = self._robot_local_pose[1][2]
        pose_stamped.pose.orientation.z = self._robot_local_pose[1][3]

        self._local_pose_pub.publish(pose_stamped)


    def get_transform(self):
        try:
            (position, orientation) = self._listener.lookupTransform(SOLO_CAMERA_FRAME,
                                                                     AR_MARKER_FRAME,
                                                                     rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            return None, None

        return position, orientation


    def filter_pose(self, new_pose, old_pose):
        position  = [0, 0, 0]
        position[0] = (new_pose[0][0] * (1.0 - POSITION_FILTER_COEF)) + \
                      (old_pose[0][0] * POSITION_FILTER_COEF)
        position[1] = (new_pose[0][1] * (1.0 - POSITION_FILTER_COEF)) + \
                      (old_pose[0][1] * POSITION_FILTER_COEF)
        position[2] = (new_pose[0][2] * (1.0 - POSITION_FILTER_COEF)) + \
                      (old_pose[0][2] * POSITION_FILTER_COEF)
        orientation = quaternion_slerp(old_pose[1], new_pose[1], (1.0 - QUATERNION_FILTER_COEF))

        return [position, orientation]

    def cyclic_task(self):
        position, orientation = self.get_transform()

        if (position is not None) and (orientation is not None):
            position = list(position)
            if self._marker_frame_pose is None:
                self._marker_frame_pose = (position, orientation)
            self._marker_frame_pose = self.filter_pose((position, orientation),
                                                       self._marker_frame_pose)

            self._broadcaster.sendTransform(self._marker_frame_pose[0],
                                            self._marker_frame_pose[1],
                                            rospy.Time.now(),
                                            AR_MARKER_FILTERED_FRAME,
                                            SOLO_CAMERA_FRAME,)
        self._master.after(100, self.cyclic_task)

    def local_pose_callback(self, pose_stamped):

        pose = [[pose_stamped.pose.position.x, pose_stamped.pose.position.y, pose_stamped.pose.position.z],
                [pose_stamped.pose.orientation.x, pose_stamped.pose.orientation.y,
                 pose_stamped.pose.orientation.z, pose_stamped.pose.orientation.w]]

        if self._robot_local_pose is None:
            self._robot_local_pose = pose

        self._robot_local_pose = self.filter_pose(pose, self._robot_local_pose)



def main():
    rospy.init_node(NODE_NAME, anonymous=True)

    root = Tk()
    app = DroneCommands(master=root)
    app.mainloop()
    root.destroy()


if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
