MIST ROSBuzz Nodes
==============================

This repository contains a series of nodes for ROSBuzz ecosystem:

- DJI M100 Gazebo adapter with custom GPS sensor and collision detection
- 3DR Solo Gazebo adapter based on dronekit
- DJI Guidance adapter with transformations (launch file)
- various ROS .launch files for both onboard and simulated launch
- a Zooids TCP client (Json format) node (visualisation of Gazebo outputs only)
- a Zooids USB-Serial controler node
- a Crazyflies Swarm controle node (visualisation of Gazebo outputs only)
- many 3D mesh models for Gazebo (buildings and UAVs)
- a 3D model from PANGAEA-X field deployment (Moon/Mars alike)
- DJI Onboard SDK adapter for ROSBuzz
- XBEE XCTU profiel configuration files
- various .world files for Gazebo
- a list of the latest M100/Spiri IP for debug ssh connections