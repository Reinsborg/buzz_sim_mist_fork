#include <boost/bind.hpp>
#include "sensor_msgs/NavSatFix.h"
#ifndef __GAZEBO_BRIDGE_H__
#define __GAZEBO_BRIDGE_H__

#include "mavros_msgs/GlobalPositionTarget.h"
#include "mavros_msgs/CommandCode.h"
#include "mavros_msgs/CommandLong.h"
#include "mavros_msgs/ExtendedState.h"
#include "mavros_msgs/CommandBool.h"
#include "mavros_msgs/SetMode.h"
#include "mavros_msgs/State.h"
#include "sensor_msgs/BatteryState.h"
#include "mavros_msgs/PositionTarget.h"
#include "mavros_msgs/StreamRate.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/PoseStamped.h"
#include <ros/ros.h>
#include <stdio.h>
#include <cstdlib>
#include <string>

#include "std_msgs/String.h"

#include <gazebo_msgs/ModelState.h>
#include <gazebo_msgs/LinkState.h>
#include <gazebo_msgs/SetModelState.h>
#include <gazebo_msgs/SetLinkState.h>

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/Range.h>
#include <sensor_msgs/LaserScan.h>

#include <tf/LinearMath/Quaternion.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Vector3.h>

#include <cmath>


class GAZEBOROTORS
{
private:
    sensor_msgs::NavSatFix GlobalPosition;
    sensor_msgs::NavSatFix GlobalPosition_ref;
    geometry_msgs::PoseStamped LocalENUPosition;
    geometry_msgs::PoseStamped TargetLocalNEDPosition;
    geometry_msgs::PoseStamped HomeLocalENUPosition;
    int global_position_ref_seted = 0;
    int local_position_ref_seted = 0;

    mavros_msgs::ExtendedState status;
    bool sdk_incontrol = false;
    bool sim_batt = true;
    sensor_msgs::LaserScan LS;
    double takeofftime = 0.0;

    ros::Publisher mav_power_status;
    ros::Publisher mav_currentpos;
    ros::Publisher mav_currentLpos;
    ros::Publisher obstacles;
    ros::Subscriber localsetpointSub;
    ros::Subscriber GlobalPoseSub;
    ros::Subscriber LocalPoseSub;
    ros::Subscriber Obs1Sub;
    ros::Subscriber Obs2Sub;
    ros::Subscriber Obs3Sub;
    ros::Subscriber Obs4Sub;
    ros::Subscriber Obs5Sub;
    ros::Publisher mav_flight_status;
    ros::Publisher mav_currentalt;

    ros::ServiceServer cmd_service;
    ros::ServiceServer arm_service;
    ros::ServiceServer mode_service;
    ros::ServiceServer stream_service;
    ros::ServiceClient enable_motors_client;
    ros::Publisher set_pose;
    
public:
    GAZEBOROTORS(ros::NodeHandle& nh, ros::NodeHandle& nh_priv) {
	init_parameters(nh_priv);
	init_pubsub(nh);
    }
    std::string model_name;
    void publish();
    
private:
    int init_parameters(ros::NodeHandle& nh_private);
    int init_pubsub(ros::NodeHandle& nh);
    
    bool sdk_permission_control(int st);


    void gps_convert_ned(float &ned_x, float &ned_y, double gps_t_lon, double gps_t_lat, double gps_r_lon, double gps_r_lat);
    void ned_convert_gps(float ned_x, float ned_y, double &gps_t_lon, double &gps_t_lat, double gps_r_lon, double gps_r_lat);
    void gps_convert_enu_ned(float &ned_x, float &ned_y, float enu_x, float enu_y);
    void localsetpoint(const geometry_msgs::PoseStamped::ConstPtr &pt);
    void getfix(const sensor_msgs::NavSatFix::ConstPtr& msg);
    void getPose(const geometry_msgs::PoseStamped::ConstPtr& msg);
    void getObsF(const sensor_msgs::LaserScan::ConstPtr& msg);
    void getObsR(const sensor_msgs::LaserScan::ConstPtr& msg);
    void getObsL(const sensor_msgs::LaserScan::ConstPtr& msg);
    void getObsB(const sensor_msgs::LaserScan::ConstPtr& msg);
    void getObsD(const sensor_msgs::LaserScan::ConstPtr& msg);
    bool fcucmds(mavros_msgs::CommandLong::Request& req, mavros_msgs::CommandLong::Response& res);
    bool fcuarm(mavros_msgs::CommandBool::Request& req, mavros_msgs::CommandBool::Response& res);
    bool fcumode(mavros_msgs::SetMode::Request& req, mavros_msgs::SetMode::Response& res);
    bool fcustream(mavros_msgs::StreamRate::Request& req, mavros_msgs::StreamRate::Response& res);
    bool local_position_control(float x, float y, float z, geometry_msgs::Quaternion dst_q);
    bool global_position_control(double lat, double lon, float alt, float yaw);
};

#endif
