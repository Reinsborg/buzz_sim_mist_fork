#ifndef __DJI_SDK_RCCLIENT_H__
#define __DJI_SDK_RCCLIENT_H__
/* ----------------------------------------------------------------
 * File: rcclient2.h
 * Created on: 27/06/2017
 * Author: David St-Onge
 * Description: ROS node for tcp server of the Mission Planner
 *
 * Copyright Humanitas Solutions & MIST Laboratory. All rights reserved.
 ------------------------------------------------------------------ */

#include "sensor_msgs/NavSatFix.h"
#include "rosbuzz/neigh_pos.h"
#include "mavros_msgs/GlobalPositionTarget.h"
#include "mavros_msgs/CommandCode.h"
#include "mavros_msgs/CommandLong.h"
#include "mavros_msgs/CommandBool.h"
#include "mavros_msgs/State.h"
#include "mavros_msgs/BatteryStatus.h"
#include <mavros_msgs/ParamGet.h>
#include <mavros_msgs/ParamValue.h>
#include <mavros_msgs/Mavlink.h>

#include "std_msgs/String.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Float64.h"
#include <ros/ros.h>
#include <stdio.h>
#include <cstdlib>
#include <ros/ros.h>
//#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/fcntl.h>
#include <netinet/in.h>

using namespace std;

ros::ServiceClient mav_client;
ros::ServiceClient arm_client;
ros::ServiceClient sendrc_client;
ros::Publisher bag_client;
ros::Publisher userspos;

mavros_msgs::CommandLong cmd_srv;
mavros_msgs::CommandBool arm_srv;
std_msgs::String bag_cmd;
bool record=false, newcmd=false;
uint8_t robotID = 0;
int sockfd, newsockfd, portno=8001; //Socket file descriptors and port number
struct sockaddr_in cli_addr;
string sendgpsrc, sendstatusrc;
sensor_msgs::NavSatFix globalpos;

const int CMD_REQUEST_UPDATE = 666;

enum UAV_CMDS {Arm=1, Disarm, Takeoff, Land, Gohome, Gimbal, Goto, Mission, Startbag, Stopbag, Shutdown, Sendstates, Sendgps, Getusers};
enum UAV_MISSIONS {Square, P, O, L, Y, Scan};

#endif