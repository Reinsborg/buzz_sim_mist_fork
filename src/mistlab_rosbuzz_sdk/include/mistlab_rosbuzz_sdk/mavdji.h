#ifndef __DJI_SDK_MIST_H__
#define __DJI_SDK_MIST_H__

#include <boost/bind.hpp>
#include "sensor_msgs/NavSatFix.h"
#include "sensor_msgs/Imu.h"
#include "mavros_msgs/GlobalPositionTarget.h"
#include "mavros_msgs/CommandCode.h"
#include "mavros_msgs/CommandLong.h"
#include "mavros_msgs/ExtendedState.h"
#include "mavros_msgs/CommandBool.h"
#include "mavros_msgs/SetMode.h"
#include "mavros_msgs/StreamRate.h"
#include "mavros_msgs/State.h"
#include "mavros_msgs/BatteryStatus.h"
#include "mavros_msgs/PositionTarget.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/PoseStamped.h"
#include <ros/ros.h>
#include <stdio.h>
#include <cstdlib>

#include <dji_sdk/dji_sdk.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>

#define EARTH_RADIUS (double) 6378137.0
#define C_PI (double) 3.141592653589793
#define DEG2RAD(DEG) ((DEG)*((C_PI)/(180.0)))

//extern DJI::onboardSDK::ROSAdapter *rosAdapter;
//using namespace DJI::onboardSDK;
DJI::onboardSDK::ROSAdapter *rosAdapter;

class DJISDKMIST
{
private:

	ros::Publisher mav_power_status;
	sensor_msgs::NavSatFix GlobalPosition;
	sensor_msgs::NavSatFix GlobalPosition_ref;
	geometry_msgs::PoseStamped LocalPosition;
	ros::Publisher mav_currentpos;
	ros::Publisher mav_currentLpos;
	ros::Publisher mav_currentodom;
	ros::Publisher mav_currentimu;
	ros::Publisher mav_currentalt;
	ros::Subscriber localsetpointSub;
	ros::Publisher mav_flight_status;
	ros::Publisher data_received_from_remote_device_publisher;

	ros::ServiceServer send_data_to_remote_device_service;
	ros::ServiceServer cmd_service;
	ros::ServiceServer arm_service;
	ros::ServiceServer mode_service;
	ros::ServiceServer stream_service;

	tf::TransformBroadcaster odom_broadcaster;

	int global_position_ref_seted = 0;
	bool sdk_incontrol = false;
	char app_key[65];
	unsigned char transparent_transmission_data[100];
	DJI::onboardSDK::ActivateData user_act_data;

public:
    DJISDKMIST(ros::NodeHandle& nh, ros::NodeHandle& nh_priv) {
	init_pubsub(nh);
	init_parameters(nh_priv);
    }

private:
    int init_parameters(ros::NodeHandle& nh_private);
    int init_pubsub(ros::NodeHandle& nh);
    void broadcast_callback();
    void transparent_transmission_callback(unsigned char *buf, unsigned char len);
    void takecontrol_callback(DJI::onboardSDK::CoreAPI* api, DJI::onboardSDK::Header *protHeader, DJI::UserData data __UNUSED);
    bool send_data_to_remote_device_callback(dji_sdk::SendDataToRemoteDevice::Request& request, dji_sdk::SendDataToRemoteDevice::Response& response);


    void gps_convert_ned(float &ned_x, float &ned_y, double gps_t_lon, double gps_t_lat, double gps_r_lon, double gps_r_lat);
    void gps_convert_enu_ned(float &ned_x, float &ned_y, float enu_x, float enu_y);
    uint8_t getextended(uint8_t state);
    bool sdk_permission_control(int i);
    void localsetpoint(const geometry_msgs::PoseStamped::ConstPtr &);
    bool djicmds(mavros_msgs::CommandLong::Request& req, mavros_msgs::CommandLong::Response& res);
    bool djiarm(mavros_msgs::CommandBool::Request& req, mavros_msgs::CommandBool::Response& res);
    bool djimode(mavros_msgs::SetMode::Request& req, mavros_msgs::SetMode::Response& res);
    bool djistream(mavros_msgs::StreamRate::Request& req, mavros_msgs::StreamRate::Response& res);
    bool local_position_control(float x, float y, float z, float yaw);
    bool global_position_control(double lat, double lon, float alt, float yaw);
    void doOrbit();
};

#endif


