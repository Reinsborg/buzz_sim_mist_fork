/* ----------------------------------------------------------------
 * File: solo_gazebo_plugin.h
 * Created on: 26/07/2017
 * Author: Pierre-Yves Breches
 * Description: Solo ROS plugin for Gazebo
 *
 * Copyright Humanitas Solutions. All rights reserved.
 ------------------------------------------------------------------ */

#pragma once

#include <vector>
#include <ros/callback_queue.h>
#include <ros/subscribe_options.h>
#include <sensor_msgs/NavSatFix.h>
#include <nav_msgs/Odometry.h>

#include <gazebo/physics/physics.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/common/Plugin.hh>
#include <gazebo/common/Events.hh>

#include <ros/ros.h>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <geometry_msgs/WrenchStamped.h>

// latitude, longitude, altitude, bearing
typedef std::array<double, 4> navsatfix_t;


namespace gazebo
{
class GazeboSolo : public ModelPlugin
{

public:
  static const double DEG_2_RAD;
  static const double EARTH_RADIUS;
  static const navsatfix_t DEFAULT_ORIGIN_POSITION;
  static const double POSTION_FILTER_COEF;

  GazeboSolo();
  virtual ~GazeboSolo();
  bool getParams( physics::ModelPtr _parent, sdf::ElementPtr _sdf );
  void Load( physics::ModelPtr _parent, sdf::ElementPtr _sdf );

protected:
  virtual void UpdateChild();

private:
  physics::WorldPtr world_;
  physics::ModelPtr model_;
  physics::LinkPtr link_;
  math::Pose link_pose_;
  ros::NodeHandle* rosnode_;
  ros::Subscriber sub_global_pose_;
  ros::Subscriber sub_local_pose_;
  std::string link_name_;
  std::string group_name_;
  std::string global_position_topic_;
  std::string local_position_topic_;
  std::string frame_name_;
  std::string robot_namespace_;
  boost::mutex pose_lock_;

  navsatfix_t origin_position_;


  // Custom Callback Queue
  ros::CallbackQueue queue_;
  void QueueThread();
  boost::thread callback_queue_thread_;

  void CallbackGlobalPose(const sensor_msgs::NavSatFix::ConstPtr nav_sat);
  void CallbackLocalPose(const nav_msgs::Odometry::ConstPtr odom_msg_ptr);

  // Pointer to the update event connection
  event::ConnectionPtr update_connection_;

  math::Vector3 getXYZFromGps(const sensor_msgs::NavSatFix::ConstPtr);
  math::Vector3 filterPosition(const math::Vector3 new_vec,
                               const math::Vector3 old_vec);

};

}