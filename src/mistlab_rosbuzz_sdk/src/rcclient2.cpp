
/* ----------------------------------------------------------------
 * File: rcclient2.cpp
 * Created on: 27/06/2017
 * Author: David St-Onge
 * Description: ROS node for tcp server of the Mission Planner
 *
 * Copyright Humanitas Solutions & MIST Laboratory. All rights reserved.
 ------------------------------------------------------------------ */

#include <mistlab_rosbuzz_sdk/rcclient2_config.h>

void rcCallback(const std_msgs::String rc_data_in)
	/** Description: parse a string received from the Mission Planner and send the command
	******************************************************************* */
{
  std::string rcdata = rc_data_in.data;
  uint8_t cmdID = 0;
  int args[10];
  std::string delimiter = ",";
  size_t pos = 0;
  int parse_it=0, socket_res;
  std::string token;
  while ((pos = rcdata.find(delimiter)) != std::string::npos)
  {
      token = rcdata.substr(0, pos);
      if(parse_it==0)
		    cmdID = std::atoi(token.c_str());
      else
        args[parse_it-1]=std::atof(token.c_str());
//      ROS_INFO("Got %s(%f)", token,args[i]);  // Kept for Debug
      rcdata.erase(0, pos + delimiter.length());
      parse_it++;
  }
  cmd_srv.request.param1 = 0;
  cmd_srv.request.command = 0;
  newcmd=false;
  UAV_CMDS gotcmd = (UAV_CMDS)cmdID;
  switch(gotcmd)
  {
    case Arm:
      cmd_srv.request.command = mavros_msgs::CommandCode::COMPONENT_ARM_DISARM;
      cmd_srv.request.param1 = 1;
      newcmd=true;
      break;
    case Disarm:
      cmd_srv.request.command = mavros_msgs::CommandCode::COMPONENT_ARM_DISARM;
      cmd_srv.request.param1 = 0;
      newcmd=true;
      break;
    case Takeoff:
      cmd_srv.request.command = mavros_msgs::CommandCode::NAV_TAKEOFF;
      newcmd=true;
      break;
    case Land:
      cmd_srv.request.command = mavros_msgs::CommandCode::NAV_LAND;
      newcmd=true;
      break;
    case Gohome: // TODO: Not implemented in ROSBuzz yet.
      cmd_srv.request.command = mavros_msgs::CommandCode::NAV_RETURN_TO_LAUNCH;
      newcmd=true;
      break;
    case Gimbal: // Expecting yaw, pitch and drone id.
      cmd_srv.request.command = mavros_msgs::CommandCode::DO_MOUNT_CONTROL;
      cmd_srv.request.param1 = (float)args[0];
      cmd_srv.request.param2 = (float)args[1]/pow(10,7);
      cmd_srv.request.param3 = (float)args[2]/pow(10,7);
      newcmd=true;
      break;
    case Goto: // Expecting longitude, latitude, altitude and drone id.
      ROS_INFO(rcdata.c_str());
      ROS_INFO("RCCLIENT - Received %i: [%i] with [%i,%i,%i,%i]", parse_it, cmdID, args[0], args[1], args[2], args[3]);
      cmd_srv.request.command = mavros_msgs::CommandCode::NAV_WAYPOINT;
      cmd_srv.request.param1 = (float)args[0];
      cmd_srv.request.param5 = (float)args[1]/pow(10,7);
      cmd_srv.request.param6 = (float)args[2]/pow(10,7);
      cmd_srv.request.param7 = (float)args[3]/pow(10,7);
      newcmd=true;
      break;
    case Mission: // Routine to scan around an object, TODO: not re-implemented yet.
      cmd_srv.request.command = mavros_msgs::CommandCode::MISSION_START;
      cmd_srv.request.param1 = Scan;
      newcmd=true;
      break;
    case Startbag:
      ROS_INFO("RCCLIENT - Start a rosbag!");
      if(!record)
      {
        bag_cmd.data="record";
        bag_client.publish(bag_cmd);
        record=true;
      }
      break;
    case Stopbag:
      ROS_INFO("RCCLIENT - Stop the rosbag!");
      if(record)
      {
        bag_cmd.data = "stop";
        bag_client.publish(bag_cmd);
        record=false;
      }
      break;
    case Shutdown:  // Locally shutting down the companion computer. TODO: Share over the fleet
      ROS_INFO("RCCLIENT - Shutdown the Manifold!");
      system("dbus-send --system --dest=\"org.freedesktop.ConsoleKit\" --print-reply --type=method_call /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop");
      break;
    case Sendstates:
      //ROS_INFO("TCP Sending Status [%i]", sendstatusrc.length());
      socket_res = write(newsockfd,sendstatusrc.c_str(),(size_t)sendstatusrc.length());
      if(socket_res<0)
        ROS_ERROR("ERROR writing to socket");
      cmd_srv.request.command = CMD_REQUEST_UPDATE;
      newcmd=true;
      break;
    case Sendgps:
      //ROS_INFO("TCP Sending GPS [%i]", sendgpsrc.length());
      socket_res = write(newsockfd,sendgpsrc.c_str(),(size_t)sendgpsrc.length());
      if(socket_res<0)
        ROS_ERROR("ERROR writing to socket");
      break;
    case Getusers:  // TODO: Not yet re-implemented.
      uint8_t n = args[0];
      uint8_t id = 0;
      //ROS_INFO("RCCLIENT - Got %i users GPS array!", n);
      auto current_time = ros::Time::now();
      rosbuzz::neigh_pos users_pos_array; //neigh_pos_array.clear();
      users_pos_array.header.frame_id = "/world";
      users_pos_array.header.stamp = current_time;
      for(int i=0;i<n;i++) {
        sensor_msgs::NavSatFix newuser;
        /*id = rcdata.data[i*13+2];
                          newuser.position_covariance_type=id; //custom robot id storage
        union {uint8_t bytes[4]; int32_t i;}lon={.bytes={rcdata.data[i*13+3],rcdata.data[i*13+4],rcdata.data[i*13+5],rcdata.data[i*13+6]}};
        newuser.latitude=lon.i/pow(10,7);
        union {uint8_t bytes[4]; int32_t i;}lat={.bytes={rcdata.data[i*13+7],rcdata.data[i*13+8],rcdata.data[i*13+9],rcdata.data[i*13+10]}};
        newuser.longitude=lat.i/pow(10,7);
        union {uint8_t bytes[4]; int32_t i;}alt={.bytes={rcdata.data[i*13+11],rcdata.data[i*13+12],rcdata.data[i*13+13],rcdata.data[i*13+14]}};
        newuser.altitude=alt.i/pow(10,7);
        ROS_INFO("Got %i user at: %0.7f, %0.7f, %0.7f",id,newuser.latitude,newuser.longitude,newuser.altitude);
        newuser.header.stamp = current_time;
        newuser.header.frame_id = "/world";*/
        users_pos_array.pos_neigh.push_back(newuser);
      }
      userspos.publish(users_pos_array);
      break;
  }
  if(newcmd)
  {
	if (mav_client.call(cmd_srv))
 		{
			if(cmd_srv.response.success!=1)
        ROS_ERROR("RCCLIENT - Flight Controller (or Buzz controller) service refused CMDID: %i",cmd_srv.request.command);
		}
    else{ ROS_ERROR("RCCLIENT - Failed to call DJI MISTLab FC (or Buzz controller) service"); }
  }
}

void updateNeighbors(const rosbuzz::neigh_pos data)
	/** Description: parse the neighbors gps msg, create a string and store it
	******************************************************************* */
{
	int id = 0, longi = 0, lati = 0, alti = 0;
	int nb_uavs = data.pos_neigh.size() + 1;
	sendgpsrc.clear();
	char tmp[(nb_uavs*4+1)*sizeof(int)];
	//ROS_INFO("Neighbors array size: %i\n", nb_uavs);
	sprintf(tmp,"%i,%i,%i,%i,%i", nb_uavs, robotID, (int)(globalpos.longitude*pow(10,7)), (int)(globalpos.latitude*pow(10,7)), (int)(globalpos.altitude*pow(10,7)));
	sendgpsrc.append(tmp);
	for(int uav_it=0; uav_it<nb_uavs-1; ++uav_it)
	{
		id = data.pos_neigh[uav_it].position_covariance_type;
		longi = static_cast<int>((data.pos_neigh[uav_it].longitude*pow(10,7)));
		lati = (int)(data.pos_neigh[uav_it].latitude*pow(10,7));
		alti = (int)(data.pos_neigh[uav_it].altitude*pow(10,7));
		sprintf(tmp,",%i,%i,%i,%i",id,longi,lati,alti);
		sendgpsrc.append(tmp);
	}
	sprintf(tmp,",\n");
	sendgpsrc.append(tmp);
	//ROS_INFO("Construct GPS string: %s",sendrc.c_str());
}

void localPosCallback(const sensor_msgs::NavSatFix::ConstPtr& my_pose)
	/** Description: update the local gps from the flight controler topic
	******************************************************************* */
{
  globalpos.position_covariance_type = robotID;
  globalpos.longitude = my_pose->longitude;
  globalpos.latitude = my_pose->latitude;
	if(false) // for debug test without simulator or drones
  {
    rosbuzz::neigh_pos test;
		sensor_msgs::NavSatFix tmp;
		tmp.position_covariance_type = 2;
		tmp.longitude = globalpos.longitude + 0.005;
		tmp.latitude = globalpos.latitude;
		tmp.altitude = globalpos.altitude;
		test.pos_neigh.push_back(tmp);
		tmp.position_covariance_type = 3;
		tmp.longitude = globalpos.longitude;
		tmp.latitude = globalpos.latitude - 0.005;
		tmp.altitude = globalpos.altitude;
		test.pos_neigh.push_back(tmp);
    updateNeighbors(test);
	}
}

void localAltCallback(const std_msgs::Float64::ConstPtr& my_height)
	/** Description: update the local height from the flight controler topic
	******************************************************************* */
{
  globalpos.altitude = my_height->data;	//TODO: Change this to a subscriber on rel_alt topic.
}

void getFleetStates(const mavros_msgs::Mavlink msg)
	/** Description: parse the neighbors state msg, create a string and store it
	******************************************************************* */
{
	sendstatusrc.clear();
	char tmp[sizeof(int)];

	int nb_uavs = msg.sysid;
	sprintf(tmp,"%i,", nb_uavs);
  sendstatusrc.append(tmp);
	int len = msg.payload64.size();
	/* Go throught the obtained payload*/
  for (int msg_it = 0; msg_it < len; msg_it++) {
    sprintf(tmp,"%i,",(int)msg.payload64[msg_it]);
    sendstatusrc.append(tmp);
  }
	sendstatusrc.append("\n");
	//ROS_INFO("Made a Status String[%i]: %s", len, sendstatusrc.c_str());  // for debug
}

int init_socket()
{
  	/** Description: creates the TCP socket and waits for a first client
	******************************************************************* */
  socklen_t clilen;
  struct sockaddr_in serv_addr;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
    ROS_ERROR("ERROR opening socket");
  fcntl(sockfd, F_SETFL, O_NONBLOCK);  // set to non-blocking
  fcntl(sockfd, F_SETFL, O_ASYNC);     // set to asynchronous I/O
  int enable = 1;
  if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
    ROS_ERROR("setsockopt(SO_REUSEADDR) failed");
  bzero((char *) &serv_addr, sizeof(serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);
  if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    ROS_ERROR("ERROR on binding");
  listen(sockfd,5);
  clilen = sizeof(cli_addr);
  newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
  if (newsockfd < 0)
		ROS_ERROR("ERROR on accept");
	else
		ROS_INFO("Received Connection. Starting ROS loop.");
}

int main(int argc, char *argv[])
	/** Description: main ros loop initiating the tcp server and managing the communication
	******************************************************************* */
{
  int main_operate_code = 0;
  bool valid_flag = false;
  bool err_flag = false;
  
  char buffer[256]; //buffer array of size 256
  std_msgs::String message;
  std::stringstream sstream_read;
  int nb_lines, rv;
  ros::Duration d(0.1); // 10Hz

  std::string fcclient_name;
  ros::init(argc, argv, "dji_sdk_rcclient");
  ROS_INFO("dji_sdk_rcclient");
  ros::NodeHandle nh("~");
  ros::NodeHandle nh2;
  ros::Rate loop_rate(10); //loops per sec.
	bool xbeeplugged = false;
	std::string robot_name;

  /*Obtain fc client name from parameter server*/
  if(nh.getParam("fcclient_name", fcclient_name)) {ROS_INFO("FC CLIENT NAME %s",fcclient_name.c_str());}
  else {ROS_ERROR("Provide a fc client name in Launch file"); system("rosnode kill dji_sdk_rcclient");}

	if (nh.getParam("xbee_plugged", xbeeplugged))
    ; // Nothing to do.
	else
  {
		ROS_ERROR("Provide the xbee plugged boolean in Launch file");
		system("rosnode kill dji_sdk_rcclient");
	}
	if (!xbeeplugged)
  {
		if (nh.getParam("name", robot_name))
		; // Nothing to do.
		else
    {
      ROS_ERROR("Provide the xbee plugged boolean in Launch file");
      system("rosnode kill dji_sdk_rcclient");
		}
		robotID=strtol(robot_name.c_str() + 5, NULL, 10);
	}
  else
  {
		/*Wait until we get the current robot id*/
		mavros_msgs::ParamGet::Request robot_id_srv_request; robot_id_srv_request.param_id="id";
		mavros_msgs::ParamGet::Response robot_id_srv_response;
		ros::ServiceClient  xbeestatus_srv = nh2.serviceClient<mavros_msgs::ParamGet>("xbee_status");
		while(!xbeestatus_srv.call(robot_id_srv_request,robot_id_srv_response))
    {
      //run once
      ros::spinOnce();
      loop_rate.sleep();
      //sleep for the mentioned loop rate
      ROS_WARN("RC Client is waiting for Xbee device ID");
		}
		robotID=robot_id_srv_response.value.integer;
	}

  ros::spinOnce();

  bag_cmd.data="idle";
  bag_client = nh.advertise<std_msgs::String>("/record/bagcmd",5);
  mav_client = nh2.serviceClient<mavros_msgs::CommandLong>(fcclient_name);
  arm_client = nh2.serviceClient<mavros_msgs::CommandBool>("dji_mavarm");
  userspos = nh2.advertise<rosbuzz::neigh_pos>("users_pos", 5);
  ros::Subscriber fleet_update_sub = nh2.subscribe<mavros_msgs::Mavlink>("fleet_status", 5, getFleetStates);
  ros::Subscriber fleet_pos_sub = nh2.subscribe<rosbuzz::neigh_pos>("neighbours_pos", 5, updateNeighbors);
  ros::Subscriber local_pos_sub = nh2.subscribe("global_position/global", 5, localPosCallback);
  ros::Subscriber local_alt_sub = nh2.subscribe("global_position/rel_alt", 5, localAltCallback);

  init_socket();

  while(ros::ok())
  {
    ros::spinOnce();

    sstream_read.str(std::string()); //Clear contents of string stream
    bzero(buffer,256);
    nb_lines = read(newsockfd,buffer,255); /* there was data to read */
      
    if (nb_lines <= 0)
    {
      ROS_ERROR("ERROR reading from socket");
      listen(sockfd,5);
      socklen_t clilen = sizeof(cli_addr);
      newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
      if (newsockfd < 0)
        ROS_ERROR("ERROR on accept");
      else
        ROS_INFO("Received Connection. RE-Starting ROS loop.");
    }
    // printf("Here is the message: %s\n",buffer);
    sstream_read << buffer;
    message.data = sstream_read.str();
    if(message.data.length()>0)
    {
      ROS_INFO("Received TCP string %s(%i)", message.data.c_str(), message.data.length());
      rcCallback(message);
    }

    loop_rate.sleep();
  }
  return 0;
}
