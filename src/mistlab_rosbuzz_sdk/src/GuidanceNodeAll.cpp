/*
 * GuidanceNode.cpp
 *
 *  Created on: Apr 29, 2015
 */

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <ros/ros.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
//#include <sensor_msgs/Image.h>
//#include <sensor_msgs/image_encodings.h>

#include <image_transport/image_transport.h>


#include "DJI_guidance.h"
#include "DJI_utility.h"

#include <geometry_msgs/TransformStamped.h> //IMU
#include <geometry_msgs/Vector3Stamped.h> //velocity
#include <sensor_msgs/LaserScan.h> //obstacle distance & ultrasonic
#include <sensor_msgs/CameraInfo.h> // camera info message. Contains cam params
#include "yaml-cpp/yaml.h" // use to parse YAML calibration file
#include <fstream> // required to parse YAML


image_transport::Publisher left_image_pub[CAMERA_PAIR_NUM];
image_transport::Publisher right_image_pub[CAMERA_PAIR_NUM];
image_transport::Publisher depth_image_pub[CAMERA_PAIR_NUM];
ros::Publisher cam_info_left_pub[CAMERA_PAIR_NUM];
ros::Publisher cam_info_right_pub[CAMERA_PAIR_NUM];
ros::Publisher cam_info_depth_pub[CAMERA_PAIR_NUM];

ros::Publisher imu_pub;
ros::Publisher obstacle_distance_pub;
ros::Publisher velocity_pub;
ros::Publisher ultrasonic_pub;

using namespace cv;

#define WIDTH 320
#define HEIGHT 240
#define IMAGE_SIZE (HEIGHT * WIDTH)

char        	key       = 0;
bool            show_images = 0;
uint8_t         verbosity = 0;
e_vbus_index	CAMERA_ID = e_vbus1;
DJI_lock        g_lock;
DJI_event       g_event;
Mat             g_greyscale_image_left(HEIGHT, WIDTH, CV_8UC1);
Mat				g_greyscale_image_right(HEIGHT, WIDTH, CV_8UC1);
Mat				g_depth(HEIGHT,WIDTH,CV_16SC1);
Mat				depth8(HEIGHT, WIDTH, CV_8UC1);
String direction[] = {"bottom", "front", "right", "back", "left"};
stereo_cali cali[CAMERA_PAIR_NUM];

std::ostream& operator<<(std::ostream& out, const e_sdk_err_code value){
	const char* s = 0;
	static char str[100]={0};
#define PROCESS_VAL(p) case(p): s = #p; break;
	switch(value){
		PROCESS_VAL(e_OK);
		PROCESS_VAL(e_load_libusb_err);
		PROCESS_VAL(e_sdk_not_inited);
		PROCESS_VAL(e_disparity_not_allowed);
		PROCESS_VAL(e_image_frequency_not_allowed);
		PROCESS_VAL(e_config_not_ready);
		PROCESS_VAL(e_online_flag_not_ready);
		PROCESS_VAL(e_stereo_cali_not_ready);
		PROCESS_VAL(e_libusb_io_err);
		PROCESS_VAL(e_timeout);
	default:
		strcpy(str, "Unknown error");
		s = str;
		break;
	}
#undef PROCESS_VAL

	return out << s;
}


std::string camera_params_left;
std::string camera_params_right;

static const char CAM_YML_NAME[]    = "camera_name";
static const char WIDTH_YML_NAME[]  = "image_width";
static const char HEIGHT_YML_NAME[] = "image_height";
static const char K_YML_NAME[]      = "camera_matrix";
static const char D_YML_NAME[]      = "distortion_coefficients";
static const char R_YML_NAME[]      = "rectification_matrix";
static const char P_YML_NAME[]      = "projection_matrix";
static const char DMODEL_YML_NAME[] = "distortion_model";

// struct to parse camera calibration YAML
struct SimpleMatrix
{
    int rows;
    int cols;
    double* data;

    SimpleMatrix(int rows, int cols, double* data)
        : rows(rows), cols(cols), data(data)
    {}
};

void transfer_SimpleMatrix_from_YML_to_ROSmsg(const YAML::Node& node, SimpleMatrix& m)
{
    int rows, cols;
    rows = node["rows"].as<int>();
    cols = node["cols"].as<int>();
    const YAML::Node& data = node["data"];
    for (int i = 0; i < rows*cols; ++i)
    {
        m.data[i] = data[i].as<double>();
    }
}

void read_params_from_yaml_and_fill_cam_info_msg(std::string& file_name, sensor_msgs::CameraInfo& cam_info)
{
    std::ifstream fin(file_name.c_str());
    YAML::Node doc = YAML::Load(fin);

    cam_info.width = doc[WIDTH_YML_NAME].as<int>();
    cam_info.height = doc[HEIGHT_YML_NAME].as<int>();

    SimpleMatrix K_(3, 3, &cam_info.K[0]);
    transfer_SimpleMatrix_from_YML_to_ROSmsg(doc[K_YML_NAME], K_);
    SimpleMatrix R_(3, 3, &cam_info.R[0]);
    transfer_SimpleMatrix_from_YML_to_ROSmsg(doc[R_YML_NAME], R_);
    SimpleMatrix P_(3, 4, &cam_info.P[0]);
    transfer_SimpleMatrix_from_YML_to_ROSmsg(doc[P_YML_NAME], P_);

    cam_info.distortion_model = doc[DMODEL_YML_NAME].as<std::string>();
    const YAML::Node& D_node = doc[D_YML_NAME];
    int D_rows, D_cols;
    D_rows = D_node["rows"].as<int>();
    D_cols = D_node["cols"].as<int>();
    const YAML::Node& D_data = D_node["data"];
    cam_info.D.resize(D_rows*D_cols);

    for (int i = 0; i < D_rows*D_cols; ++i)
    {
        cam_info.D[i] = D_data[i].as<float>();
    }
}

int my_callback(int data_type, int data_len, char *content)
{
    g_lock.enter();
    ros::Time current_time = ros::Time::now();

    if (e_image == data_type && NULL != content)
    {
	//std::cout << "grabbing images" << std::endl;
        image_data* data = (image_data*)content;

		for(int cid=0; cid < CAMERA_PAIR_NUM; ++cid )
		{
			//std::cout << cid << std::endl;
			if ( data->m_greyscale_image_left[cid] ){
				memcpy(g_greyscale_image_left.data, data->m_greyscale_image_left[cid], IMAGE_SIZE);
	            if (show_images) {
			        imshow("left",  g_greyscale_image_left);
        	    }
				// publish left greyscale image
				cv_bridge::CvImage left_8;
				g_greyscale_image_left.copyTo(left_8.image);
				left_8.header.frame_id  = "guidance_" + direction[cid] + "_left";
				left_8.header.stamp	= current_time;
				left_8.encoding		= sensor_msgs::image_encodings::MONO8;
				left_image_pub[cid].publish(left_8.toImageMsg());

				sensor_msgs::CameraInfo g_cam_info_left;
        		g_cam_info_left.header.stamp = current_time;
            	g_cam_info_left.header.frame_id = "guidance_" + direction[cid] + "_left";
     	        try {
			        camera_params_left = "camera_params_left.yaml";
				    //hardcoded location as it was unable to find the files
			        std::string cam_info_location = "/home/ubuntu/ROS_WS/src/Guidance-SDK-ROS/calibration_files/"+camera_params_left;
           		    read_params_from_yaml_and_fill_cam_info_msg(cam_info_location, g_cam_info_left);
			        g_cam_info_left.P[2] = cali[cid].cu;
			        g_cam_info_left.P[6] = cali[cid].cv;
			        g_cam_info_left.P[0] = cali[cid].focal;
			        g_cam_info_left.P[5] = cali[cid].focal;
			        g_cam_info_left.P[3] = 0; //Tx
			        g_cam_info_left.P[7] = 0; //Ty
           		    cam_info_left_pub[cid].publish(g_cam_info_left);
                    // adjustments for depth camera info
			        //g_cam_info_left.P[3] = -cali[cid].focal * cali[cid].baseline/2; //Tx for centered depth
                	//cam_info_depth_pub[cid].publish(g_cam_info_left);
			        
                    //std::cout << "left camera info published successfully" << std::endl;
       			} catch(...) {
            		// if yaml fails to read data, don't try to publish
       			}
			}
			if ( data->m_greyscale_image_right[cid] ){
				memcpy(g_greyscale_image_right.data, data->m_greyscale_image_right[cid], IMAGE_SIZE);
       	 	    if (show_images) {
			        imshow("right", g_greyscale_image_right);
	            }
				// publish right greyscale image
				cv_bridge::CvImage right_8;
				g_greyscale_image_right.copyTo(right_8.image);
				right_8.header.frame_id  = "guidance_" + direction[cid] + "_right";
				right_8.header.stamp	 = current_time;
				right_8.encoding  	 = sensor_msgs::image_encodings::MONO8;
				right_image_pub[cid].publish(right_8.toImageMsg());

		        sensor_msgs::CameraInfo g_cam_info_right;
		        g_cam_info_right.header.stamp = current_time;
       			g_cam_info_right.header.frame_id = "guidance_" + direction[cid] + "_right";
                try {
			        camera_params_right = "camera_params_right.yaml";
				    //hardcoded location as it was unable to find the files
			        std::string cam_info_location = "/home/ubuntu/ROS_WS/src/Guidance-SDK-ROS/calibration_files/"+camera_params_right;
                    read_params_from_yaml_and_fill_cam_info_msg(cam_info_location, g_cam_info_right);
                    g_cam_info_right.P[2] = cali[cid].cu;
                    g_cam_info_right.P[6] = cali[cid].cv;
                    g_cam_info_right.P[0] = cali[cid].focal;
                    g_cam_info_right.P[5] = cali[cid].focal;
                    g_cam_info_right.P[3] = -cali[cid].focal * cali[cid].baseline;
                    g_cam_info_right.P[7] = 0;
                    cam_info_right_pub[cid].publish(g_cam_info_right);
                    //std::cout << "right camera info published successfully" << std::endl;
				} catch(...) {
                    // if yaml fails to read data, don't try to publish
                }
			}
			if ( data->m_disparity_image[cid] ){
				memcpy(g_depth.data, data->m_disparity_image[cid], IMAGE_SIZE * 2);
				g_depth = cali[cid].baseline * 1000 * cali[cid].focal * 16 / g_depth;
				g_depth.convertTo(depth8, CV_8UC1);
		       	if (show_images) {
				    imshow("depth", depth8);
            	}
				//publish depth image
				cv_bridge::CvImage depth_16;
				g_depth.copyTo(depth_16.image);
				depth_16.header.frame_id  = "guidance_" + direction[cid];
				depth_16.header.stamp	  = current_time;
				depth_16.encoding	  = sensor_msgs::image_encodings::TYPE_16UC1; //sensor_msgs::image_encodings::MONO16;
				depth_image_pub[cid].publish(depth_16.toImageMsg());
				//std::cout << "depth image received" << std::endl;
			}
	        //key = waitKey(1);
    		}
	}

    /* imu */
    if ( e_imu == data_type && NULL != content )
    {
        imu *imu_data = (imu*)content;
        if (verbosity > 1) {
            printf( "frame index: %d, stamp: %d\n", imu_data->frame_index, imu_data->time_stamp );
            printf( "imu: [%f %f %f %f %f %f %f]\n", imu_data->acc_x, imu_data->acc_y, imu_data->acc_z, imu_data->q[0], imu_data->q[1], imu_data->q[2], imu_data->q[3] );

        }
    	// publish imu data
		geometry_msgs::TransformStamped g_imu;
		g_imu.header.frame_id = "guidance";
		g_imu.header.stamp    = current_time;
		g_imu.transform.translation.x = imu_data->acc_x;
		g_imu.transform.translation.y = imu_data->acc_y;
		g_imu.transform.translation.z = imu_data->acc_z;
		g_imu.transform.rotation.w = imu_data->q[0];
		g_imu.transform.rotation.x = imu_data->q[1];
		g_imu.transform.rotation.y = imu_data->q[2];
		g_imu.transform.rotation.z = imu_data->q[3];
		//imu_pub.publish(g_imu);
    }
    /* velocity */
    if ( e_velocity == data_type && NULL != content )
    {
        velocity *vo = (velocity*)content;
        if (verbosity > 1) {
            printf( "frame index: %d, stamp: %d\n", vo->frame_index, vo->time_stamp );
            printf( "vx:%f vy:%f vz:%f\n", 0.001f * vo->vx, 0.001f * vo->vy, 0.001f * vo->vz );
        }

		// publish velocity
		geometry_msgs::Vector3Stamped g_vo;
		g_vo.header.frame_id = "guidance";
		g_vo.header.stamp    = current_time;
		g_vo.vector.x = 0.001f * vo->vx;
		g_vo.vector.y = 0.001f * vo->vy;
		g_vo.vector.z = 0.001f * vo->vz;
		velocity_pub.publish(g_vo);
    }

    /* obstacle distance */
    if ( e_obstacle_distance == data_type && NULL != content )
    {
        obstacle_distance *oa = (obstacle_distance*)content;
        if (verbosity > 1) {
            printf( "frame index: %d, stamp: %d\n", oa->frame_index, oa->time_stamp );
            printf( "obstacle distance:" );
            for ( int i = 0; i < CAMERA_PAIR_NUM; ++i )
            {
                printf( " %f ", 0.01f * oa->distance[i] );
            }
            printf( "\n" );
        }

		// publish obstacle distance
		sensor_msgs::LaserScan g_oa;
		g_oa.ranges.resize(CAMERA_PAIR_NUM);
		g_oa.header.frame_id = "guidance";
		g_oa.header.stamp    = current_time;
		for ( int i = 0; i < CAMERA_PAIR_NUM; ++i )
			g_oa.ranges[i] = 0.01f * oa->distance[i];
		obstacle_distance_pub.publish(g_oa);
	}

    /* ultrasonic */
    if ( e_ultrasonic == data_type && NULL != content )
    {
        ultrasonic_data *ultrasonic = (ultrasonic_data*)content;
        if (verbosity > 1) {
            printf( "frame index: %d, stamp: %d\n", ultrasonic->frame_index, ultrasonic->time_stamp );
            for ( int d = 0; d < CAMERA_PAIR_NUM; ++d )
            {
                printf( "ultrasonic distance: %f, reliability: %d\n", ultrasonic->ultrasonic[d] * 0.001f, (int)ultrasonic->reliability[d] );
            }
        }

		// publish ultrasonic data
		sensor_msgs::LaserScan g_ul;
		g_ul.ranges.resize(CAMERA_PAIR_NUM);
		g_ul.intensities.resize(CAMERA_PAIR_NUM);
		g_ul.header.frame_id = "guidance";
		g_ul.header.stamp    = current_time;
		for ( int d = 0; d < CAMERA_PAIR_NUM; ++d ){
			g_ul.ranges[d] = 0.001f * ultrasonic->ultrasonic[d];
			g_ul.intensities[d] = 1.0 * ultrasonic->reliability[d];
		}
		ultrasonic_pub.publish(g_ul);
    }

    g_lock.leave();
    g_event.set_event();

    return 0;
}

#define RETURN_IF_ERR(err_code) { if( err_code ){ release_transfer(); \
std::cout<<"Error: "<<(e_sdk_err_code)err_code<<" at "<<__LINE__<<","<<__FILE__<<std::endl; return -1;}}

int main(int argc, char** argv)
{
    show_images = 0;
    verbosity = 0;

    /* initialize ros */
    ros::init(argc, argv, "GuidanceNodeAll");
    ros::NodeHandle my_node;
    image_transport::ImageTransport it(my_node);
    my_node.getParam("/left_param_file", camera_params_left);
    my_node.getParam("/right_param_file", camera_params_right);
    for (int i = 0; i<CAMERA_PAIR_NUM; i++) {
    	if (i == 0 || i == 1 || i == 2 || i == 4 ) { // for bottom and left
	    	left_image_pub[i]           = it.advertise("/guidance/"+direction[i]+"/left/image_rect",1);
	    	right_image_pub[i]          = it.advertise("/guidance/"+direction[i]+"/right/image_rect",1);
            cam_info_left_pub[i]        = my_node.advertise<sensor_msgs::CameraInfo>("/guidance/"+direction[i]+"/left/camera_info",1);
	    	cam_info_right_pub[i]       = my_node.advertise<sensor_msgs::CameraInfo>("/guidance/"+direction[i]+"/right/camera_info",1);
	    }
	    if (false) { // for only front and right
	    	left_image_pub[i]           = it.advertise("/guidance/"+direction[i]+"/left/image_rect",1);
            cam_info_left_pub[i]        = my_node.advertise<sensor_msgs::CameraInfo>("/guidance/"+direction[i]+"/left/camera_info",1);
	    	//cam_info_depth_pub[i]       = my_node.advertise<sensor_msgs::CameraInfo>("/guidance/"+direction[i]+"/depth/camera_info",1);
            depth_image_pub[i]          = it.advertise("/guidance/"+direction[i]+"/depth/image_rect",1);
	    }
    }
//    imu_pub  				= my_node.advertise<geometry_msgs::TransformStamped>("/guidance/imu",1);
//    velocity_pub  			= my_node.advertise<geometry_msgs::Vector3Stamped>("/guidance/velocity",1);
    obstacle_distance_pub		= my_node.advertise<sensor_msgs::LaserScan>("/guidance/obstacle_distance",1);
//    ultrasonic_pub			= my_node.advertise<sensor_msgs::LaserScan>("/guidance/ultrasonic",1);


    int err_code;
    /* initialize guidance */
    try {
	reset_config();
    	err_code = init_transfer();
    	//RETURN_IF_ERR(err_code);
	//wait_for_board_ready();
    } catch(...) {
	ROS_INFO("OUPS!!!");
	ros::shutdown;
    }

	int online_status[CAMERA_PAIR_NUM];
	err_code = get_online_status(online_status);
	RETURN_IF_ERR(err_code);
    std::cout<<"Sensor online status: ";
	for (int i=0; i<CAMERA_PAIR_NUM; i++)
        std::cout<<online_status[i]<<" ";
    std::cout<<std::endl;

	// get cali param
	err_code = get_stereo_cali(cali);
	RETURN_IF_ERR(err_code);
	std::cout<<"cu\tcv\tfocal\tbaseline\n";
	for (int i=0; i<CAMERA_PAIR_NUM; i++)
	{
            std::cout<<cali[i].cu<<"\t"<<cali[i].cv<<"\t"<<cali[i].focal<<"\t"<<cali[i].baseline<<std::endl;
	}

    /* select data */

    for (int i = 0; i<CAMERA_PAIR_NUM; i++) {
        if (i == 0 || i == 1 || i == 2 || i == 4) { // for bottom and left
	        err_code = select_greyscale_image((e_vbus_index)i, true);
            RETURN_IF_ERR(err_code);
    	    err_code = select_greyscale_image((e_vbus_index)i, false);
            RETURN_IF_ERR(err_code);
	    }
	    if (false) { // for only front and right
            err_code = select_greyscale_image((e_vbus_index)i, true);
            RETURN_IF_ERR(err_code);
            err_code = select_disparity_image((e_vbus_index)i);
            RETURN_IF_ERR(err_code);
        }
    }

//    select_imu();
//    select_ultrasonic();
    select_obstacle_distance();
//    select_velocity();
    /* start data transfer */
    err_code = set_sdk_event_handler(my_callback);
    RETURN_IF_ERR(err_code);
    err_code = start_transfer();
    RETURN_IF_ERR(err_code);

	// for setting exposure
	exposure_param para;
	para.m_is_auto_exposure = 1;
	para.m_step = 10;
	para.m_expected_brightness = 120;
	para.m_camera_pair_index = CAMERA_ID;

	std::cout << "start_transfer" << std::endl;

	while (ros::ok())
	{
		ros::spinOnce();
	}

	/* release data transfer */
	err_code = stop_transfer();
	RETURN_IF_ERR(err_code);
	//make sure the ack packet from GUIDANCE is received
	sleep(1);
	std::cout << "release_transfer" << std::endl;
	err_code = release_transfer();
	RETURN_IF_ERR(err_code);
	ros::Duration(0.5).sleep();

    return 0;
}

/* vim: set et fenc=utf-8 ff=unix sts=0 sw=4 ts=4 : */
