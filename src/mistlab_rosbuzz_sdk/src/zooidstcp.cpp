
/* ----------------------------------------------------------------
 * File: zooidstcp.cpp
 * Created on: 20/11/2017
 * Author: David St-Onge
 * Description: ROS node for tcp client of the Zooids manager
 *
 * Copyright MIST Laboratory. All rights reserved.
 ------------------------------------------------------------------ */

#include "sensor_msgs/NavSatFix.h"
#include "rosbuzz/neigh_pos.h"
#include "mavros_msgs/GlobalPositionTarget.h"
#include "mavros_msgs/CommandCode.h"
#include "mavros_msgs/CommandLong.h"
#include "mavros_msgs/CommandBool.h"
#include "mavros_msgs/State.h"
#include "mavros_msgs/BatteryStatus.h"
#include <mavros_msgs/ParamGet.h>
#include <mavros_msgs/ParamValue.h>
#include <mavros_msgs/Mavlink.h>

#include <gazebo_msgs/ModelState.h>
#include <gazebo_msgs/GetModelState.h>

#include "std_msgs/String.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Float64.h"
#include <ros/ros.h>
#include <stdio.h>
#include <cstdlib>
#include <ros/ros.h>

#include <iostream>
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <glib-object.h>
#include <json-glib/json-glib.h>

using namespace std;
using boost::asio::ip::udp;
using boost::asio::ip::tcp;
using bfmt = boost::format;
std::string json_tcp_str;

uint8_t robotID = 0;
int PORT_NUMBER = 6789; //Socket file descriptors and port
char IP_ADD[30] = "127.0.0.1"; //"132.207.203.78";
// FROM zooids table
//  coordinatesMinX   100.0f
//  coordinatesMaxX   875.0f
//  coordinatesMinY   250.0f
//  coordinatesMaxY   800.0f
// FROM gazebo (y and x inverted)
// MinX -21
// MaxX 49
// MinY -98
// MaxY 17
float zooids_xscale = 775.0/120.0;
float zooids_yscale = 550.0/75.0;
float zooids_xoffset = 100.0 + 5*775.0/6.0;
float zooids_yoffset = 250.0 + 3*550.0/4.0-20;
int nb_uavs = 0;

struct sockaddr_in cli_addr;
string sendgpsrc, sendstatusrc;
sensor_msgs::NavSatFix globalpos;

void updateNeighbors(const rosbuzz::neigh_pos data)
	/** Description: parse the neighbors gps msg, create a string and store it
	******************************************************************* */
{
	nb_uavs = data.pos_neigh.size();
}

int main(int argc, char *argv[])
	/** Description: main ros loop initiating the tcp server and managing the communication
	******************************************************************* */
{
  ros::init(argc, argv, "zooids_bridge");
  ROS_INFO("zooids_bridge");
  ros::NodeHandle nh("~");
  ros::NodeHandle nh2;
  ros::Rate loop_rate(50); //loops per sec.

  
  ros::spinOnce();
  ros::Subscriber fleet_pos_sub = nh2.subscribe<rosbuzz::neigh_pos>("neighbours_pos", 5, updateNeighbors);

  ros::ServiceClient get_models = nh.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state");
  gazebo_msgs::GetModelState getModelState;

  boost::asio::io_service io_service;

  tcp::endpoint endpoint(boost::asio::ip::address::from_string(IP_ADD), PORT_NUMBER);

  // udp::socket socket(io_service);
  // socket.open(udp::v4());
  tcp::socket socket(io_service);
  boost::system::error_code error = boost::asio::error::host_not_found;
//        ROS_INFO_STREAM("Wait for acceptance... \n(Blocked here means server busy. Another x3client is ""running?)");
  socket.connect(endpoint, error);
  if (error) {
      ROS_ERROR_STREAM("Server offline, exit...");
      return EXIT_FAILURE;
  }

  ROS_INFO("Connected successfully.");

  while(ros::ok())
  {
    ros::spinOnce();

    if(get_models)
    {
      JsonBuilder *builder = json_builder_new (); 
      json_builder_begin_object (builder);
      json_builder_set_member_name (builder, "behaviour");
      json_builder_add_string_value (builder, "MANUAL");
      json_builder_set_member_name (builder, "state");
      json_builder_add_string_value (builder, "NORMAL");
      json_builder_set_member_name (builder, "body");
      json_builder_begin_object (builder);
      json_builder_set_member_name (builder, "zoo");
      json_builder_begin_array (builder);

      for(int i=0;i<nb_uavs;i++) {
        getModelState.request.model_name = (std::string)"robot" + std::to_string(i+1) ;
        getModelState.request.relative_entity_name = (std::string)"world" ;
        get_models.call(getModelState);
        if(getModelState.response.success) {
          geometry_msgs::Point pt  = getModelState.response.pose.position ;
 
          json_builder_begin_object (builder);
          json_builder_set_member_name (builder, "id");
          json_builder_add_int_value (builder, i);
          json_builder_set_member_name (builder, "pos");
          json_builder_begin_array (builder);
          json_builder_add_double_value (builder, pt.y * zooids_xscale + zooids_xoffset);
          json_builder_add_double_value (builder, -pt.x * zooids_yscale + zooids_yoffset);
          json_builder_end_array (builder);
          json_builder_end_object (builder);
        }
      }
      json_builder_end_array (builder);

      json_builder_end_object (builder);
      json_builder_end_object (builder);

      JsonGenerator *gen = json_generator_new ();
      JsonNode * root = json_builder_get_root (builder);
      json_generator_set_root (gen, root);
      gsize *length;
      gchar * json_buf = json_generator_to_data (gen, length);
      json_tcp_str.assign(json_buf, *length);
      json_tcp_str = json_tcp_str + "\n";
    }

    boost::array<char, 500> buf;
    std::copy(json_tcp_str.begin(),json_tcp_str.end(),buf.begin());
    boost::system::error_code error;
	  socket.write_some(boost::asio::buffer(buf, json_tcp_str.size()), error);

    loop_rate.sleep();
  }
  
  socket.close();
  return 0;
}
