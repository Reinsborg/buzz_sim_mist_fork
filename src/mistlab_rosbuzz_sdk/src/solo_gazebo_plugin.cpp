/* ----------------------------------------------------------------
 * File: solo_gazebo_plugin.cpp
 * Created on: 26/07/2017
 * Author: Pierre-Yves Breches
 * Description: Solo ROS plugin for Gazebo
 *
 * Copyright Humanitas Solutions. All rights reserved.
 ------------------------------------------------------------------ */

#include <mistlab_rosbuzz_sdk/solo_gazebo_plugin.h>
#include <tf/tf.h>



namespace gazebo
{
GZ_REGISTER_MODEL_PLUGIN(GazeboSolo);

const double GazeboSolo::DEG_2_RAD = M_PI/180.0;
const double GazeboSolo::EARTH_RADIUS = 6378137.0;
const navsatfix_t GazeboSolo::DEFAULT_ORIGIN_POSITION = {45.505280, -73.614574, 135.0, 350.0};
const double GazeboSolo::POSTION_FILTER_COEF = 0.7;

GazeboSolo::GazeboSolo()
{
}


GazeboSolo::~GazeboSolo()
{
  event::Events::DisconnectWorldUpdateBegin(this->update_connection_);
  // Custom Callback Queue
  this->queue_.clear();
  this->queue_.disable();
  this->rosnode_->shutdown();
  this->callback_queue_thread_.join();
  delete this->rosnode_;
}

bool GazeboSolo::getParams( physics::ModelPtr _parent, sdf::ElementPtr _sdf )
/** Description: Try to get the Gazebo parameters and return false if
 * one the mandatory parameters can be retrieved.
 ******************************************************************* */
{
  // load parameters
  this->robot_namespace_ = "";
  if (_sdf->HasElement("robotNamespace"))
  {
    this->robot_namespace_ = _sdf->GetElement("robotNamespace")->Get<std::string>() + "/";
  }

  if (!_sdf->HasElement("group_name"))
  {
    ROS_FATAL_NAMED("solo", "solo plugin missing <group_name>, cannot proceed");
    return false;
  }
  else
  {
    this->group_name_ = _sdf->GetElement("group_name")->Get<std::string>();
  }

  if (!_sdf->HasElement("bodyName"))
  {
    ROS_FATAL_NAMED("solo", "solo plugin missing <bodyName>, cannot proceed");
    return false;
  }
  else
  {
    this->link_name_ = _sdf->GetElement("bodyName")->Get<std::string>();
  }

  this->model_ = _parent;

  this->link_ = _parent->GetLink(this->link_name_);
  if (!this->link_)
  {
    ROS_FATAL_NAMED("solo", "gazebo_ros_solo plugin error: bodyName: %s does not exist\n",this->link_name_.c_str());
    return false;
  }

  if (!_sdf->HasElement("global_position_topic"))
  {
    ROS_FATAL_NAMED("solo", "solo plugin missing <global_position_topic>, cannot proceed");
    return false;
  }
  else
  {
    this->global_position_topic_ = _sdf->GetElement("global_position_topic")->Get<std::string>();
  }

  if (!_sdf->HasElement("local_position_topic"))
  {
    ROS_FATAL_NAMED("solo", "solo plugin missing <local_position_topic>, cannot proceed");
    return false;
  }
  else
  {
    this->local_position_topic_ = _sdf->GetElement("local_position_topic")->Get<std::string>();
  }

  if (!_sdf->HasElement("origin_position"))
  {
    ROS_WARN_NAMED("solo", "solo missing ros param <origin_position>, taking the default value");
    this->origin_position_ = DEFAULT_ORIGIN_POSITION;
  }
  else
  {
    std::string tmp_str = _sdf->GetElement("origin_position")->Get<std::string>();;
    std::string seperator = ",";
    size_t start = 0;

    for (size_t index = 0; index < 4; index++)
    {
      size_t end = tmp_str.find(seperator, start);
      this->origin_position_[index] = std::stod(tmp_str.substr(start, end).c_str());
      start = end + seperator.size();
    }
  }

  return true;
}

void GazeboSolo::Load( physics::ModelPtr _parent, sdf::ElementPtr _sdf )
{
  // Get the world name.
  this->world_ = _parent->GetWorld();

  if(!getParams(_parent, _sdf))
  {
    return;
  }

  // Make sure the ROS node for Gazebo has already been initialized
  if (!ros::isInitialized())
  {
    ROS_FATAL_STREAM_NAMED("solo", "A ROS node for Gazebo has not been initialized, unable to load plugin. "
      << "Load the Gazebo system plugin 'libgazebo_ros_api_plugin.so' in the gazebo_ros package)");
    return;
  }

  if(this->group_name_ == "")
  {
    this->rosnode_ = new ros::NodeHandle(this->robot_namespace_);
  }
  else
  {
    this->rosnode_ = new ros::NodeHandle(this->group_name_ + "_" + this->robot_namespace_);

    this->global_position_topic_.insert(0, "/" + this->group_name_);
    this->local_position_topic_.insert(0, "/" + this->group_name_);
  }


  ros::SubscribeOptions global_pose_so =
      ros::SubscribeOptions::create<sensor_msgs::NavSatFix>(
      this->global_position_topic_, 10, boost::bind(
      &GazeboSolo::CallbackGlobalPose, this, _1),
      ros::VoidPtr(), &this->queue_);
  this->sub_global_pose_ = this->rosnode_->subscribe(global_pose_so);

  ros::SubscribeOptions local_pose_so =
      ros::SubscribeOptions::create<nav_msgs::Odometry>(
      this->local_position_topic_, 10, boost::bind(
      &GazeboSolo::CallbackLocalPose, this, _1),
      ros::VoidPtr(), &this->queue_);
  this->sub_local_pose_ = this->rosnode_->subscribe(local_pose_so);

  // Custom Callback Queue
  this->callback_queue_thread_ = boost::thread(boost::bind( &GazeboSolo::QueueThread,this ) );

  // New Mechanism for Updating every World Cycle
  // Listen to the update event. This event is broadcast every
  // simulation iteration.
  this->update_connection_ = event::Events::ConnectWorldUpdateBegin(
      boost::bind(&GazeboSolo::UpdateChild, this));
}


void GazeboSolo::UpdateChild()
{
  this->model_->SetGravityMode(false);
  this->pose_lock_.lock();
  this->model_->SetWorldPose (this->link_pose_);
  this->pose_lock_.unlock();
}


void GazeboSolo::QueueThread()
{
  static const double timeout = 0.01;

  while (this->rosnode_->ok())
  {
    this->queue_.callAvailable(ros::WallDuration(timeout));
  }
}


void GazeboSolo::CallbackGlobalPose(const sensor_msgs::NavSatFix::ConstPtr nav_sat_ptr)
{
  math::Vector3 position = getXYZFromGps(nav_sat_ptr);

  this->pose_lock_.lock();
  this->link_pose_.pos = filterPosition(position, this->link_pose_.pos);
  this->pose_lock_.unlock();
}

void GazeboSolo::CallbackLocalPose(const nav_msgs::Odometry::ConstPtr odom_msg_ptr)
{
  math::Quaternion quaternion = math::Quaternion(odom_msg_ptr->pose.pose.orientation.w,
                                                 odom_msg_ptr->pose.pose.orientation.x,
                                                 odom_msg_ptr->pose.pose.orientation.y,
                                                 odom_msg_ptr->pose.pose.orientation.z);

  this->pose_lock_.lock();
  this->link_pose_.rot = quaternion;
  this->pose_lock_.unlock();
}

math::Vector3 GazeboSolo::getXYZFromGps(const sensor_msgs::NavSatFix::ConstPtr nav_sat_ptr)
{
	double ned_x = DEG_2_RAD * (nav_sat_ptr->latitude - origin_position_[0]) * EARTH_RADIUS;
	double ned_y = DEG_2_RAD * (nav_sat_ptr->longitude - origin_position_[1]) *
                 EARTH_RADIUS * cos(DEG_2_RAD * origin_position_[0]);
  double ned_z = nav_sat_ptr->altitude - origin_position_[2];

  return math::Vector3(ned_x, ned_y, ned_z);
}

math::Vector3 GazeboSolo::filterPosition(const math::Vector3 new_vec,
                                         const math::Vector3 old_vec)
{
  return ((1.0 - POSTION_FILTER_COEF) * new_vec) + (POSTION_FILTER_COEF * old_vec);
}

}
