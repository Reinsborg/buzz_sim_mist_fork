#include <mistlab_rosbuzz_sdk/rotors_gazebo_bridge.h>
#include "gazebo_msgs/SetPhysicsProperties.h"
#include "hector_uav_msgs/EnableMotors.h"

#define EARTH_RADIUS (double) 6378137.0
#define DEG2RAD(DEG) ((DEG)*((M_PI)/(180.0)))
#define RAD2DEG(RAD) ((RAD)*((180.0)/(M_PI)))
#define BATTTIME 4.0 //in minutes
//#define ROS_RATE 50 //Hz

template <typename T> int sign(T val) {
    return (T(0) < val) - (val < T(0));
}

void GAZEBOROTORS::gps_convert_ned(float &ned_x, float &ned_y,
		double gps_t_lon, double gps_t_lat,
		double gps_r_lon, double gps_r_lat)
{
	double d_lon = gps_t_lon - gps_r_lon;
	double d_lat = gps_t_lat - gps_r_lat;
	ned_x = DEG2RAD(d_lat) * EARTH_RADIUS;
    ned_x = std::floor(ned_x * 1000000) / 1000000;
	ned_y = DEG2RAD(d_lon) * EARTH_RADIUS * cos(DEG2RAD(gps_t_lat));
    ned_y = std::floor(ned_y * 1000000) / 1000000;
}

void GAZEBOROTORS::ned_convert_gps(float ned_x, float ned_y,
		double &gps_t_lon, double &gps_t_lat,
		double gps_r_lon, double gps_r_lat)
{
	double d_lat = RAD2DEG(ned_x / EARTH_RADIUS);
    d_lat = std::floor(d_lat * 1000000) / 1000000;
    gps_t_lat = d_lat + gps_r_lat;
	double d_lon = RAD2DEG(ned_y / (EARTH_RADIUS * cos(DEG2RAD(gps_t_lat))));
    d_lon = std::floor(d_lon * 1000000) / 1000000;
    gps_t_lon = d_lon + gps_r_lon;
}

bool GAZEBOROTORS::local_position_control(float x, float y, float z, geometry_msgs::Quaternion dst_q)
{
  float dst_x = x;
  float dst_y = y;
  float dst_z = z;

  if(global_position_ref_seted == 0)
  {
      ROS_INFO("Cannot run local position navigation because home position haven't been set yet!");
      return false;
  }

  //ROS_INFO("%s MavCtrl LocalPos: %.3f, %.3f / %.3f, %.3f / %.3f, %.3f", model_name.c_str(), LocalENUPosition.pose.position.x, LocalENUPosition.pose.position.y, HomeLocalENUPosition.pose.position.x, HomeLocalENUPosition.pose.position.y, TargetLocalNEDPosition.pose.position.x, TargetLocalNEDPosition.pose.position.y);
  //ROS_INFO("Target delta: %.3f, %.3f, %.3f", dst_x, dst_y, dst_z);
  //ROS_INFO("SetFlight to: %f, %f, %f", dst_x-LocalPosition.pose.position.x, dst_y-LocalPosition.pose.position.y, dst_z+LocalPosition.pose.position.z);
//    rosAdapter->flight->setMovementControl((uint8_t)0x90, dst_x-LocalPosition.pose.position.x, dst_y-LocalPosition.pose.position.y, dst_z+LocalPosition.pose.position.z, yaw);
  //rosAdapter->flight->setMovementControl((uint8_t)0x90, dst_x, dst_y, dst_z+LocalPosition.pose.position.z, yaw);
  TargetLocalNEDPosition.pose.position.x = dst_x + HomeLocalENUPosition.pose.position.y;// + LocalPosition.pose.position.x;
  TargetLocalNEDPosition.pose.position.y = dst_y + HomeLocalENUPosition.pose.position.x;// + LocalPosition.pose.position.y;
  TargetLocalNEDPosition.pose.position.z = dst_z;
  TargetLocalNEDPosition.pose.orientation = dst_q;

  return true;
}

bool GAZEBOROTORS::global_position_control(double longitude, double latitude, float altitude, float yaw)
{
  float dst_x;
  float dst_y;
  float dst_z = altitude;
  float distance = 0.0;

  if(global_position_ref_seted == 0)
  {
      ROS_INFO("Cannot run global position navigation because home position haven't set yet!");
      return false;
  }

  gps_convert_ned(dst_x, 
          dst_y,
          longitude, latitude,
          GlobalPosition.longitude,  GlobalPosition.latitude);
  distance = sqrt(dst_x*dst_x+dst_y*dst_y);
  while (distance > 1.0)
  {
    //ROS_INFO("Current GlobalPos: %.7f, %.7f, %.7f",GlobalPosition.longitude,GlobalPosition.latitude,GlobalPosition.altitude);
    //ROS_INFO("Target GPS (%.2f): %.7f, %.7f, %.7f", distance, longitude,latitude, altitude);
    //ROS_INFO("SetFlight to: %f, %f, %f", dst_x, dst_y, dst_z);

    //rosAdapter->flight->setMovementControl((uint8_t)0x90, dst_x, dst_y, dst_z, yaw);
    geometry_msgs::Quaternion z_q;
    local_position_control(dst_x, dst_y, dst_z, z_q);

    ros::Duration(0.02).sleep();

    gps_convert_ned(dst_x, 
            dst_y,
            longitude, latitude,
            GlobalPosition.longitude,  GlobalPosition.latitude);
    distance = sqrt(dst_x*dst_x+dst_y*dst_y);
  }

  return true;
}

bool GAZEBOROTORS::sdk_permission_control(int st)
{
  if (st == 1 && !sdk_incontrol)
  {
    ROS_INFO("Request Control");
    hector_uav_msgs::EnableMotors srv;
    srv.request.enable = true;
    //rosAdapter->coreAPI->setControl(true);//,(DJI::onboardSDK::CallBack)&DJISDKMIST::takecontrol_callback,this);
    if(enable_motors_client.call(srv))
    {
        ROS_INFO("Enabling motors: %i", (int)srv.response.success);
        sdk_incontrol = (int)srv.response.success;
    } else
        ROS_ERROR("Failed to call service enable_motors");
      
  }
  else if (st == 0 && sdk_incontrol)
  {
    ROS_INFO("Release Control");
    hector_uav_msgs::EnableMotors srv;
    srv.request.enable = false;
    enable_motors_client.call(srv);//rosAdapter->coreAPI->setControl(false);
    sdk_incontrol = 0;
  }
  return true;
}

bool GAZEBOROTORS::fcucmds(mavros_msgs::CommandLong::Request  &req,
         mavros_msgs::CommandLong::Response &res)
{
   float d_x =666, d_y=666;
   ROS_INFO("Got command: %i",req.command);
   double target[3] = {(float)req.param5,(float)req.param6,(float)req.param7};
   switch(req.command)
   {
	case mavros_msgs::CommandCode::NAV_TAKEOFF:
   	ROS_INFO("TAKE OFF!!!!");
		sdk_permission_control(1);
		ros::Duration(0.01).sleep();
		//rosAdapter->flight->task(DJI::onboardSDK::Flight::TASK::TASK_TAKEOFF);
    ROS_INFO("Go up to altitude: %.2f", req.param7);
		//rosAdapter->flight->setMovementControl((uint8_t)0x90, 0.0, 0.0, req.param7, 0.0);
    TargetLocalNEDPosition = HomeLocalENUPosition;
    TargetLocalNEDPosition.pose.position.x = HomeLocalENUPosition.pose.position.y;
    TargetLocalNEDPosition.pose.position.y = HomeLocalENUPosition.pose.position.x;
    TargetLocalNEDPosition.pose.position.z=req.param7;
    status.landed_state = 2;
    takeofftime = ros::Time::now().toSec();
		res.success = true;
		break;
	case mavros_msgs::CommandCode::NAV_LAND:
   		ROS_INFO("LAND!!!!");
		//rosAdapter->flight->task(DJI::onboardSDK::Flight::TASK::TASK_LANDING);
                TargetLocalNEDPosition = LocalENUPosition;
                TargetLocalNEDPosition.pose.position.x=LocalENUPosition.pose.position.y+HomeLocalENUPosition.pose.position.y;
                TargetLocalNEDPosition.pose.position.y=LocalENUPosition.pose.position.x+HomeLocalENUPosition.pose.position.x;
                TargetLocalNEDPosition.pose.position.z = HomeLocalENUPosition.pose.position.z;
                status.landed_state = 1;
		ros::Duration(2).sleep();
		res.success = true;
		break;
	case mavros_msgs::CommandCode::NAV_RETURN_TO_LAUNCH:
   		ROS_INFO("GO HOME!!!!");
		//rosAdapter->flight->task(DJI::onboardSDK::Flight::TASK::TASK_GOHOME);
                TargetLocalNEDPosition.pose.position.x = HomeLocalENUPosition.pose.position.y;
                TargetLocalNEDPosition.pose.position.y = HomeLocalENUPosition.pose.position.x;
		res.success = true;
		break;
	case mavros_msgs::CommandCode::DO_MOUNT_CONTROL:
   		ROS_INFO("MOVING GIMBAL TO %.2f,%.2f,%.2f for %.2f",req.param1,req.param2,req.param3,req.param4);
//		drone->request_sdk_permission_control();
		ros::Duration(0.5).sleep();
//		drone->gimbal_angle_control(req.param1,req.param2,req.param3,req.param4);
		res.success = true;
		break;
	case mavros_msgs::CommandCode::MISSION_START:
   		if(req.param1==666) {
			//doOrbit();
		}/* else {
			ROS_INFO("START WAYPOINTS");
			drone->waypoint_navigation_send_request(newWaypointList);
		}*/
		res.success = true;
		break;
	default:
		res.success = false;
		break;
   }
   return true;
}

void GAZEBOROTORS::getfix(const sensor_msgs::NavSatFix::ConstPtr& msg)
{
  GlobalPosition.header = msg->header;
  GlobalPosition.latitude = msg->latitude;
  GlobalPosition.longitude = msg->longitude;
  GlobalPosition.altitude = msg->altitude;
  if(global_position_ref_seted == 0){ //plus init pose
      global_position_ref_seted = 1;
      GlobalPosition_ref = GlobalPosition;
  }
}

void GAZEBOROTORS::getObsL(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  //ROS_INFO("Left Range: %f", msg->range);
  LS.header.stamp    = ros::Time::now();

  float cur_ang = msg->angle_min + 0.5*M_PI;
  LS.ranges[4] = msg->range_max;
  for (auto x : msg->ranges)
  {
    if(x < LS.ranges[4])
    {
        LS.ranges[4] = x;
        LS.intensities[4] = cur_ang;
    }
    cur_ang += msg->angle_increment;
  }

}

void GAZEBOROTORS::getObsB(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  //ROS_INFO("Back Range: %f", msg->range);
  LS.header.stamp    = ros::Time::now();

  float cur_ang = msg->angle_min + M_PI;
  LS.ranges[3] = msg->range_max;
  for (auto x : msg->ranges)
  {
    if(x < LS.ranges[3])
    {
        LS.ranges[3] = x;
        LS.intensities[3] = cur_ang;
    }
    cur_ang += msg->angle_increment;
  }
}

void GAZEBOROTORS::getObsR(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  //ROS_INFO("Right Range: %f", msg->range);
  LS.header.stamp    = ros::Time::now();
   float cur_ang = msg->angle_min - 0.5*M_PI;
  LS.ranges[2] = msg->range_max;
  for (auto x : msg->ranges)
  {
    if(x < LS.ranges[2])
    {
        LS.ranges[2] = x;
        LS.intensities[2] = cur_ang;
    }
    cur_ang += msg->angle_increment;
  }
}

void GAZEBOROTORS::getObsF(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  //ROS_INFO("Front Range: %f", msg->range);
  LS.header.stamp    = ros::Time::now();
  float cur_ang = msg->angle_min;
  LS.ranges[1] = msg->range_max;
  for (auto x : msg->ranges)
  {
    if(x < LS.ranges[1])
    {
        LS.ranges[1] = x;
        LS.intensities[1] = cur_ang;
    }
    cur_ang += msg->angle_increment;
  }
}

void GAZEBOROTORS::getObsD(const sensor_msgs::LaserScan::ConstPtr& msg)
{
  //ROS_INFO("Bottom Range: %f", msg->range);
  LS.header.stamp    = ros::Time::now();
  LS.ranges[0]=msg->ranges[0];
}

void GAZEBOROTORS::getPose(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  LocalENUPosition.header = msg->header;
  LocalENUPosition.pose = msg->pose;
  LocalENUPosition.pose.position.x = msg->pose.position.y;
  LocalENUPosition.pose.position.y = msg->pose.position.x;
  if(local_position_ref_seted == 0){ //plus init pose
      local_position_ref_seted = 1;
      HomeLocalENUPosition = LocalENUPosition;
      TargetLocalNEDPosition = LocalENUPosition;
      TargetLocalNEDPosition.pose.position.x = LocalENUPosition.pose.position.y;
      TargetLocalNEDPosition.pose.position.y = LocalENUPosition.pose.position.x;
  }
  LocalENUPosition.pose.position.x-=HomeLocalENUPosition.pose.position.x;
  LocalENUPosition.pose.position.y-=HomeLocalENUPosition.pose.position.y;
}

void GAZEBOROTORS::localsetpoint(const geometry_msgs::PoseStamped::ConstPtr &pt)
{
	float dx = pt->pose.position.y-LocalENUPosition.pose.position.y;
	float dy = pt->pose.position.x-LocalENUPosition.pose.position.x;
	float dz = pt->pose.position.z;
	float yaw = 0.0;//pt->yaw;
	if(sqrt(dx*dx+dy*dy)>25.0) {
		ROS_INFO("REFUSED LOCAL SETPOINT: TOO FAR (%.2f)!", sqrt(dx*dx+dy*dy));
	} else {
                // RECEIVED ENU SO TRANSFERING TO NED
                local_position_control(pt->pose.position.y, pt->pose.position.x, pt->pose.position.z, pt->pose.orientation);
   		//ROS_INFO("Going to %.7f,%.7f,%.7f,%.7f",x, y, z, yaw);
	}
	return;
}

bool GAZEBOROTORS::fcuarm(mavros_msgs::CommandBool::Request  &req,
         mavros_msgs::CommandBool::Response &res)
{
	if(req.value) {
		sdk_permission_control(1);
		ros::Duration(0.5).sleep();
		//rosAdapter->flight->setArm(true);
	} else {
		//rosAdapter->flight->setArm(false);
		ros::Duration(0.5).sleep();
		//drone->release_sdk_permission_control();
	}
	res.success = true;
	return true;
}

bool GAZEBOROTORS::fcumode(mavros_msgs::SetMode::Request  &req,
         mavros_msgs::SetMode::Response &res)
{
	ROS_INFO("Change mode to %i with %s (not)", req.base_mode, req.custom_mode.c_str());
	return true;
}

bool GAZEBOROTORS::fcustream(mavros_msgs::StreamRate::Request  &req,
         mavros_msgs::StreamRate::Response &res)
{
	ROS_INFO("Change stream rate to %i (not)", req.message_rate);
	return true;
}

double timeintheair = 0.0;
void GAZEBOROTORS::publish()
{
  auto current_time = ros::Time::now();
  
  std_msgs::UInt8 msg;
  status.header.stamp = current_time;
  status.header.frame_id = "/world";
  mav_flight_status.publish(status);

  if(sim_batt){
    if(LocalENUPosition.pose.position.z>0.25)  // flying
        timeintheair=ros::Time::now().toSec()-takeofftime;//timeintheair+1.0/(double)ROS_RATE;
    else
        timeintheair = 0.0;
    if(timeintheair/60.0 > BATTTIME)
    {
      ROS_ERROR("LOW BATTERY: LAND!!!!");
      TargetLocalNEDPosition = LocalENUPosition;
      TargetLocalNEDPosition.pose.position.x = LocalENUPosition.pose.position.y+HomeLocalENUPosition.pose.position.y;
      TargetLocalNEDPosition.pose.position.y = LocalENUPosition.pose.position.x+HomeLocalENUPosition.pose.position.x;
      TargetLocalNEDPosition.pose.position.z = HomeLocalENUPosition.pose.position.z;
      status.landed_state = 1;
      ros::Duration(2).sleep();
    }
    sensor_msgs::BatteryState percent;
    percent.header.stamp = current_time;
    //ROS_INFO("Remaining batt pwr: %f (%fs - %i)",(double)100-timeintheair/(BATTTIME*60),timeintheair,status.landed_state);
    percent.percentage= (double)1.0-(timeintheair/(BATTTIME*60));
    if(percent.percentage<0.0)
      percent.percentage=0.0;
    mav_power_status.publish(percent);
  }

  std_msgs::Float64 rel_alt;
  rel_alt.data = LocalENUPosition.pose.position.z;
  mav_currentalt.publish(rel_alt);
  mav_currentLpos.publish(LocalENUPosition);
  /*double t_lat =0.0, t_lon=0.0;
  ned_convert_gps(LocalPosition.pose.position.x,LocalPosition.pose.position.y,t_lon,t_lat,GlobalPosition_ref.longitude,GlobalPosition_ref.latitude);
  GlobalPosition.longitude=t_lon;
  GlobalPosition.latitude=t_lat;*/
  //mav_currentpos.publish(GlobalPosition);

  obstacles.publish(LS);
  
  if(sdk_incontrol)
      set_pose.publish(TargetLocalNEDPosition);
  
  return;
}

int GAZEBOROTORS::init_parameters(ros::NodeHandle& nh_private)
{
  status.landed_state = 1;
  nh_private.param("model", model_name, std::string("m100"));
  nh_private.param("sim_batt", sim_batt, true);
  std::string tmp;
  nh_private.param("init_pose", tmp, std::string("-x 0 -y 0 -z 5"));
  /*float x = 0.0, y = 0.0, z = 0.0;
  sscanf(tmp.c_str(),"-x %f -y %f -z %f",&x,&y,&z);
  LocalENUPosition.header.frame_id = "world";
  LocalENUPosition.pose.position.x = y;
  LocalENUPosition.pose.position.y = x;
  LocalENUPosition.pose.position.z = z;
  LocalENUPosition.pose.orientation.x = 0.0;
  LocalENUPosition.pose.orientation.y = 0.0;
  LocalENUPosition.pose.orientation.z = 0.0;
  LocalENUPosition.pose.orientation.w = 1.0;
  TargetLocalNEDPosition = LocalENUPosition;
  HomeLocalENUPosition = LocalENUPosition;
  ROS_INFO("%s starting at %.3f, %.3f, %.3f", model_name.c_str(), HomeLocalENUPosition.pose.position.x, HomeLocalENUPosition.pose.position.y, HomeLocalENUPosition.pose.position.z);*/
  /*double t_lat =0.0, t_lon=0.0;
  ned_convert_gps(x,y,t_lon,t_lat,GlobalPosition_ref.longitude,GlobalPosition_ref.latitude);
  GlobalPosition.longitude=t_lon;
  GlobalPosition.latitude=t_lat;*/

  LS.ranges.resize(5);
	LS.intensities.resize(5);
	LS.header.frame_id = "base_link";
}

int GAZEBOROTORS::init_pubsub(ros::NodeHandle& nh)
{
  /*gazebo_msgs::SetPhysicsProperties  set_physics;
  ros::ServiceClient set_physics_client = nh.serviceClient<gazebo_msgs::SetPhysicsProperties>("/gazebo/set_physics_properties");
  set_physics.request.time_step=0.005;
  set_physics.request.max_update_rate=200.0;
  set_physics.request.gravity.x=0.0;
  set_physics.request.gravity.y=0.0;
  set_physics.request.gravity.z=0.0;
  set_physics.request.ode_config.auto_disable_bodies=false;
  set_physics.request.ode_config.sor_pgs_precon_iters=0;
  set_physics.request.ode_config.sor_pgs_iters=50;
  set_physics.request.ode_config.sor_pgs_w=1.3;
  set_physics.request.ode_config.sor_pgs_rms_error_tol=0.0;
  set_physics.request.ode_config.contact_surface_layer=0.001;
  set_physics.request.ode_config.contact_max_correcting_vel=100.0;
  set_physics.request.ode_config.cfm=0.0;
  set_physics.request.ode_config.erp=0.2;
  set_physics.request.ode_config.max_contacts=20;
  set_physics_client.call(set_physics);
  while(!set_physics.response.success){
      ROS_INFO("WAITING TO SET GAZEBO PHYSICS PROPERTIES");
      ros::Duration(0.05).sleep();
      set_physics_client.call(set_physics);
  }*/
  
  
  // Subscribe topics from Gazebo for the UAV
  GlobalPoseSub = nh.subscribe("/"+model_name+"/global_position/global",10, &GAZEBOROTORS::getfix, this);
  LocalPoseSub = nh.subscribe("/"+model_name+"/ground_truth_to_tf/pose",10, &GAZEBOROTORS::getPose, this);
  Obs1Sub = nh.subscribe("/"+model_name+"/obstacle_front",10, &GAZEBOROTORS::getObsF, this);
  Obs2Sub = nh.subscribe("/"+model_name+"/obstacle_left",10, &GAZEBOROTORS::getObsL, this);
  Obs3Sub = nh.subscribe("/"+model_name+"/obstacle_right",10, &GAZEBOROTORS::getObsR, this);
  Obs4Sub = nh.subscribe("/"+model_name+"/obstacle_back",10, &GAZEBOROTORS::getObsB, this);
  Obs5Sub = nh.subscribe("/"+model_name+"/obstacle_down",10, &GAZEBOROTORS::getObsD, this);
  // Advertise topics to Gzebo for UAV control
  enable_motors_client = nh.serviceClient<hector_uav_msgs::EnableMotors>("/"+model_name+"/enable_motors");
  set_pose = nh.advertise<geometry_msgs::PoseStamped>("/"+model_name+"/command/pose",10);

  // Advertise topics and services for ROSBuzz
  obstacles = nh.advertise<sensor_msgs::LaserScan>("obstacles",10);
  if(sim_batt)
    mav_power_status = nh.advertise<sensor_msgs::BatteryState>("battery",10);
  //mav_currentpos = nh.advertise<sensor_msgs::NavSatFix>("global_position/global",10);
  mav_currentLpos = nh.advertise<geometry_msgs::PoseStamped>("local_position/pose",10);
  mav_currentalt = nh.advertise<std_msgs::Float64>("global_position/rel_alt",10);
  mav_flight_status = nh.advertise<mavros_msgs::ExtendedState>("extended_state",10);
  cmd_service = nh.advertiseService("cmd/command", &GAZEBOROTORS::fcucmds, this);
  arm_service = nh.advertiseService("cmd/arming", &GAZEBOROTORS::fcuarm, this);
  mode_service = nh.advertiseService("set_mode", &GAZEBOROTORS::fcumode, this);
  stream_service = nh.advertiseService("set_stream_rate", &GAZEBOROTORS::fcustream, this);

  // Subscribe to the flight control service from ROSBuzz
  localsetpointSub = nh.subscribe("/"+model_name+"/setpoint_position/local",10, &GAZEBOROTORS::localsetpoint, this);

    return 0;
}

int main(int argc, char **argv)
{

  ros::init(argc, argv, "rotors_gazebo_bridge");

  ros::NodeHandle nh;
  ros::NodeHandle nh_priv("~");
  
  GAZEBOROTORS *gazebo_rotors = new GAZEBOROTORS(nh, nh_priv);
  int ROS_RATE = 50;
  nh_priv.getParam("ros_rate", ROS_RATE);
  ROS_INFO("Bridge between PC sim and gazebo connected");

  ros::Rate spin_rate(ROS_RATE); //Hz

  while(ros::ok())
  {
    ros::spinOnce();

    gazebo_rotors->publish();
    
    spin_rate.sleep();
  }

  //clear
  delete gazebo_rotors;
  gazebo_rotors = NULL;
    
  return 0;
}