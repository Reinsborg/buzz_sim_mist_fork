
/* ----------------------------------------------------------------
 * File: rosbuzz_observer.cpp
 * Created on: 30/04/2019
 * Author: Vivek Shankar Varadharajan
 * Description: ROS node to auto takeoff, waypoint assignment and auto kill ros nodes 
 *              for batch experimental evaluations.
 *
 * Copyright MIST Laboratory. All rights reserved.
 ------------------------------------------------------------------ */

#include <ros/ros.h>
#include "sensor_msgs/NavSatFix.h"
#include "rosbuzz/neigh_pos.h"
#include "mavros_msgs/GlobalPositionTarget.h"
#include "mavros_msgs/CommandCode.h"
#include "mavros_msgs/CommandLong.h"
#include "mavros_msgs/CommandBool.h"
#include "mavros_msgs/State.h"
#include "mavros_msgs/BatteryStatus.h"
#include <mavros_msgs/ParamGet.h>
#include <mavros_msgs/ParamValue.h>
#include <mavros_msgs/Mavlink.h>

#include <gazebo_msgs/ModelState.h>
#include <gazebo_msgs/GetModelState.h>

#include "std_msgs/String.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/Float64.h"
#include <ros/ros.h>
#include <stdio.h>
#include <cstdlib>
#include <signal.h>
#include <chrono>
#include <thread>

#include <iostream>

using namespace std;

const int TakeOffTime = 200;
const int WayPointTime = 300;
const int ExperimentEndTime = 1200;


sensor_msgs::NavSatFix globalpos;

int main(int argc, char *argv[])
	/** Description: Observer node for autotakeoff at a specified time, waypoint call 
  at specified time and kill all nodes once the experimental time has expired.
	******************************************************************* */
{
  ros::init(argc, argv, "rosbuzz_observer");
  ROS_INFO("rosbuzz_observer");
  ros::NodeHandle nh("~");
  ros::Rate loop_rate(10); //loops per sec.

  
  ros::spinOnce();
  
  ros::ServiceClient BuzzcmdSvr = nh.serviceClient<mavros_msgs::CommandLong>("/robot1/buzzcmd");
  

  long int TimeSteps = 0;
  while(ros::ok() && TimeSteps < ExperimentEndTime)
  {
    ros::spinOnce();
    TimeSteps += 1;
    if(TimeSteps == TakeOffTime){
       mavros_msgs::CommandLong takeoffcmd;
       takeoffcmd.request.command = mavros_msgs::CommandCode::NAV_TAKEOFF;
      if (BuzzcmdSvr.call(takeoffcmd)){
        if (takeoffcmd.response.success == 1)
          ROS_WARN("OBSERVER: Takeoff called sucessfully!");
        else
          ROS_WARN("OBSERVER: Takeoff call FAILED!");
      }
    }


    if(TimeSteps == WayPointTime){
      mavros_msgs::CommandLong wpcmd;
      wpcmd.request.command = mavros_msgs::CommandCode::NAV_WAYPOINT;
      // wp1
      wpcmd.request.param1 = 51;
      wpcmd.request.param5 = 29.020924;
      wpcmd.request.param6 = -13.715563;
      wpcmd.request.param7 = 10.00;
      if (BuzzcmdSvr.call(wpcmd)){
        if (wpcmd.response.success == 1)
          ROS_WARN("OBSERVER: WP 1 set sucessfully!");
        else
          ROS_WARN("OBSERVER: WP 1 call FAILED!");
      }
    }
    else if(TimeSteps == WayPointTime+1){
      mavros_msgs::CommandLong wpcmd;
      wpcmd.request.command = mavros_msgs::CommandCode::NAV_WAYPOINT;
      // Wp2
      wpcmd.request.param1 = 52;
      wpcmd.request.param5 = 29.021023;
      wpcmd.request.param6 = -13.715395;
      wpcmd.request.param7 = 10.00;
      if (BuzzcmdSvr.call(wpcmd)){
        if (wpcmd.response.success == 1)
          ROS_WARN("OBSERVER: WP 2 set sucessfully!");
        else
          ROS_WARN("OBSERVER: WP 2 call FAILED!");
      }
    }
    // else if(TimeSteps == WayPointTime+2){
    //   mavros_msgs::CommandLong wpcmd;
    //   wpcmd.request.command = mavros_msgs::CommandCode::NAV_WAYPOINT;
    //   // wp3
    //   wpcmd.request.param1 = 51;
    //   wpcmd.request.param5 = 29.020927;
    //   wpcmd.request.param6 = -13.715398;
    //   wpcmd.request.param7 = 10.00;
    //   if (BuzzcmdSvr.call(wpcmd)){
    //     if (wpcmd.response.success == 1)
    //       ROS_WARN("OBSERVER: WP 3 set sucessfully!");
    //     else
    //       ROS_WARN("OBSERVER: WP 3 call FAILED!");
    //   }
    // }
    
    loop_rate.sleep();
  }
  system("rosnode kill -a");
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));
  system("killall -9 rosmaster; killall -9 roscore");
  return 0;
}
