/** @file rcclient.cpp
 *  @version 1
 *  @date Setember 26th, 2016
 *
 *  @brief
 *  All the required RC commands for ROS control. 
 *
 *  @copyright 2016 MistLab. All rights reserved.
 *
 */

#include "sensor_msgs/NavSatFix.h"
#include "rosbuzz/neigh_pos.h"
#include "mavros_msgs/GlobalPositionTarget.h"
#include "mavros_msgs/CommandCode.h"
#include "mavros_msgs/CommandLong.h"
#include "mavros_msgs/CommandBool.h"
#include "mavros_msgs/State.h"
#include "mavros_msgs/BatteryStatus.h"
#include <mavros_msgs/ParamGet.h>
#include <mavros_msgs/ParamValue.h>

#include <gazebo_msgs/ModelState.h>
#include <gazebo_msgs/GetModelState.h>
#include <gazebo_msgs/SetModelState.h>

#include "std_msgs/String.h"
#include "std_msgs/UInt8.h"
#include <ros/ros.h>
#include <stdio.h>
#include <cstdlib>

ros::ServiceClient mav_client;
ros::ServiceClient arm_client;
ros::ServiceClient sendrc_client;
ros::Publisher bag_client;
ros::Publisher userspos;
ros::ServiceClient get_userspos;
ros::ServiceClient set_userspos;

mavros_msgs::CommandLong cmd_srv;
mavros_msgs::CommandBool arm_srv;
std_msgs::String bag_cmd;
bool record=false, newcmd=false;
uint8_t robotID = 0;
double start_secs = 0;

gazebo_msgs::ModelState home;
gazebo_msgs::ModelState current;
gazebo_msgs::SetModelState setModelState;
gazebo_msgs::GetModelState getModelState;

	#define EARTH_RADIUS (double) 6371000.0
	#define DEG2RAD(DEG) ((DEG)*((M_PI)/(180.0)))
	#define RAD2DEG(DEG) ((DEG)*((180.0)/(M_PI)))

/*void RCCallback(const dji_sdk::TransparentTransmissionData rcdata)
{
  uint8_t cmdID = rcdata.data[0];
  union {
	uint8_t bytes[4];
	float f;
	int32_t i;
  }arg1={.bytes={rcdata.data[1],rcdata.data[2],rcdata.data[3],rcdata.data[4]}}, arg2={.bytes={rcdata.data[5],rcdata.data[6],rcdata.data[7],rcdata.data[8]}}, arg3={.bytes={rcdata.data[9],rcdata.data[10],rcdata.data[11],rcdata.data[12]}};
  ROS_INFO("RCCLIENT - Received: [%i] with [%.6f,%.6f,%.6f]", cmdID,arg1.i/pow(10,7),arg2.i/pow(10,7),arg3.i/pow(10,7));
  cmd_srv.request.param1 = 0;
  cmd_srv.request.command = 0;
  newcmd=false;
  switch(cmdID)
  {
	case 1: //SDKVersion
	case 2: //ObtainControl
	case 3: //ReleaseControl
	case 4: // ??
		ROS_INFO("RCCLIENT - Control command not required");
		break;
	case 5: //Arm
		cmd_srv.request.command = mavros_msgs::CommandCode::CMD_COMPONENT_ARM_DISARM;
		cmd_srv.request.param1 = 1;
		newcmd=true;
		break;
	case 6: //Disarm
		cmd_srv.request.command = mavros_msgs::CommandCode::CMD_COMPONENT_ARM_DISARM;
		cmd_srv.request.param1 = 0;
		newcmd=true;
		break;
	case 7: //Take Off
		cmd_srv.request.command = mavros_msgs::CommandCode::NAV_TAKEOFF;
		newcmd=true;
		break;
	case 8: //Land
		cmd_srv.request.command = mavros_msgs::CommandCode::NAV_LAND;
		newcmd=true;
		break;
	case 9: //Go home
		cmd_srv.request.command = mavros_msgs::CommandCode::NAV_RETURN_TO_LAUNCH;
		newcmd=true;
		break;
	case 10: //Take photo
	case 11: //Start video
	case 12: //Stop video
		ROS_INFO("RCCLIENT - Media command not implemented yet.");
		break;
	case 20: //Move gimbal
		cmd_srv.request.command = mavros_msgs::CommandCode::CMD_DO_MOUNT_CONTROL;
		cmd_srv.request.param1 = (float)arg1.i/pow(10,7);
		cmd_srv.request.param2 = (float)arg2.i/pow(10,7);
		cmd_srv.request.param3 = (float)arg3.i/pow(10,7);
		cmd_srv.request.param4 = 20.0;
		newcmd=true;
		break;
	case 21: //Go To
		cmd_srv.request.command = mavros_msgs::CommandCode::NAV_WAYPOINT;
		cmd_srv.request.param5 = (float)arg1.i/pow(10,7);
		cmd_srv.request.param6 = (float)arg2.i/pow(10,7);
		cmd_srv.request.param7 = (float)arg3.i/pow(10,7);
		newcmd=true;
		break;
	case 22: //Launch Go To list
		cmd_srv.request.command = mavros_msgs::CommandCode::CMD_MISSION_START;
		newcmd=true;
		break;
	case 23: //Scan Object
		cmd_srv.request.command = mavros_msgs::CommandCode::CMD_MISSION_START;
		cmd_srv.request.param1 = 666;
		newcmd=true;
		break;
	case 24:
		ROS_INFO("RCCLIENT - Start a rosbag!");
		if(!record)
		{
			bag_cmd.data="record";
			bag_client.publish(bag_cmd);
			record=true;
		}
		break;
	case 25:
		ROS_INFO("RCCLIENT - Stop the rosbag!");
		if(record)
		{
			bag_cmd.data="stop";
			bag_client.publish(bag_cmd);
			record=false;
		}
		break;
	case 32:
		ROS_INFO("RCCLIENT - Shutdown the Manifold!");
		system("dbus-send --system --dest=\"org.freedesktop.ConsoleKit\" --print-reply --type=method_call /org/freedesktop/ConsoleKit/Manager org.freedesktop.ConsoleKit.Manager.Stop");
		break;
	case 254:
		uint8_t n = rcdata.data[1];	uint8_t id = 0;
		//ROS_INFO("RCCLIENT - Got %i users GPS array!", n);
		auto current_time = ros::Time::now();
                rosbuzz::neigh_pos users_pos_array; //neigh_pos_array.clear();
                users_pos_array.header.frame_id = "/world";
                users_pos_array.header.stamp = current_time;
		for(int i=0;i<n;i++) {
			sensor_msgs::NavSatFix newuser;
			id = rcdata.data[i*13+2];
            newuser.position_covariance_type=id; //custom robot id storage
			union {uint8_t bytes[4]; int32_t i;}lon={.bytes={rcdata.data[i*13+3],rcdata.data[i*13+4],rcdata.data[i*13+5],rcdata.data[i*13+6]}};
			newuser.longitude=lon.i/pow(10,7);
			union {uint8_t bytes[4]; int32_t i;}lat={.bytes={rcdata.data[i*13+7],rcdata.data[i*13+8],rcdata.data[i*13+9],rcdata.data[i*13+10]}};
			newuser.latitude=lat.i/pow(10,7);
			union {uint8_t bytes[4]; int32_t i;}alt={.bytes={rcdata.data[i*13+11],rcdata.data[i*13+12],rcdata.data[i*13+13],rcdata.data[i*13+14]}};
			newuser.altitude=alt.i/pow(10,7);
			//ROS_INFO("Got %i user at: %0.7f, %0.7f, %0.7f",id,newuser.longitude,newuser.latitude,newuser.altitude);
			newuser.header.stamp = current_time;
                        newuser.header.frame_id = "/world";
                        users_pos_array.pos_neigh.push_back(newuser);
		}
		userspos.publish(users_pos_array);
		break;
  }
  if(newcmd)
  {
	if (mav_client.call(cmd_srv))
 		{
			if(cmd_srv.response.success==1){ROS_INFO("RCCLIENT - DJI MISTLab MavFC (or Buzz) replied with success."); }
   			else{ ROS_ERROR("RCCLIENT - Failed to call DJI MISTLab FC (or Buzz controller) service"); }
		}
   	else{ ROS_ERROR("RCCLIENT - Failed to call DJI MISTLab FC (or Buzz controller) service"); }
  }
}

void updateNeighbors(const rosbuzz::neigh_pos data)
{
	dji_sdk::SendDataToRemoteDevice sendrc_srv;
	double longlat = 0; float alt = 0; int8_t id =0;
	int n = data.pos_neigh.size();
//	ROS_INFO("Neighbors array size: %i\n", n);
	if(n>0)
	{
		uint8_t *rcdata2send  = reinterpret_cast<uint8_t*>(&n);
		std::copy(rcdata2send, rcdata2send+sizeof(uint8_t), std::back_inserter(sendrc_srv.request.data));
		rcdata2send  = reinterpret_cast<uint8_t*>(&robotID);
		std::copy(rcdata2send, rcdata2send+sizeof(uint8_t), std::back_inserter(sendrc_srv.request.data));
		for(int it=0; it<n; ++it)
		{
			id = data.pos_neigh[it].position_covariance_type;
			rcdata2send = reinterpret_cast<uint8_t*>(&id);
			std::copy(rcdata2send, rcdata2send+sizeof(int8_t), std::back_inserter(sendrc_srv.request.data));
			longlat = data.pos_neigh[it].longitude;
			rcdata2send = reinterpret_cast<uint8_t*>(&longlat);
			std::copy(rcdata2send, rcdata2send+sizeof(double), std::back_inserter(sendrc_srv.request.data));
			longlat = data.pos_neigh[it].latitude;
			rcdata2send = reinterpret_cast<uint8_t*>(&longlat);
			std::copy(rcdata2send, rcdata2send+sizeof(double), std::back_inserter(sendrc_srv.request.data));
			alt = data.pos_neigh[it].altitude;
			rcdata2send = reinterpret_cast<uint8_t*>(&alt);
			std::copy(rcdata2send, rcdata2send+sizeof(float), std::back_inserter(sendrc_srv.request.data));
			id = 50;	//false battery status
			rcdata2send = reinterpret_cast<uint8_t*>(&id);
			std::copy(rcdata2send, rcdata2send+sizeof(int8_t), std::back_inserter(sendrc_srv.request.data));
		}

		if (sendrc_client.call(sendrc_srv))
		{
			if(sendrc_srv.response.result!=1)
			{ ROS_ERROR("RCCLIENT - Failed to send data to RC"); }
		}
	}
}*/

// WGS84 constants
/*static const double equatorial_radius = 6378137.0;
static const double flattening = 1.0/298.257223563;
static const double excentrity2 = 2*flattening - flattening*flattening;*/

// default reference position
static const double DEFAULT_REFERENCE_LATITUDE  = 45.457817;
static const double DEFAULT_REFERENCE_LONGITUDE = -73.636075;
static const double DEFAULT_REFERENCE_HEADING   = 0.0;
static const double DEFAULT_REFERENCE_ALTITUDE  = 0.0;

void pubGPS()
{
    // calculate earth radii
  /*double temp = 1.0 / (1.0 - excentrity2 * sin(DEFAULT_REFERENCE_LATITUDE * M_PI/180.0) * sin(DEFAULT_REFERENCE_LATITUDE * M_PI/180.0));
  double prime_vertical_radius = equatorial_radius * sqrt(temp);
  double radius_north = prime_vertical_radius * (1 - excentrity2) * temp;
  double radius_east  = prime_vertical_radius * cos(DEFAULT_REFERENCE_LATITUDE * M_PI/180.0);*/
  
    auto current_time = ros::Time::now();
    rosbuzz::neigh_pos users_pos_array; //neigh_pos_array.clear();
    users_pos_array.header.frame_id = "/world";
    users_pos_array.header.stamp = current_time;
    int n=1;
    for(int i=0;i<n;i++) {
            sensor_msgs::NavSatFix newuser;
			newuser.position_covariance_type=i; //custom robot id storage

			/*double d_lon = gps_t_lon - gps_r_lon;
			double d_lat = gps_t_lat - gps_r_lat;
			ned_x = DEG2RAD(d_lat) * EARTH_RADIUS;
			ned_y = DEG2RAD(d_lon) * EARTH_RADIUS * cos(DEG2RAD(gps_t_lat));*/
			double d_lat = RAD2DEG(current.pose.position.x / EARTH_RADIUS);
    		d_lat = std::floor(d_lat * 1000000) / 1000000;
            newuser.latitude = DEFAULT_REFERENCE_LATITUDE + d_lat;
			double d_lon = RAD2DEG(current.pose.position.y / EARTH_RADIUS / cos(DEG2RAD(newuser.latitude)));
    		d_lon = std::floor(d_lon * 1000000) / 1000000;
			newuser.longitude = DEFAULT_REFERENCE_LONGITUDE + d_lon;
            newuser.altitude = DEFAULT_REFERENCE_ALTITUDE + current.pose.position.z;
            /*newuser.longitude = DEFAULT_REFERENCE_LONGITUDE - (-sin(DEFAULT_REFERENCE_HEADING) * current.pose.position.x + cos(DEFAULT_REFERENCE_HEADING) * current.pose.position.y) / equatorial_radius * 180.0/M_PI;
            newuser.latitude = DEFAULT_REFERENCE_LATITUDE  + ( cos(DEFAULT_REFERENCE_HEADING) * current.pose.position.x + sin(DEFAULT_REFERENCE_HEADING) * current.pose.position.y) / equatorial_radius * 180.0/M_PI;
            newuser.altitude = DEFAULT_REFERENCE_ALTITUDE  + current.pose.position.z;*/
            newuser.header.stamp = current_time;
            newuser.header.frame_id = "/world";
            users_pos_array.pos_neigh.push_back(newuser);
    }
    userspos.publish(users_pos_array);
}

void move_people()
{
    if(set_userspos)
    {
	  double elapsedTime = ros::Time::now().toSec() - start_secs;
      if(elapsedTime > 20.0)
	  	current.pose.position.x = 3*1000.0/3600.0 * (elapsedTime-20.0);
      setModelState.request.model_state = current;
      set_userspos.call(setModelState);
      // std::cout << "service called" << std::endl;
    }
    pubGPS();
}

int main(int argc, char *argv[])
{
    int main_operate_code = 0;
    bool valid_flag = false;
    bool err_flag = false;
    std::string fcclient_name;
    ros::init(argc, argv, "sdk_RCclient");
    ROS_INFO("sdk_service_RCclient");
    ros::NodeHandle nh("~");
    ros::Rate loop_rate(1); //loops per sec.

	/*Obtain fc client name from parameter server
        if(nh.getParam("fcclient_name", fcclient_name)) {ROS_INFO("FC CLIENT NAME %s",fcclient_name.c_str());}
        else {ROS_ERROR("Provide a fc client name in Launch file"); system("rosnode kill dji_sdk_rcclient");}*/

    uint8_t userData = 0;
    ros::spinOnce();

    bag_cmd.data="idle";
    mav_client = nh.serviceClient<mavros_msgs::CommandLong>(fcclient_name);
    arm_client = nh.serviceClient<mavros_msgs::CommandBool>("/dji_mavarm");
//    sendrc_client = nh.serviceClient<dji_sdk::SendDataToRemoteDevice>("/dji_sdk/send_data_to_remote_device");
    bag_client = nh.advertise<std_msgs::String>("/record/bagcmd",5);
    userspos = nh.advertise<rosbuzz::neigh_pos>("/robot1/users_pos", 5);
//    ros::Subscriber rc_sub = nh.subscribe<dji_sdk::TransparentTransmissionData>("/dji_sdk/data_received_from_remote_device", 5, RCCallback);
//    ros::Subscriber fleetpos_sub = nh.subscribe<rosbuzz::neigh_pos>("/neighbours_pos", 5, updateNeighbors);
    set_userspos = nh.serviceClient<gazebo_msgs::SetModelState>("/gazebo/set_model_state");
    get_userspos = nh.serviceClient<gazebo_msgs::GetModelState>("/gazebo/get_model_state");

//    ros::Subscriber testpos_sub = nh.subscribe<sensor_msgs::NavSatFix>("/global_position", 5, testpos_callback);
     ros::Duration(0.5).sleep();
     
    if(get_userspos)
    {
      getModelState.request.model_name = (std::string)"person_walking" ;
      getModelState.request.relative_entity_name = (std::string)"world" ;
      get_userspos.call(getModelState) ;

      home.model_name = (std::string)"person_walking" ;
      home.pose.position = getModelState.response.pose.position ;
      home.pose.orientation = getModelState.response.pose.orientation ;
      home.twist = getModelState.response.twist ;
      current = home;
    }

	start_secs = ros::Time::now().toSec();

    while(ros::ok())
    {
        move_people();
    	ros::spinOnce();
		loop_rate.sleep();
    }
    return 0;
}
