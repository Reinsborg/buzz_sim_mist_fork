
/* ----------------------------------------------------------------
 * File: zooidstcp.cpp
 * Created on: 20/11/2018
 * Author: David St-Onge
 * Description: ROS node for serial control of the Zooids
 *
 * Copyright MIST Laboratory. All rights reserved.
 ------------------------------------------------------------------ */

#include "sensor_msgs/NavSatFix.h"
#include "rosbuzz/neigh_pos.h"
#include "mavros_msgs/GlobalPositionTarget.h"
#include "mavros_msgs/CommandCode.h"
#include "mavros_msgs/CommandLong.h"
#include "mavros_msgs/CommandBool.h"
#include "mavros_msgs/State.h"
#include "sensor_msgs/BatteryState.h"
#include <mavros_msgs/ParamGet.h>
#include <mavros_msgs/ParamValue.h>
#include <mavros_msgs/Mavlink.h>

#include "std_msgs/String.h"
#include "std_msgs/UInt8.h"
#include "std_msgs/UInt32.h"
#include "std_msgs/Float64.h"
#include "std_msgs/UInt16MultiArray.h"
#include <ros/ros.h>

// System includes
#include <iostream>
#include <sstream>
#include <stdio.h>   	// Standard input/output definitions
#include <string.h>  	// String function definitions
#include <unistd.h>  	// UNIX standard function definitions
#include <fcntl.h>   	// File control definitions
#include <errno.h>   	// Error number definitions
#include <termios.h> 	// POSIX terminal control definitions (struct termios)
#include <system_error>	// For throwing std::system_error

ros::ServiceClient mav_client;
ros::Publisher event_pub;

uint8_t robotID = 0;
int ROS_RATE = 120;
int print_time = ROS_RATE;
bool wpctr = 1;
bool debug = 0;
bool isFlying = 0;
bool BUFlying = 0;
bool isIDLE = 0;
double origin_gps[2] = {29.021055, -13.715155};//{29.020971, -13.714859};
float deploy_last[2] = {0.0, 0.0};
float origin_proj[2] = {70, 770};//{856, 595};
float projo_minmax[4] = {70, 955, 240, 770};
float projo_size[2] = {865, 530};
float field_size[2] = {48, 35};//{180, 100};
float zooids_xscale = projo_size[0]/field_size[0];
float zooids_yscale = projo_size[1]/field_size[1];

struct Robot
{ 
    float x; 
    float y;
    float batt;
    float yaw;
    float WPat;
    Robot(): x( origin_proj[0] ), y( origin_proj[1] ), yaw( 0 ), batt ( 100 ), WPat ( -1 ){ }
    Robot( float x, float y, float yaw, float batt, int WPat ): x( x ), y( y ), yaw( yaw ), batt ( batt ), WPat ( WPat ) { }
};

struct DistS
{
  float lastx, lasty;
  std::vector<float> hist;
};

std::map<int, std::vector<float>> ComHist;
std::map<int, DistS> DistHist;
std::map<int, Robot> drones;
std::map<int, Robot> targets;
std::map<int, int> hotspots;
std::map<int, Robot> zooids;
std::map<int, int> zooids_wptimer;
std::map<int, int> active;

struct StatusMessage
{
  uint16_t positionX;
  uint16_t positionY;
  int16_t orientation;
  uint8_t state;
  bool reached;
  uint8_t batteryLevel;
};

struct PositionControlMessage {
  uint16_t positionX;
  uint16_t positionY;
  uint8_t colorRed;
  uint8_t colorGreen;
  uint8_t colorBlue;
  uint8_t preferredSpeed;
  int16_t orientation;
  bool isFinalGoal;
  uint8_t empty;
  PositionControlMessage( uint16_t positionX, uint16_t positionY, uint8_t colorRed, uint8_t colorGreen, uint8_t colorBlue, uint8_t preferredSpeed, int16_t orientation, bool isFinalGoal): positionX (positionX), positionY (positionY), colorRed (colorRed), colorGreen (colorGreen), colorBlue (colorBlue), preferredSpeed (preferredSpeed), orientation (orientation), isFinalGoal (isFinalGoal){ }
};

using namespace std;
const string device_ = "/dev/ttyACM0";

// FROM zooids table
//  coordinatesMinX   80.0f
//  coordinatesMaxX   950.0f
//  coordinatesMinY   250.0f
//  coordinatesMaxY   800.0f

sensor_msgs::NavSatFix globalpos;

#define EARTH_RADIUS (double)6371000.0
#define DEG2RAD(DEG) (double)((DEG) * ((M_PI) / (180.0)))
#define RAD2DEG(RAD) (double)((RAD) * ((180.0) / (M_PI)))

float constrainAngle(float x)
/*
/ Wrap the angle between -pi, pi
----------------------------------------------------------- */
{
  x = fmod(x + M_PI, 2 * M_PI);
  if (x < 0.0)
    x += 2 * M_PI;
  return x - M_PI;
}

void ned_from_gps(double nei[], double out[], double cur[])
/*
/ Compute NED cartesian coordinates from 2 GPS set of coordinates
/----------------------------------------*/
{
  double d_lon = nei[1] - cur[1];
  double d_lat = nei[0] - cur[0];
  out[0] = DEG2RAD(d_lat) * EARTH_RADIUS;
  out[1] = DEG2RAD(d_lon) * EARTH_RADIUS * cos(DEG2RAD(nei[0]));
  //out[0] = sqrt(ned_x * ned_x + ned_y * ned_y);
  //out[1] = constrainAngle(atan2(ned_y, ned_x));
}

void ned_to_gps(double ned[], double gps[])
{
	double d_lat = RAD2DEG(ned[0] / EARTH_RADIUS);
  gps[0] = d_lat + origin_gps[0];
	double d_lon = RAD2DEG(ned[1] / (EARTH_RADIUS * cos(DEG2RAD(gps[0]))));
  gps[1] = d_lon + origin_gps[1];
}

void scalefromdrones(double ned[], int T[])
{
  T[0] = (field_size[0] + ned[1]) * zooids_xscale + origin_proj[0];
  if(T[0] < projo_minmax[0])
    T[0] = projo_minmax[0];
  if(T[0] > projo_minmax[1])
    T[0] = projo_minmax[1];
  T[1] = origin_proj[1] + ned[0] * zooids_yscale;
  if(T[1] < projo_minmax[2])
    T[1] = projo_minmax[2];
  if(T[1] > projo_minmax[3])
    T[1] = projo_minmax[3];
}

void scaletodrones(double ned[], int T[])
{
  ned[0] = (T[1] - origin_proj[1])/zooids_yscale;
  ned[1] = (T[0] - origin_proj[0])/zooids_xscale - field_size[0];
}

void updateNeighbors(const rosbuzz::neigh_pos data)
	/** Description: parse the neighbors gps msg, create a string and store it
	******************************************************************* */
{
	int nb_uavs = data.pos_neigh.size();
  
  for(int i=0; i<nb_uavs; i++){
    int uid = data.pos_neigh[i].position_covariance_type;
    double ned[2], gps[2];
    gps[0] = data.pos_neigh[i].latitude;
    gps[1] = data.pos_neigh[i].longitude;
    ned_from_gps(gps, ned, origin_gps);
    int T[2];
    scalefromdrones(ned,T);

    Robot Dtmp(T[0], T[1], 0.0, 100.0, -1);
    //ROS_INFO("Take drone %i: %f,%f (%f, %f)",uid, Dtmp.x, Dtmp.y, ned[0], ned[1]);

    map<int, Robot>::iterator it = drones.find(uid);
    if (it != drones.end())
      drones.erase(it);
    drones.insert(make_pair(uid, Dtmp));

    map<int, int>::iterator ita = active.find(uid);
    if (ita == active.end())
      active.insert(make_pair(uid, 1));
  }
  
}

void sendevent(int id)
{
  std_msgs::UInt32 msg;
  msg.data=id;
  event_pub.publish(msg);
}

void updateTargets(const rosbuzz::neigh_pos data)
	/** Description: parse the neighbors gps msg, create a string and store it
	******************************************************************* */
{
	int nb_targets = data.pos_neigh.size();
  
  for(int i=0; i<nb_targets; i++){
    int uid = data.pos_neigh[i].position_covariance_type;
    double ned[2], gps[2];
    gps[0] = data.pos_neigh[i].latitude;
    gps[1] = data.pos_neigh[i].longitude;
    ned_from_gps(gps, ned, origin_gps);
    int T[2];
    scalefromdrones(ned,T);

    Robot Dtmp(T[0], T[1], 0.0, 100.0, -1);
    //ROS_INFO("Show target %i: %f,%f (%f, %f)",uid, Dtmp.x, Dtmp.y, ned[0], ned[1]);

    map<int, Robot>::iterator it = targets.find(uid);
    if (it != targets.end())
      targets.erase(it);
    targets.insert(make_pair(uid, Dtmp));
  }
  
}

void updateBatt(const sensor_msgs::BatteryState msg)
{
  map<int, Robot>::iterator it;
  for(it = drones.begin(); it != drones.end(); it++) {
    (it->second).batt = msg.percentage*100;
  }
}

void updateNeiBatt(const mavros_msgs::Mavlink msg)
{
  std::vector<long unsigned int> results = msg.payload64;
  int nb = results.size()/5;
  for(int i=0; i<nb; i++) {
    int did = results[i*5];
    map<int, Robot>::iterator it = drones.find(did);
    if (it != drones.end())
      (it->second).batt = results[i*5+2];
  }
}

void deadCFs(const std_msgs::UInt16MultiArray msg)
{
	int nb_deaths = msg.data.size();
  
  for(int i=0; i<nb_deaths; i++){
    map<int, int>::iterator it = active.find(msg.data[i]);
    if (it != active.end())
      active.erase(it);
    active.insert(make_pair(msg.data[i], 0));
    //ROS_WARN("Got a dead CF:%i",msg.data[i]);
  }
}

void updateState(const std_msgs::String msg)
{
  /*if(msg.data=="BARRIERWAIT")
      return;
  if(msg.data=="IDLE")
      isIDLE = true;
  else
      isIDLE = false;
  bool tmp = true;
  if(msg.data=="TURNEDOFF" || msg.data=="STOP")
      tmp = false;
  if(tmp==isFlying)
      return;
  else
      isFlying = tmp;*/
}

termios GetTermios(int fileDesc_)
{
		struct termios tty;
		memset(&tty, 0, sizeof(tty));

		// Get current settings (will be stored in termios structure)
		if(tcgetattr(fileDesc_, &tty) != 0)
		{
			// Error occurred
			ROS_ERROR("Could not get terminal attributes for %s: %s", device_.c_str(), strerror(errno));
		}

		return tty;
}

void SetTermios(int fileDesc_, termios myTermios)
{
  // Flush port, then apply attributes
  tcflush(fileDesc_, TCIFLUSH);

  if(tcsetattr(fileDesc_, TCSANOW, &myTermios) != 0)
  {
    // Error occurred
    ROS_ERROR("Could not apply terminal attributes for %s, %s", device_.c_str() , strerror(errno));
  }
  // Successful!
}

void ConfigureTermios(int fileDesc_)
{
		ROS_INFO("Configuring COM port %s",  device_.c_str());

		//================== CONFIGURE ==================//
		termios tty = GetTermios(fileDesc_);
		//================= (.c_cflag) ===============//
		tty.c_cflag     &=  ~PARENB;       	// No parity bit is added to the output characters
		tty.c_cflag     &=  ~CSTOPB;		// Only one stop-bit is used
		tty.c_cflag     &=  ~CSIZE;			// CSIZE is a mask for the number of bits per character
		tty.c_cflag     |=  CS8;			// Set to 8 bits per character
		tty.c_cflag     &=  ~CRTSCTS;       // Disable hadrware flow control (RTS/CTS)
		tty.c_cflag     |=  CREAD | CLOCAL;     	

    cfsetispeed(&tty, B230400);
    //===================== (.c_oflag) =================//
		tty.c_oflag     =   0;              // No remapping, no delays
		tty.c_oflag     &=  ~OPOST;			// Make raw
		//================= CONTROL CHARACTERS (.c_cc[]) ==================//
    tty.c_cc[VTIME] = 0;
    tty.c_cc[VMIN] = 0;
    //======================== (.c_iflag) ====================//
		tty.c_iflag   &= ~(IXON | IXOFF | IXANY);			// Turn off s/w flow ctrl
		tty.c_iflag 	&= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);
		//=========================== LOCAL MODES (c_lflag) =======================//
		// Canonical input is when read waits for EOL or EOF characters before returning. In non-canonical mode, the rate at which
		// read() returns is instead controlled by c_cc[VMIN] and c_cc[VTIME]
		tty.c_lflag		&= ~ICANON;								// Turn off canonical input, which is suitable for pass-through
		tty.c_lflag		&= ~ECHOE;								// Turn off echo erase (echo erase only relevant if canonical input is active)
		tty.c_lflag		&= ~ECHONL;								//
		tty.c_lflag		&= ~ISIG;								// Disables recognition of INTR (interrupt), QUIT and SUSP (suspend) characters

		SetTermios(fileDesc_, tty);
}

int send_PositionControlMessage(int fileDesc_, uint x, uint y, uint r, uint g, uint b, uint zid){
  int msg_size = 18;
  uint8_t data[msg_size];

  PositionControlMessage Pmsg(x, y, r, g, b, 35, 0, 1);
  data[0] = '~';
  data[1] = 6;  //PositionControlMessage
  data[2] = (uint8_t)(zid);
  data[3] = 13;
  memcpy(&data[4], &Pmsg, 13);
  data[17] = '!';

  //ROS_INFO("Sending target (%i) %i", Ztarget, it->first);

  int writeResult = write(fileDesc_, data, msg_size);

  // Check status
  if (writeResult == -1) {
    ROS_ERROR("Zooids serial write error!");
    throw std::system_error(EFAULT, std::system_category());
  }

  return writeResult;
}

int blink_time = 0;
int sendDronestoZooids(int fileDesc_)
{
  int r,g,b,x,y;
  int writeResult = -1;

  map<int, Robot>::iterator it;
  for (it = drones.begin(); it != drones.end(); ++it)
  {
    //float dist = 500;
    map<int, Robot>::iterator itz = zooids.find(it->first - 1); // Zooids ids start at 0
    if (itz == zooids.end())
      continue;
    /*if (itz != zooids.end()) {
      dist = sqrt(((itz->second).x-(it->second).x)*((itz->second).x-(it->second).x)+((itz->second).y-(it->second).y)*((itz->second).y-(it->second).y));
    }*/

    g = 50; //(it->second).batt/2;
    r = 0;
    b = 0;
    if((it->second).batt < 40) {
      r = 35;
      g = 0;
      b = 0;
    }
    /*if(blink_time<10 && !wpctr){
        r = 0; b = 0; g = 0;
    }
    blink_time++;
    if(blink_time>20)
      blink_time=0;*/

    /*map<int, int>::iterator itT = zooids_wptimer.find(it->first - 1);
    if (itT != zooids_wptimer.end()){  // This zooids is currently used for waypoint command
      if((itT->second)<0){
        b = 50;
        g = 0;
        r = 0;
      }
    }*/

    if(itz->second.WPat >= 0){
      b = 50;
      g = 0;
      r = 0;
      itz->second.WPat = itz->second.WPat -1;
    }

    x = (it->second).x;
    y = (it->second).y;
    //ROS_INFO("Zooid %i go %f %f", it->first , (it->second).x, (it->second).y);
    
    map<int, int>::iterator ita = active.find(it->first);
    if (ita != active.end() && ita->second==0){
      x = (itz->second).x;
      y = (itz->second).y;
      r = 50.0;
      g = 50.0;
      b = 50.0;
    }
    /*if(dist < 30.0 && dist > 5 && wpctr){
      //ROS_INFO("Not enough distance (%f).", dist);
      //Pmsg.positionX=(itz->second).x;
      //Pmsg.positionY=(itz->second).y;
    }*/

    //ROS_INFO("Sending for %i", it->first);

    writeResult = send_PositionControlMessage(fileDesc_, x, y, r, g, b, (it->first) - 1);
  }
  return writeResult;
}

int sendTargetstoZooids(int fileDesc_)
{
  int r,g,b,x,y;
  int writeResult = -1;

  
  // ------------------------------------------
  //      Send position of targets discovered
  int Ztarget = 0;
  if(!targets.empty()) {
    map<int, Robot>::iterator itT;
    for(itT = targets.begin(); itT != targets.end(); itT++) {
      x = (itT->second).x;
      y = (itT->second).y;
      r = 35;
      g = 0;
      b = 0;

      ROS_INFO("Sending target (%i) %i", Ztarget, itT->first);

      writeResult = send_PositionControlMessage(fileDesc_, x, y, r, g, b, 12+Ztarget);

      Ztarget++;
    }
  }
  if(Ztarget<hotspots.size()){
    map<int, int>::iterator ith;
    for (ith = hotspots.begin(); ith != hotspots.end(); ++ith){
      //ROS_INFO("Zooids target (%i) %i", ith->second, ith->first);
      if(!(ith->second) && Ztarget<=(ith->first-12)){
        x = origin_proj[0] + 50*(12+Ztarget);
        y = origin_proj[1] + 10;
        r = 0;
        g = 0;
        b = 0;

        //ROS_INFO("Initializing target (%i) %i: %i - %i", Ztarget, ith->first, x, y);

        writeResult = writeResult = send_PositionControlMessage(fileDesc_, x, y, r, g, b, ith->first);

        //ith->second = 1;

        Ztarget++;
      }
    }
  }
  return writeResult;
}

void SClose(int fileDesc_)
{
    if(fileDesc_ != -1) {
        auto retVal = close(fileDesc_);
        if(retVal != 0)
            ROS_ERROR("Tried to close serial port %s, but close() failed.", device_.c_str());

        fileDesc_ = -1;
    }
}

void sendstate(int state)
{
  sendevent(state);
  mavros_msgs::CommandLong cmd_srv;
  cmd_srv.request.broadcast = false;
  cmd_srv.request.command = state;
  cmd_srv.request.param1 = 0.0;
  cmd_srv.request.param5 = 0.0;
  cmd_srv.request.param6 = 0.0;
  cmd_srv.request.param7 = 0.0;
  if (mav_client.call(cmd_srv))
  {
    if(cmd_srv.response.success!=1)
      ROS_ERROR("Zooids bridge - ROSBuzz controller service refused CMDID: %i",cmd_srv.request.command);
  } else
    ROS_ERROR("Zooids bridge - Failed to call ROSBuzz controller service");
}

void sendwaypoint(double goal[2], int uid)
{
  sendevent(uid);
  mavros_msgs::CommandLong cmd_srv;
  cmd_srv.request.broadcast = false;
  cmd_srv.request.command = 16;
  cmd_srv.request.param1 = uid;
  cmd_srv.request.param5 = goal[0];
  cmd_srv.request.param6 = goal[1];
  cmd_srv.request.param7 = 10.0;
  if (mav_client.call(cmd_srv))
  {
    if(cmd_srv.response.success!=1)
      ROS_ERROR("Zooids bridge - ROSBuzz controller service refused CMDID: %i",cmd_srv.request.command);
  } else
    ROS_ERROR("Zooids bridge - Failed to call ROSBuzz controller service");
}

/*void turnonLed(int fileDesc_, int uid)
{
  int msg_size = 18;
  uint8_t data[msg_size];
  map<int, Robot>::iterator it = zooids.find(uid);
  if (it != zooids.end()) {
    PositionControlMessage Pmsg( (it->second).x, (it->second).y, (it->second).batt, 0, 100, 35, (it->second).yaw, 1);
    data[0] = '~';
    data[1] = 6;  //PositionControlMessage
    data[2] = (uint8_t)(it->first); // Zooids ids start at 0
    data[3] = 13;
    memcpy(&data[4], &Pmsg, 13);
    data[17] = '!';

    //ROS_INFO("Sending for %i", it->first);

    write(fileDesc_, data, msg_size);
  }
}*/

int readSerial(int fileDesc_)
{
  char readBuffer_[32];
  int ready = 0;
  ssize_t n = read(fileDesc_, readBuffer_, 32);
  // Error Handling
  if(n < 0) {
    // Read was unsuccessful
    ROS_ERROR("Serial read error.");
    throw std::system_error(EFAULT, std::system_category());
  }else if(n > 0) {
    int i = 0;
    while(i < n) {
      if ((readBuffer_[i] == '~') && readBuffer_[i + 14] == '!') {
        int messageType = readBuffer_[i + 1];
        if (messageType == 8) {   //LaunchButtonMessage
          int isreleased = readBuffer_[i + 2]; // Press state in the ID
            //ROS_WARN("USER press %i", isreleased);
          if(!isFlying && isreleased==1){
            ROS_INFO("USER asks to TAKEOFF!!!!");
            sendstate(22);
            isFlying = 1;
          } else if(isFlying && isreleased==0){
            ROS_INFO("USER asks to LAND!!!!");
            sendstate(21);
            isFlying = 0;

          }
          ready = 1;
        } else if (messageType == 4) {   //StatusMessage
            
            int uid = readBuffer_[i + 2]; // Zooids ids start at 0
            int payloadSize = readBuffer_[i + 3];

            //ROS_INFO("Got msg (%i) size %i from %i",payloadSize,messageType,uid);

            if (payloadSize == 10) {
              StatusMessage msg;
              memcpy(&msg, &readBuffer_[i + 4], payloadSize);
              if(float(msg.positionX)<projo_minmax[0] or float(msg.positionX)>projo_minmax[1] or float(msg.positionY)<projo_minmax[2] or float(msg.positionY)>projo_minmax[3])
                return 1;
              Robot Ztmp(msg.positionX, msg.positionY, msg.orientation / 100.0, msg.batteryLevel, -1);
              if(msg.batteryLevel<3.0)
                ROS_ERROR("%i has low battery (%f) !!", uid, msg.batteryLevel);

              // stats of communication
              map<int, vector<float>>::iterator itCH = ComHist.find(uid);
              if (itCH != ComHist.end()) {
                if(itCH->second.size()==10)
                  itCH->second.erase(itCH->second.begin());
                itCH->second.push_back(ros::Time::now().toNSec());
              } else {
                vector<float> vect{ ros::Time::now().toNSec() };
                ComHist.insert(make_pair(uid, vect));
              }

              if(uid>11){ // Zooids for targets visualization
                  map<int, int>::iterator ith = hotspots.find(uid);
                  if (ith == hotspots.end())
                    hotspots.insert(make_pair(uid, 0));
                return 1;
              } else if(uid>9){ // Zooids for hotspot inputs
                  double goal[2], ned[2];
                  int T[2] = {Ztmp.x, Ztmp.y};
                  if(sqrt((deploy_last[0]-Ztmp.x)*(deploy_last[0]-Ztmp.x)+(deploy_last[1]-Ztmp.y)*(deploy_last[1]-Ztmp.y))>80) {
                    deploy_last[0] = Ztmp.x;
                    deploy_last[1] = Ztmp.y;
                    scaletodrones(ned, T);
                    ned_to_gps(ned, goal);
                    ROS_WARN("User sending hotspots from Zooids %i (%f, %f) - (%f, %f)!", uid, goal[0], goal[1], ned[0], ned[1]);
                    sendwaypoint(goal, uid+40);
                  }
                  return 1;
              } else {
                map<int, Robot>::iterator it = zooids.find(uid);
                if (it != zooids.end()) {
                  float dist = sqrt(((it->second).x-Ztmp.x)*((it->second).x-Ztmp.x)+((it->second).y-Ztmp.y)*((it->second).y-Ztmp.y));
                  // history of distance
                  map<int, DistS>::iterator itD = DistHist.find(uid);
                  if (itD != DistHist.end()) {
                    if(itD->second.hist.size()==30)
                      itD->second.hist.erase(itD->second.hist.begin());
                    itD->second.hist.push_back(dist);
                    itD->second.lastx = Ztmp.x;
                    itD->second.lasty = Ztmp.y;
                  } else {
                    vector<float> vect(29, 0);
                    vect.push_back(dist);
                    DistS ts = {Ztmp.x,Ztmp.y,vect};
                    DistHist.insert(make_pair(uid, ts));
                  }
                  map<int, int>::iterator ita = active.find(uid);
                  if((dist < 35.0 || !isFlying || it->second.WPat > 0) && ita->second==1){
                    it->second.x = Ztmp.x; it->second.y = Ztmp.y; it->second.batt = Ztmp.batt;
                  }
                } else  //Add new Zooid
                  zooids.insert(make_pair(uid, Ztmp));
              }
              ready = 1;
            }
        } else
          ROS_WARN("GOT UNEXPECTED MESSAGE %s", readBuffer_);
        i+=15;
      } else
        i++;
    }
  }

  return ready;
}

/*void WPtimer() {
  map<int, int>::iterator itT;
  for(itT = zooids_wptimer.begin(); itT != zooids_wptimer.end(); itT++){
    if((itT->second)>0)
      (itT->second)++;
    //ROS_INFO("Timer %i %i",(itT->first),(itT->second));
    if((itT->second)>=200){
      zooids_wptimer.erase(itT);
      //break;
    }
  }
}*/

void turnoffZooids(int fileDesc_)
{
  int msg_size = 18;
  uint8_t data[msg_size];
  map<int, Robot>::iterator it;
  for(it = zooids.begin(); it != zooids.end(); it++){
    PositionControlMessage Pmsg( (it->second).x, (it->second).y, 0, 0, 0, 35, (it->second).yaw, 1);
    data[0] = '~';
    data[1] = 6;  //PositionControlMessage
    data[2] = (uint8_t)(it->first); // Zooids ids start at 0
    data[3] = 13;
    memcpy(&data[4], &Pmsg, 13);
    data[17] = '!';

    //ROS_INFO("Sending for %i", it->first);

    write(fileDesc_, data, msg_size);
  }
}

int num_person_array[50] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int person_array_index = 0;
void yolo_box_process(const mavros_msgs::Mavlink::ConstPtr& msg){
  uint64_t size = (msg->payload64.size()*sizeof(uint64_t));
  uint8_t message_obt[size];
  size=0;
  int count_person = 0;
  //  Go throught the obtained payload
  for (int i = 0; i < (int)msg->payload64.size(); i++)
  {
    uint64_t msg_tmp = msg->payload64[i];
    memcpy((void*)(message_obt+size), (void*)(&msg_tmp), sizeof(uint64_t));
    size +=sizeof(uint64_t);
  }

  int tot = 0;
  size = 0; 
  memcpy((void*)&size, (void*)(message_obt+tot), sizeof(uint64_t));
  tot += sizeof(uint64_t);
  for(int i=0; i< size; i++){
    double probability=0;
    int64_t xmin=0;
    int64_t ymin=0;
    int64_t xmax=0;
    int64_t ymax=0;
    memcpy((void*)&probability, (void*)(message_obt+tot), sizeof(uint64_t));
    tot += sizeof(uint64_t);
    memcpy((void*)&xmin, (void*)(message_obt+tot), sizeof(uint64_t));
    tot += sizeof(uint64_t);
    memcpy((void*)&ymin, (void*)(message_obt+tot), sizeof(uint64_t));
    tot += sizeof(uint64_t);
    memcpy((void*)&xmax, (void*)(message_obt+tot), sizeof(uint64_t));
    tot += sizeof(uint64_t);
    memcpy((void*)&ymax, (void*)(message_obt+tot), sizeof(uint64_t));
    tot += sizeof(uint64_t);
    uint16_t str_size=0;
    memcpy((void*)&str_size, (void*)(message_obt+tot), sizeof(uint16_t));
    tot += sizeof(uint16_t);
    char char_class[str_size * sizeof(char) + 1]; 
    memcpy((void*)char_class, (void*)(message_obt+tot), str_size);
    /* Set the termination character */
    char_class[str_size] = '\0';
    tot += str_size;
    string obj_class(char_class);
    if(obj_class=="person"){
      count_person++;
    }
  }
  ROS_INFO("PERSON SEEN %i", count_person);
  num_person_array[person_array_index] = count_person;
  if(person_array_index==50)
    person_array_index=0;
  else
    person_array_index++;
}

void yolo_dummy(const std_msgs::UInt8::ConstPtr& msg){
  for(int i = 0; i < 50; i++)
     num_person_array[i]=msg->data;
  ROS_INFO("NUMPERSON:%i",msg->data);
}

int mean_array(int arr[50]){
  float result = 0;

   for(int i = 0; i < 50; i++)
   {
      result += arr[i];
   }
   return round(result/50);
}

void print_freq(){

  map<int, vector<float>>::iterator it;
  std::ostringstream buffer(std::ostringstream::ate);
  //buffer<<"Freq =>";
  for(it = ComHist.begin(); it != ComHist.end(); it++){
    double freq = 0;
    for(int i = 0; i < it->second.size()-1; i++){
      freq += (it->second[i+1] - it->second[i]) / 1000000000.0;
      //ROS_INFO("%i %f", it->first, it->second[i]);
    }
    freq = freq / (it->second.size() - 1);
    freq = 1.0 / freq;
    //buffer << it->first << " : " << freq << " | ";
    if(freq < 1.0)
      ROS_WARN("%i low frequency: %f", it->first, freq);

  }
  //ROS_INFO("%s", buffer.str().c_str());
}

void check4WP(){
  map<int, DistS>::iterator it;
  std::ostringstream buffer(std::ostringstream::ate);
  buffer<<"Ave =>";
  /*if(!isFlying)
    return;*/
  for(it = DistHist.begin(); it != DistHist.end(); it++){
    double ave = 0;
    for(int i = 0; i < it->second.hist.size(); i++){
      ave += it->second.hist[i+1];
    }
    ave = ave / it->second.hist.size();
    map<int, Robot>::iterator itz = zooids.find(it->first);
    buffer << it->first << "(" << itz->second.WPat << ") : " << ave << " | ";
    if(ave > 50.0 && itz->second.WPat < 0){
      /*for(int i = 0; i < it->second.hist.size(); i++){
        buffer << it->second.hist[i+1] << ", ";
      }*/
      double goal[2], ned[2];
      int T[2] = {it->second.lastx, it->second.lasty};
      scaletodrones(ned, T);
      ned_to_gps(ned, goal);
      ROS_WARN("User sending waypoint from Zooids %i (%f, %f) - (%f, %f)!", it->first, goal[0], goal[1], ned[0], ned[1]);
      sendwaypoint(goal, it->first + 1);
      itz->second.WPat = ROS_RATE;
    }

  }
  ROS_INFO("%s", buffer.str().c_str());
}

void yolo_demo(int deploytimer){
  int num_person = mean_array(num_person_array);
  if(num_person > 1 && wpctr){
    wpctr = 0;
    sendstate(901);
    deploytimer = 0;
  }
  if(num_person < 2 && !wpctr && deploytimer>100){
    wpctr = 1;
    sendstate(902);
  }
  if(!wpctr && deploytimer>50 && deploytimer<60){
    double goal[2] = {29.020558, -13.714524};
    sendwaypoint(goal, 50);
    ros::Duration(0.01).sleep();
    goal[0] = 29.020132; goal[1] = -13.714923;
    sendwaypoint(goal, 51);
    ros::Duration(0.01).sleep();
    goal[0] = 29.019911; goal[1] = -13.714443;
    sendwaypoint(goal, 52);
  }else if(!wpctr && deploytimer>2*60*50 && deploytimer<2*60*50+10){
    double goal[2] = {29.019620, -13.714341};
    sendwaypoint(goal, 50);
    ros::Duration(0.01).sleep();
    goal[0] = 29.019965; goal[1] = -13.714682;
    sendwaypoint(goal, 51);
    ros::Duration(0.01).sleep();
    goal[0] = 29.019756; goal[1] = -13.715049;
    sendwaypoint(goal, 52);
  }
  if(deploytimer>2*60*50)
    deploytimer=0;
  else
    deploytimer++;
}

int main(int argc, char *argv[])
	/** Description: main ros loop initiating the tcp server and managing the communication
	******************************************************************* */
{
  //// TEST
  //Robot Dtmp;
  //drones.insert(make_pair(1, Dtmp));
  //////////////
  ros::init(argc, argv, "zooids_bridge");
  ROS_INFO("zooids_bridge");
  ros::NodeHandle nh("~");
  ros::NodeHandle nh2;
  ros::Rate loop_rate(ROS_RATE); //loops per sec.
  int deploytimer = 0, targettimer = ROS_RATE;
  int fileDesc_ = -1, serial_ready = -1;

  
  ros::spinOnce();
  mav_client = nh2.serviceClient<mavros_msgs::CommandLong>("buzzcmd");
  event_pub = nh.advertise<std_msgs::UInt32>("/pressed",5);
  ros::Subscriber deadcf_sub = nh.subscribe<std_msgs::UInt16MultiArray>("/deadCFs", 1, deadCFs);

  ros::Subscriber fleet_pos_sub = nh2.subscribe<rosbuzz::neigh_pos>("neighbours_pos", 5, updateNeighbors);
  ros::Subscriber fleet_st_sub = nh2.subscribe<std_msgs::String>("bvmstate", 5, updateState);
  ros::Subscriber target_sub = nh2.subscribe<rosbuzz::neigh_pos>("targets_found", 5, updateTargets);
  ros::Subscriber batt_sub = nh2.subscribe<sensor_msgs::BatteryState>("battery", 5, updateBatt);
  ros::Subscriber neibatt_sub = nh2.subscribe<mavros_msgs::Mavlink>("fleet_status", 5, updateNeiBatt);
  ros::Subscriber yolo_sub = nh2.subscribe<mavros_msgs::Mavlink>("intermediator_ros/bounding_boxes", 5, yolo_box_process);
  ros::Subscriber yolodum_sub = nh2.subscribe<std_msgs::UInt8>("yolodummy", 5, yolo_dummy);

  if(!debug) {
    fileDesc_ = open(device_.c_str(), O_RDWR);
    // Check status
    if(fileDesc_ == -1) {
        ROS_ERROR_STREAM("Could not open device " + device_ + ". Is the device name correct and do you have read/write permission?");
        return EXIT_FAILURE;
    }

    ConfigureTermios(fileDesc_);
  }

  ROS_INFO("Connected successfully.");
  while(ros::ok())
  {
    ros::spinOnce();
    if(!drones.empty()){
      // YOLO AUTO STATE SWTICH
      yolo_demo(deploytimer);
    }

    // REAL SERIAL COMMUNICATION WORK
    if(!debug){
      serial_ready = readSerial(fileDesc_);
      if(targettimer == 0){
          if(serial_ready){
            targettimer = ROS_RATE;
            serial_ready = sendTargetstoZooids(fileDesc_);
          }
      } else
        targettimer--;
      if(serial_ready) {
        //ROS_INFO("Serial is ready (%i)", targettimer);
        sendDronestoZooids(fileDesc_);
      }
      //WPtimer();
      if(print_time == 0){
        print_freq();
        check4WP();
        print_time = ROS_RATE * 2;
      } else
        print_time--;
    }
    
    loop_rate.sleep();
  }

  if(!debug){
    turnoffZooids(fileDesc_);
    SClose(fileDesc_);
  }
  
  return 0;
}
