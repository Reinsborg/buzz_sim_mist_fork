/** @file mavdji.cpp
 *  @version 2
 *  @date March 1st, 2017
 *
 *  @brief
 *  DJI SDK control from Mavlink service.
 *
 *  @copyright 2017 MistLab. All rights reserved.
 *
 */

#include <mistlab_rosbuzz_sdk/mavdji.h>

#include <functional>
#include <dji_sdk/DJI_LIB_ROS_Adapter.h>


void DJISDKMIST::gps_convert_ned(float &ned_x, float &ned_y,
		double gps_t_lon, double gps_t_lat,
		double gps_r_lon, double gps_r_lat)
{
	double d_lon = gps_t_lon - gps_r_lon;
	double d_lat = gps_t_lat - gps_r_lat;
	ned_x = DEG2RAD(d_lat) * EARTH_RADIUS;
	ned_x = std::floor(ned_x * 1000000) / 1000000;
	ned_y = DEG2RAD(d_lon) * EARTH_RADIUS * cos(DEG2RAD(gps_t_lat));
	ned_y = std::floor(ned_y * 1000000) / 1000000;
}
void DJISDKMIST::gps_convert_enu_ned(float &ned_x, float &ned_y,
		float enu_x, float enu_y)
{
	ned_x = enu_y;
	ned_y = enu_x;
}
bool DJISDKMIST::global_position_control(double longitude, double latitude, float altitude, float yaw)
{
    float dst_x;
    float dst_y;
    float dst_z = altitude;
    float distance = 0.0;

    if(global_position_ref_seted == 0)
    {
        ROS_INFO("Cannot run global position navigation because home position haven't set yet!");
        return false;
    }

    gps_convert_ned(dst_x, 
            dst_y,
            longitude, latitude,
            GlobalPosition.longitude,  GlobalPosition.latitude);
    distance = sqrt(dst_x*dst_x+dst_y*dst_y);
    while (distance > 1.0) {

    ROS_INFO("Current GlobalPos: %.7f, %.7f, %.7f",GlobalPosition.longitude,GlobalPosition.latitude,GlobalPosition.altitude);
    ROS_INFO("Target GPS (%.2f): %.7f, %.7f, %.7f", distance, longitude,latitude, altitude);
    ROS_INFO("SetFlight to: %f, %f, %f", dst_x, dst_y, dst_z);

    rosAdapter->flight->setMovementControl((uint8_t)0x90, dst_x, dst_y, dst_z, yaw);

    ros::Duration(0.02).sleep();

    gps_convert_ned(dst_x, 
            dst_y,
            longitude, latitude,
            GlobalPosition.longitude,  GlobalPosition.latitude);
    distance = sqrt(dst_x*dst_x+dst_y*dst_y);
    }

    return true;
}

bool DJISDKMIST::local_position_control(float x, float y, float z, float yaw)
{
    float dst_x = x;//-LocalPosition.pose.position.x
    float dst_y = y;//-LocalPosition.pose.position.y
    float dst_z = z;//+LocalPosition.pose.position.z;

    if(global_position_ref_seted == 0)
    {
        ROS_INFO("Cannot run local position navigation because home position haven't set yet!");
        return false;
    }

    ROS_INFO("Current LocalPos: %f, %f, %f",LocalPosition.pose.position.x,LocalPosition.pose.position.y,LocalPosition.pose.position.z);
    ROS_INFO("Target delta: %f, %f, %f", x, y, z);
//    ROS_INFO("SetFlight to: %f, %f, %f", dst_x-LocalPosition.pose.position.x, dst_y-LocalPosition.pose.position.y, dst_z+LocalPosition.pose.position.z);
//    rosAdapter->flight->setMovementControl((uint8_t)0x90, dst_x-LocalPosition.pose.position.x, dst_y-LocalPosition.pose.position.y, dst_z+LocalPosition.pose.position.z, yaw);
    rosAdapter->flight->setMovementControl((uint8_t)0x90, dst_x, dst_y, dst_z, yaw);

    return true;
}

bool DJISDKMIST::sdk_permission_control(int st)
{
    if (st == 1 && !sdk_incontrol) {
        ROS_INFO("Request Control");
        rosAdapter->coreAPI->setControl(true);//,(DJI::onboardSDK::CallBack)&DJISDKMIST::takecontrol_callback,this);
    }
    else if (st == 0 && sdk_incontrol) {
        ROS_INFO("Release Control");
        rosAdapter->coreAPI->setControl(false);
    }

    return true;
}

void DJISDKMIST::doOrbit()
{
float circleRadius = 3.0;
float x_center = LocalPosition.pose.position.x - circleRadius;
float y_center = LocalPosition.pose.position.y;
float x = LocalPosition.pose.position.x;
float y = LocalPosition.pose.position.y;
float circleRadiusIncrements = 0.01;
float circleHeight = LocalPosition.pose.position.z;
float Phi =0.0;

/* start to draw circle */
for(int i = 0; i < 1890; i ++)
    {
       x =  x_center + circleRadius*cos((Phi/300.0));
       y =  y_center + circleRadius*sin((Phi/300.0));
       if (Phi/300.0*57.0 < 180.0)
	local_position_control(x ,y ,circleHeight, -180+Phi/300*57);
       else
	local_position_control(x ,y ,circleHeight, Phi/300*57-180);
       Phi = Phi + 1;
       ros::Duration(0.02).sleep();
    }
return;

}

bool DJISDKMIST::djicmds(mavros_msgs::CommandLong::Request  &req,
         mavros_msgs::CommandLong::Response &res)
{
   float d_x =666, d_y=666;
   double target[3] = {(float)req.param5,(float)req.param6,(float)req.param7};
   switch(req.command)
   {
	case mavros_msgs::CommandCode::NAV_TAKEOFF:
   		ROS_INFO("TAKE OFF!!!!");
		sdk_permission_control(1);
		ros::Duration(0.01).sleep();
		rosAdapter->flight->task(DJI::onboardSDK::Flight::TASK::TASK_TAKEOFF);
		ros::Duration(0.01).sleep();
   		ROS_INFO("Go up to altitude: %.2f", req.param7);
		rosAdapter->flight->setMovementControl((uint8_t)0x90, 0.0, 0.0, req.param7, 0.0);
		res.success = true;
		break;
	case mavros_msgs::CommandCode::NAV_LAND:
   		ROS_INFO("LAND!!!!");
		rosAdapter->flight->task(DJI::onboardSDK::Flight::TASK::TASK_LANDING);
		ros::Duration(2).sleep();
		res.success = true;
		break;
	case mavros_msgs::CommandCode::NAV_RETURN_TO_LAUNCH:
   		ROS_INFO("GO HOME!!!!");
		rosAdapter->flight->task(DJI::onboardSDK::Flight::TASK::TASK_GOHOME);
		res.success = true;
		break;
	case mavros_msgs::CommandCode::NAV_WAYPOINT:
		gps_convert_ned(d_x, d_y,(double)req.param6,(double)req.param5,GlobalPosition.longitude,GlobalPosition.latitude);
		if(sqrt(d_x*d_x+d_y*d_y)>25.0) {
			ROS_INFO("REFUSED WAYPOINT: TOO FAR (%.2f)!", sqrt(d_x*d_x+d_y*d_y));
			res.success = false;
			break;
		}
		global_position_control((double)req.param6, (double)req.param5, req.param7, 0.0);
   		ROS_INFO("Sent GlobalPosition to %.7f,%.7f,%.7f",(float)req.param6, (float)req.param5, req.param7);
		break;
	case mavros_msgs::CommandCode::DO_MOUNT_CONTROL:
   		ROS_INFO("MOVING GIMBAL TO %.2f,%.2f,%.2f for %.2f",req.param1,req.param2,req.param3,req.param4);
//		drone->request_sdk_permission_control();
		ros::Duration(0.5).sleep();
//		drone->gimbal_angle_control(req.param1,req.param2,req.param3,req.param4);
		res.success = true;
		break;
	case mavros_msgs::CommandCode::MISSION_START:
   		if(req.param1==666) {
			doOrbit();
		}/* else {
			ROS_INFO("START WAYPOINTS");
			drone->waypoint_navigation_send_request(newWaypointList);
		}*/
		res.success = true;
		break;
	default:
		res.success = false;
		break;
   }
   return true;
}

//void DJISDKMIST::localsetpoint(const mavros_msgs::PositionTarget::ConstPtr &pt)
void DJISDKMIST::localsetpoint(const geometry_msgs::PoseStamped::ConstPtr &pt)
{
	float x = pt->pose.position.y - LocalPosition.pose.position.x;
	float y = pt->pose.position.x - LocalPosition.pose.position.y;
	float z = pt->pose.position.z;
	float yaw = 0.0;//pt->yaw;
	if(sqrt(x*x+y*y)>15.0) {
		ROS_INFO("REFUSED LOCAL SETPOINT: TOO FAR (%.2f)!", sqrt(x*x+y*y));
	} else {
		//gps_convert_enu_ned(x,y,pt->pose.position.x,pt->pose.position.y);
		local_position_control(x, y, z, yaw);
   		ROS_INFO("Going to %.7f,%.7f,%.7f,%.7f",x, y, z, yaw);
	}
	return;
}

bool DJISDKMIST::djiarm(mavros_msgs::CommandBool::Request  &req,
         mavros_msgs::CommandBool::Response &res)
{
	if(req.value) {
		sdk_permission_control(1);
		ros::Duration(0.01).sleep();
		rosAdapter->flight->setArm(true);
	} else {
		rosAdapter->flight->setArm(false);
		ros::Duration(0.01).sleep();
		//drone->release_sdk_permission_control();
	}
	res.success = true;
	return true;
}

bool DJISDKMIST::djimode(mavros_msgs::SetMode::Request  &req,
         mavros_msgs::SetMode::Response &res)
{
	ROS_INFO("Change mode to %i with %s (not)", req.base_mode, req.custom_mode.c_str());
	return true;
}
bool DJISDKMIST::djistream(mavros_msgs::StreamRate::Request  &req,
         mavros_msgs::StreamRate::Response &res)
{
	ROS_INFO("Change stream rate to %i (not)", req.message_rate);
	return true;
}

bool DJISDKMIST::send_data_to_remote_device_callback(dji_sdk::SendDataToRemoteDevice::Request& request, dji_sdk::SendDataToRemoteDevice::Response& response)
{
	memcpy(transparent_transmission_data, &request.data[0], request.data.size());
	rosAdapter->sendToMobile(transparent_transmission_data, request.data.size());
	response.result = true;
	return true;
}

/*****************************************************************
DJI_SDK                         -Mavlink ExtendedState landed_state

STATUS_GROUND_STANDBY = 1,          1

STATUS_TAKE_OFF = 2,                3

STATUS_SKY_STANDBY = 3,             2

STATUS_LANDING = 4,                 4

STATUS_FINISHING_LANDING = 5        0
******************************************************************/
uint8_t DJISDKMIST::getextended(uint8_t state){
    switch (state){
   	case 1:
		return 1;
		break;
	case 2:
		return 3;
		break;
	case 3:
		return 2;
		break;
	case 4:
		return 4;
		break;
	case 5:
		return 0;
		break;
	default:
		return 0;
		break;
	}
}

void DJISDKMIST::broadcast_callback()
{
    DJI::onboardSDK::BroadcastData bc_data = rosAdapter->coreAPI->getBroadcastData();

    DJI::onboardSDK::Version version = rosAdapter->coreAPI->getFwVersion();
    unsigned short msg_flags = bc_data.dataFlag;

    static int frame_id = 0;
    frame_id ++;

    auto current_time = ros::Time::now();
	
	//update flight_status
    	if (msg_flags & HAS_STATUS) {
        	std_msgs::UInt8 msg;
    		mavros_msgs::ExtendedState status;
    		status.header.stamp = current_time;
    		status.header.frame_id = "/world";
        	status.landed_state = getextended(bc_data.status);
        	mav_flight_status.publish(status);
    	}

    	//update battery msg
    	if (msg_flags & HAS_BATTERY) {
    		mavros_msgs::BatteryStatus percent;
    		percent.header.stamp = current_time;
    		percent.remaining= (double)bc_data.battery;
    		mav_power_status.publish(percent);
    	}

    //update global_position msg
    if (msg_flags & HAS_POS) {
    		GlobalPosition.header.stamp = current_time;
    		GlobalPosition.header.frame_id = "/world";
     		GlobalPosition.latitude  = (double) bc_data.pos.latitude * 180.0 / M_PI;
     		GlobalPosition.longitude = (double) bc_data.pos.longitude * 180.0 / M_PI;
     		GlobalPosition.altitude  = (double) bc_data.pos.altitude;
     		mav_currentpos.publish(GlobalPosition);
		std_msgs::Float64 rel_alt;
		rel_alt.data = (double) bc_data.pos.height;
		mav_currentalt.publish(rel_alt);
        //TODO:
        // FIX BUG about flying at lat = 0
        if (bc_data.timeStamp.time != 0 && global_position_ref_seted == 0 && GlobalPosition.latitude != 0 && bc_data.pos.health > 3) {
            GlobalPosition_ref = GlobalPosition;
            global_position_ref_seted = 1;
        }

	//update global_position msg
	float tmpx, tmpy;
	LocalPosition.pose.position.z = bc_data.pos.height;
	gps_convert_ned(tmpx, tmpy, GlobalPosition.longitude, GlobalPosition.latitude, GlobalPosition_ref.longitude, GlobalPosition_ref.latitude);
	LocalPosition.pose.position.x = tmpx;
	LocalPosition.pose.position.y = tmpy;
	mav_currentLpos.publish(LocalPosition);
//	ROS_INFO("LocalPosition: %f,%f,%f",LocalPosition.pose.position.x, LocalPosition.pose.position.y, LocalPosition.pose.position.z);

	//update odometry and imu msgs
	if ( (msg_flags & HAS_POS) && (msg_flags & HAS_Q) && (msg_flags & HAS_W) && (msg_flags & HAS_V) ) {
	nav_msgs::Odometry odometry;
		odometry.header.frame_id = "/odom";
	        odometry.header.stamp = current_time;
            odometry.pose.pose.position.x = LocalPosition.pose.position.x;
            odometry.pose.pose.position.y = -LocalPosition.pose.position.y;
            odometry.pose.pose.position.z = GlobalPosition.altitude - GlobalPosition_ref.altitude;
            tf::Quaternion bc_quaternion(bc_data.q.q1, bc_data.q.q2, bc_data.q.q3, bc_data.q.q0);
            
            tf::Vector3 odometry_axis = bc_quaternion.getAxis();
            odometry_axis.setY(-odometry_axis.getY());
            odometry_axis.setZ(-odometry_axis.getZ());
            tf::Quaternion odometry_quaternion(odometry_axis, bc_quaternion.getAngle());
            
            tf::Quaternion z_rotation = tf::createIdentityQuaternion();
            z_rotation.setRPY(0,0,0);
            odometry_quaternion = z_rotation * odometry_quaternion;
            
            tf::quaternionTFToMsg(odometry_quaternion, odometry.pose.pose.orientation);
            odometry.twist.twist.angular.x = bc_data.w.x;
            odometry.twist.twist.angular.y = -bc_data.w.y;
            odometry.twist.twist.angular.z = -bc_data.w.z;
            odometry.twist.twist.linear.x = bc_data.v.x;
            odometry.twist.twist.linear.y = -bc_data.v.y;
            odometry.twist.twist.linear.z = -bc_data.v.z;
            mav_currentodom.publish(odometry);
            tf::Transform odom_trans;
            odom_trans.setOrigin(tf::Vector3(odometry.pose.pose.position.x, odometry.pose.pose.position.y, odometry.pose.pose.position.z));
            odom_trans.setRotation(odometry_quaternion);
            odom_broadcaster.sendTransform(tf::StampedTransform(odom_trans, current_time, "/odom", "/base_link"));

    sensor_msgs::Imu imu;

    imu.header.frame_id = "/odom";
    imu.header.stamp    = current_time;

    imu.linear_acceleration.x =  bc_data.a.x * 9.801;
    imu.linear_acceleration.y = -bc_data.a.y * 9.801;
    imu.linear_acceleration.z = -bc_data.a.z * 9.801;

    imu.angular_velocity.x    =  bc_data.w.x;
    imu.angular_velocity.y    = -bc_data.w.y;
    imu.angular_velocity.z    = -bc_data.w.z;

    // Since the orientation is duplicated from attitude
    // at this point, q_FLU2ENU has already been updated
    quaternionTFToMsg(odometry_quaternion, imu.orientation);
    mav_currentimu.publish(imu);

	}

    }
}

void DJISDKMIST::transparent_transmission_callback(uint8_t *buf, uint8_t len)
{
	dji_sdk::TransparentTransmissionData transparent_transmission_data;
	transparent_transmission_data.data.resize(len);
	memcpy(&transparent_transmission_data.data[0], buf, len);
	data_received_from_remote_device_publisher.publish(transparent_transmission_data);

}

void DJISDKMIST::takecontrol_callback(DJI::onboardSDK::CoreAPI* api, DJI::onboardSDK::Header *protocolHeader, DJI::UserData udata __UNUSED) {
  unsigned short ack_data = DJI::onboardSDK::ACK_COMMON_NO_RESPONSE;
  unsigned char data = 0x1;

  if (protocolHeader->length - sizeof(uint16_t) <= sizeof(ack_data))
  {
    memcpy((unsigned char *)&ack_data, ((unsigned char *)protocolHeader) + sizeof(DJI::onboardSDK::Header),
        (protocolHeader->length - sizeof(uint16_t)));
  }
  else
  {
    ROS_ERROR("ACK is exception, session id %d,sequence %d",
        protocolHeader->sessionID, protocolHeader->sequenceNumber);
  }

  switch (ack_data)
  {
    case DJI::onboardSDK::ACK_SETCONTROL_ERROR_MODE:
      /*if(api->versionData.fwVersion < DJI::onboardSDK::MAKE_VERSION(3,2,0,0))
      {
        ROS_ERROR("Obtain control failed: switch to F mode");
      }
      else
      {
        ROS_ERROR("Obtain control failed: switch to P mode");
      }*/
      ROS_ERROR("Obtain control failed: switch RC mode");
      break;
    case DJI::onboardSDK::ACK_SETCONTROL_RELEASE_SUCCESS:
      ROS_INFO("Released control successfully");
      break;
    case DJI::onboardSDK::ACK_SETCONTROL_OBTAIN_SUCCESS:
      ROS_INFO("Obtained control successfully");
      break;
    case DJI::onboardSDK::ACK_SETCONTROL_OBTAIN_RUNNING:
      ROS_INFO("Obtain control running");
//      api->send(2, DJI::onboardSDK::encrypt, DJI::onboardSDK::SET_CONTROL, DJI::onboardSDK::CODE_SETCONTROL, &data, 1, 500, 2, takecontrol_callback);
      break;
    case DJI::onboardSDK::ACK_SETCONTROL_RELEASE_RUNNING:
      ROS_INFO("Release control running");
      data = 0;
//      api->send(2, DJI::onboardSDK::encrypt, DJI::onboardSDK::SET_CONTROL, DJI::onboardSDK::CODE_SETCONTROL, &data, 1, 500, 2, takecontrol_callback);
      break;
    case DJI::onboardSDK::ACK_SETCONTROL_IOC:
      ROS_ERROR("IOC mode opening can not obtain control");
      break;
    default:
      if (!api->decodeACKStatus(ack_data))
      {
        ROS_ERROR("While calling this function");
      }
      break;
  }
}

int DJISDKMIST::init_parameters(ros::NodeHandle& nh_private)
{
    std::string drone_version;
    std::string serial_name;
    int baud_rate;
    int app_id;
    std::string app_bundle_id; //reserved
    std::string enc_key;
    int uart_or_usb;
    

    nh_private.param("serial_name", serial_name, std::string("/dev/ttyTHS1"));
    nh_private.param("baud_rate", baud_rate, 230400);
    nh_private.param("app_id", app_id, 1022384);
    nh_private.param("enc_key", enc_key,
            std::string("e7bad64696529559318bb35d0a8c6050d3b88e791e1808cfe8f7802150ee6f0d"));

    nh_private.param("uart_or_usb", uart_or_usb, 0);//chosse uart as default
    nh_private.param("drone_version", drone_version, std::string("M100"));//chosse M100 as default

    // activation
    user_act_data.ID = app_id;
    user_act_data.encKey = app_key;
    strcpy(user_act_data.encKey, enc_key.c_str());

    printf("=================================================\n");
    printf("app id   : %d\n", user_act_data.ID);
    printf("app key  : %s\n", user_act_data.encKey);
    printf("=================================================\n");

    if(uart_or_usb) //use usb port for SDK
    {
        rosAdapter->usbHandshake(serial_name);
    }

    rosAdapter->init(serial_name, baud_rate);
    rosAdapter->coreAPI->getDroneVersion();
    //usleep(1000000);
    ros::Duration(1.0).sleep();
    printf("=================================================\n");
    printf("Hardware : %s\n", rosAdapter->coreAPI->getHwVersion());
    printf("Firmware : 0x0%X\n", rosAdapter->coreAPI->getFwVersion());
    printf("=================================================\n");

    rosAdapter->activate(&user_act_data, NULL);
    rosAdapter->setBroadcastCallback(&DJISDKMIST::broadcast_callback, this);
    rosAdapter->setFromMobileCallback(&DJISDKMIST::transparent_transmission_callback, this);

    return 0;
}

int DJISDKMIST::init_pubsub(ros::NodeHandle& nh)
{
  // broadcast topics for ROSBuzz
  mav_power_status = nh.advertise<mavros_msgs::BatteryStatus>("battery",5);
  mav_currentpos = nh.advertise<sensor_msgs::NavSatFix>("global_position/global",5);
  mav_currentLpos = nh.advertise<geometry_msgs::PoseStamped>("local_position",5);
  mav_currentalt = nh.advertise<std_msgs::Float64>("global_position/rel_alt",5);
  mav_flight_status = nh.advertise<mavros_msgs::ExtendedState>("extended_state",5);
  cmd_service = nh.advertiseService("cmd/command", &DJISDKMIST::djicmds, this);
  arm_service = nh.advertiseService("cmd/arming", &DJISDKMIST::djiarm, this);
  mode_service = nh.advertiseService("set_mode", &DJISDKMIST::djimode, this);
  stream_service = nh.advertiseService("set_stream_rate", &DJISDKMIST::djistream, this);

  //Subscribe output control topic from ROSBuzz
  localsetpointSub = nh.subscribe("/setpoint_position/local",1000, &DJISDKMIST::localsetpoint, this);

  // broadcast topics for the RCclient node when using lightbridge controller - obsolete
  data_received_from_remote_device_publisher = nh.advertise<dji_sdk::TransparentTransmissionData>("dji_sdk/data_received_from_remote_device",10);
  send_data_to_remote_device_service = nh.advertiseService("dji_sdk/send_data_to_remote_device", &DJISDKMIST::send_data_to_remote_device_callback,this);

  // broadcast topics for mapping
  mav_currentodom = nh.advertise<nav_msgs::Odometry>("odometry",5);
  mav_currentimu = nh.advertise<sensor_msgs::Imu>("imu",5);

  return 0;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "sdk_mavdji");
    ROS_INFO("sdk_service_mavdji");
    ros::NodeHandle nh;
    ros::NodeHandle nh_priv("~");

    //new an object of adapter
    rosAdapter = new DJI::onboardSDK::ROSAdapter;

    DJISDKMIST *dji_sdk_mist = new DJISDKMIST(nh, nh_priv);

    ROS_INFO("Ready to receive Mav Commands.");

    ros::AsyncSpinner spinner(4); // Use 4 threads
    spinner.start();
    ros::waitForShutdown();

    //clear
    delete rosAdapter;
    rosAdapter = NULL;
    delete dji_sdk_mist;
    dji_sdk_mist = NULL;

    return 0;
}
