#!/usr/bin/env python
import os
import subprocess as sp
from signal import SIGINT
from time import sleep
import rospy
import rospkg
from mavros_msgs.srv import StreamRate
from dronekit_sitl import SITL

######################### NODE CONSTANTS #########################

NODE_NAME = 'tablet_connector'
LOOP_RATE = 20
DEFAULT_HOME_POSITION = "45.505280, -73.614574, 135, 350"
DEFAULT_APM_REL_PATH = "{}/models/solo/solo-2.0.20/apm"
DEFAULT_DRONE_INDEX = 1

PACKAGE_NAME = "mistlab_rosbuzz_sdk"
ROSLAUNCH_MAVROS = ["roslaunch", PACKAGE_NAME, 'apm2_launcher.launch'] 
MAVROS_ARGUMENT = "fcu_url:=tcp://127.0.0.1:5{:03d}"
GROUP_ARGUMENT = "group_name:={}"
FIRST_PORT_MAVROS = 760
MAVROS_SET_STREAM_RATE_SERVICE = '{}/mavros/set_stream_rate'
MAVROS_STREAM_ID = 0
MAVROS_STREAM_RATE = 10

########################## NODE METHODS ##########################

def get_param(param_name, default, type_cast=None):
    """ Tries to query the ROS parameter with name param_name, returns default if fails.
    Cast type_cast on the result if one is given.

    :param param_name: the param_name that will be queried
    :type param_name: str
    :param default: default value returned in case the parameter does not exist
    :param type_cast: type of the desired parameter
    :type type_cast: type
    :rtype: type_cast if type_cast is not None
    """
    if rospy.has_param(param_name):
        param = rospy.get_param(param_name)
    else:
        rospy.logwarn("Param {0} does not seem to exist, "
                      "taking default value: {1}".format(param_name, default))
        param = default

    if type_cast is None:
        return param

    return type_cast(param)

########################## NODE CLASSES ##########################

class DronekitSim(object):
    """Starts the dronekit sitl simulation and the mavros apm2 to have 
    a functional simulation that can be used with ros nodes
    """
    def __init__(self, apm_path):
        self._sitl = SITL(path=apm_path)
        self._mavros_process = None

    def start(self, home_position, model="quad", drone_index=0, verbose=True):
        """Starts the Dronekit SITL simulation and Mavros apm2 
        :param home_position: list of 4 float representing the location(GPS), altitude, orientation
        :param model: String containing the model
        :type verbose: bool
        """

        arguments = ["--home={},{},{},{}".format(*home_position),
                     "--model={}".format(model),
                     "--instance={}".format(drone_index)]
        self._sitl.launch(arguments, verbose=verbose, await_ready=False, restart=True)

        self._mavros_process = sp.Popen(ROSLAUNCH_MAVROS + \
                                        [MAVROS_ARGUMENT.format(FIRST_PORT_MAVROS + (10 * drone_index))] + \
                                        [GROUP_ARGUMENT.format(rospy.get_namespace())])

        self._sitl.block_until_ready(verbose=verbose)
        return True

    def poll(self):
        """ Call the SITL simulation poll function and returns its value
        """
        return self._sitl.poll()

    def stop(self):
        """ Shutdown the simulation 
        """
        self._sitl.stop()
        if self._mavros_process is not None:
            self._mavros_process.send_signal(SIGINT)

########################### NODE MAIN ###########################

def main():
    rospy.init_node(NODE_NAME, anonymous=True)

    home_position_param = get_param("~home_position", DEFAULT_HOME_POSITION, str)
    try:
        home_position = [float(str_val) for str_val in home_position_param.split(",")]
    except ValueError as e:
        print "Could not extract home_position from: ", home_position_param
        print "Error: ", e
        return

    PKG_PATH = rospkg.RosPack().get_path(PACKAGE_NAME)
    APM_PATH = get_param("~apm_path", DEFAULT_APM_REL_PATH.format(PKG_PATH), str)

    drone_index = get_param("~drone_index", DEFAULT_DRONE_INDEX, int)

    dronekit_sim = DronekitSim(APM_PATH)
    dronekit_sim.start(home_position, drone_index=drone_index)

    mavros_set_stream_rate_service = MAVROS_SET_STREAM_RATE_SERVICE.format(rospy.get_namespace())
    rospy.wait_for_service(mavros_set_stream_rate_service)
    mavros_set_stream_rate = rospy.ServiceProxy(mavros_set_stream_rate_service, StreamRate)
    try:
        mavros_set_stream_rate(stream_id=MAVROS_STREAM_ID, message_rate=MAVROS_STREAM_RATE, on_off=1)
    except rospy.ServiceException as ex:
        print ex

    rospy.loginfo("Simulation started properly")

    rate = rospy.Rate(LOOP_RATE)
    while not rospy.is_shutdown():
        rate.sleep()

    dronekit_sim.stop()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
