# /usr/bin/bash

# File: run_sitl_experiment.sh
# Created on: 30/04/2019
# Author: Vivek Shankar Varadharajan
# Description: Script to run a batch experiment with rosbuzz using 
#			   startswarmwithobserverTEMPLATE.launch
#
# Copyright MIST Laboratory. All rights reserved.

# Stop on any error
set -e

# Basic variables
ME=$(whoami)
BASEWORKDIR=${HOME}/run_dir     # -> Set the path were you want to copy your files, 
								#    expand files and run the experiment. 
HOMEDIR=${HOME}/ROS_MIST_SIM_WS # -> IMPORTANT: set this to your ros WS path.  
DATADIR=${HOME}/ROSBuzz_DATADIR  # -> Tell the script where it has to copy the data at the end of the experiment.


cd ${HOMEDIR}
source devel/setup.bash
# Script parameters, it assumes that these parameters are passed as arguments.
# try to use the launch_sitl_experiments.sh file to start the experiments.

DROP=${1}
ROBOTS=${2}

# Run id, used as base file name
RUNID=${DROP}_${ROBOTS}

# Output file names and directories
WORKDIR=${BASEWORKDIR}/${ME}_${RUNID}
OUTFILE=${RUNID}
EXPERIMENT=run_${RUNID}.launch
EXPERIMENT_CPY=run_${RUNID}_${RUNID}.launch

# Create directory
rm -rf ${WORKDIR}
mkdir -p ${WORKDIR}
cd ${WORKDIR}

# WITH THIS PLACEMENT WE CAN HAVE 90 ROBOTS
ROBOT_PREFIX="robot"
START_ROBOT_IDS=1
ROBOT_INIT_X_POS_START=-15
ROBOT_INIT_Y_POS_START=-20
ROBOT_INIT_X_MAX_VALUE=20
ROBOT_INIT_Y_MAX_VALUE=20
ROBOT_INIT_Y_MIN_VALUE=-20
ROBOT_INIT_POS_GAP=5
# Set up experiment launch file

sed -e "s|COM_NUMBER_OF_ROBOTS|$ROBOTS|g" \
    -e "s|COM_PACKET_DROP|${DROP}|g" \
    -e "s|ROBOT_ID_OF_THE_ROBOT|${ROBOT_PREFIX}${START_ROBOT_IDS}|g" \
    -e "s|ROBOT_INIT_POSITION_X|${ROBOT_INIT_X_POS_START}|g" \
    -e "s|ROBOT_INIT_POSITION_Y|${ROBOT_INIT_Y_POS_START}|g" \
    ${HOMEDIR}/src/mistlab_rosbuzz_sdk/launch_sim/startswarmwithobserverTEMPLATE.launch > ${EXPERIMENT}
echo "ROBOT: "$START_ROBOT_IDS" Init X pos: "$ROBOT_INIT_X_POS_START" Init Y pos: "$ROBOT_INIT_Y_POS_START
# for ROBOTID in $(seq $((START_ROBOT_IDS+1)) $ROBOTS)
# 	do
# 	tail -n 33 ${HOMEDIR}/src/mistlab_rosbuzz_sdk/launch_sim/startswarmwithobserverTEMPLATE.launch >> \
# 			   ${EXPERIMENT}
# 	ROBOT_INIT_Y_POS_START=$((ROBOT_INIT_Y_POS_START+ROBOT_INIT_POS_GAP))
# 	if [ ${ROBOT_INIT_Y_POS_START} -ge  ${ROBOT_INIT_Y_MAX_VALUE} ]; then
# 		ROBOT_INIT_Y_POS_START=$ROBOT_INIT_Y_MIN_VALUE
# 		ROBOT_INIT_X_POS_START=$((ROBOT_INIT_X_POS_START+ROBOT_INIT_POS_GAP))
# 		if [ ${ROBOT_INIT_Y_POS_START} -ge  ${ROBOT_INIT_Y_MAX_VALUE} ]; then
# 			echo "MAXIMUM NUMBER OF ROBOTS REACHED"
# 			exit 0
# 		fi
# 	fi
# 	echo "ROBOT: "$ROBOTID" Init X pos: "$ROBOT_INIT_X_POS_START" Init Y pos: "$ROBOT_INIT_Y_POS_START
# 	sed -e "s|ROBOT_ID_OF_THE_ROBOT|${ROBOT_PREFIX}${ROBOTID}|g" \
# 		-e "s|ROBOT_INIT_POSITION_X|${ROBOT_INIT_X_POS_START}|g" \
#     	-e "s|ROBOT_INIT_POSITION_Y|${ROBOT_INIT_Y_POS_START}|g" \
# 	    ${EXPERIMENT} > ${EXPERIMENT_CPY}
# 	mv ${EXPERIMENT_CPY} ${EXPERIMENT}
# done

# echo "</launch>" >> ${EXPERIMENT}

# start the rosmaster 
(roscore) &
PID=($!)
sleep 5
# start the ros bag for the exepriment for now i want only global pos

rosbag record -O ${RUNID}.bag /robot1/global_position/global \
/robot2/global_position/global /robot3/global_position/global \
/robot4/global_position/global /robot5/global_position/global \
/robot6/global_position/global &

# Uncoment if you want to bag all the topics
#rosbag record -O ${RUNID}.bag -a &

sleep 5
# launch the experiment
roslaunch ${EXPERIMENT} &
pidlros=($!)

# Wait until roscore is brought down
while [ -n "$(ps -p $PID -o pid=)" ]; do echo "WAITING FOR: ${PID} (roscore to die)"; sleep 2; done

echo "ROS CORE ------------------------------------------------------------------ KILLED"
sleep 2
# send SIGTERM to the launched process to kill all leftovers
kill -15 ${pidlros}
sleep 5

echo "Copying data files ..."

# Copy files back to the data directory
mkdir -p ${DATADIR}/${RUNID}
cp -af $WORKDIR/${RUNID}.bag ${DATADIR}/${RUNID}
for ROBOTID in $(seq $START_ROBOT_IDS $ROBOTS)
	do
	cp -af $HOMEDIR/src/rosbuzz/buzz_scripts/log/logger_${ROBOTID}_0.log ${DATADIR}/${RUNID}
	cp -af $HOMEDIR/src/rosbuzz/buzz_scripts/log/voronoi_${ROBOTID}.csv ${DATADIR}/${RUNID}
done

echo "Cleaning up run dir ..."
# Cleanup
rm -rf ${WORKDIR}