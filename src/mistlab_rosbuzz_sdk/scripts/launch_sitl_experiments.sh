# /usr/bin/bash

# File: run_sitl_experiment.sh
# Created on: 30/04/2019
# Author: Vivek Shankar Varadharajan
# Description: Script to run a batch experiment with rosbuzz using 
#			   startswarmwithobserverTEMPLATE.launch
#
# Copyright MIST Laboratory. All rights reserved.

# Stop on any error
set -e

ROBOTS=6

for DROP in 0 0.10 0.20 0.50 0.70 0.90 
do
	# launch the job with the specified config
      echo "$DROP $ROBOTS"
 	  bash run_sitl_experiment.sh $DROP $ROBOTS
	  sleep 20
	  clear
	
done
