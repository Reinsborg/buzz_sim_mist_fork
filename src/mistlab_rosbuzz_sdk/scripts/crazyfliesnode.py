#!/usr/bin/env python

import rospy
from rosbuzz.msg import neigh_pos
from std_msgs.msg import String
from std_msgs.msg import UInt16MultiArray
from sensor_msgs.msg import BatteryState
import math
import numpy as np
from scipy.optimize import linear_sum_assignment
import time
from collections import deque
import yaml
import os

import cflib.crtp
from cflib.crazyflie.swarm import CachedCfFactory
from cflib.crazyflie.swarm import Swarm
from cflib.crazyflie.syncLogger import SyncLogger
from cflib.crazyflie.log import LogConfig
from cflib.crazyflie.mem import MemoryElement

# Xmin Xmax Ymin Ymax
CF_arena = [10.0, 0.0, 10.0, 0.0]
# CF_arena_buffer = [0.75,0.4,0.18,0.8]
CF_arena_buffer = [0.25,0.8,0.4,0.1]
# CF_arena = [0.3, 3.1, 0.1, 4.1]
# BU_world = [0.0, 35.0, 0.0, 48.0]
BU_world = [-5.0, 15.0, -5.0, 35.0]
CFflying = False
BUflying = False
isIDLE = False
CFinitialized = False
CF_deaths= []
volt2percent = {3.8:1.0, 3.73:0.9, 3.66:0.8, 3.59:0.7, 3.52:0.6, 3.45:0.5, 3.38:0.4, 3.21:0.3, 3.14:0.2, 3.07:0.1, 3.0:0.0}
	
# Change uris according to your setup
URI1 = 'radio://0/80/2M/E7E7E7E701'
URI2 = 'radio://0/80/2M/E7E7E7E702'
URI3 = 'radio://0/80/2M/E7E7E7E703'
URI4 = 'radio://0/80/2M/E7E7E7E704'
URI5 = 'radio://0/80/2M/E7E7E7E705'
URI6 = 'radio://0/80/2M/E7E7E7E706'
URI7 = 'radio://0/80/2M/E7E7E7E707'
URI8 = 'radio://0/80/2M/E7E7E7E710'
URI9 = 'radio://0/80/2M/E7E7E7E711'
URI10 = 'radio://0/80/2M/E7E7E7E70A'
URI11 = 'radio://0/80/2M/E7E7E7E70B'
URI12 = 'radio://0/80/2M/E7E7E7E70C'

# d: for testing purpose
# z: altitude
params1 = {'x': 2.5, 'y': 3.0, 'z': 0.8}
params2 = {'x': 1.5, 'y': 3.0, 'z': 0.8}
params3 = {'x': 1.5, 'y': 1.5, 'z': 0.8}
params4 = {'x': 2.5, 'y': 1.5, 'z': 0.8}
params5 = {'x': 2.0, 'y': 2.25, 'z': 0.8}
params6 = {'x': 1.0, 'y': 2.25, 'z': 0.8}
params7 = {'x': 1.5, 'y': 2.25, 'z': 0.8}
params8 = {'x': 1.0, 'y': 2.25, 'z': 0.8}
params9 = {'x': 1.75, 'y': 2.25, 'z': 0.8}
params10 = {'x': 1.0, 'y': 2.25, 'z': 0.8}
params11 = {'x': 1.0, 'y': 2.25, 'z': 0.8}
params12 = {'x': 1.0, 'y': 2.25, 'z': 0.8}

params = {
    URI1: [params1],
    URI2: [params2],
    URI3: [params3],
    URI4: [params4],
    URI5: [params5],
    URI6: [params6],
    URI7: [params7],
    URI8: [params8],
    URI9: [params9],
    URI10: [params10],
    URI11: [params11],
    URI12: [params12]
}

alluris = {
    URI1,
    URI2,
    URI3,
    URI4,
    URI5,
    URI6,
    URI7,
    URI8,
    URI9,
    URI10,
    URI11,
    URI12
}

# Maps Crazyflies to the simulated m100s
# -1 means not assigned
# after being assigned, the number changes to the drone's ID
mapCFBU_s1 = {
    URI1: -1,
#    URI2: -1,
    URI3: -1
}
mapCFBU_s2 = {
    URI7: -1,
    URI8: -1,
    URI9: -1,
    URI10: -1,
    URI11: -1,
    URI1: -1
#    URI12: -1
}
mapCFBU = mapCFBU_s1

CFpose= {}

EARTH_RADIUS = 6371000.0

# Origin (0,0) for NED translation to LPS
# origin_gps = [29.021055, -13.715155]
origin_gps = [45.510369, -73.609415]

class CircularBuffer(deque):
     def __init__(self, size=0):
        super(CircularBuffer, self).__init__(maxlen=size)
     @property
     def average(self):
        if len(self) ==0:
            return 0
        else:
            return sum(self)/len(self)
BatArray = {}
for key in mapCFBU:
    BatArray[key] = CircularBuffer(size=10)

def DEG2RAD(DEG):
    return ((DEG) * ((math.pi) / (180.0)))
def RAD2DEG(RAD):
    return ((RAD) * ((180.0) / (math.pi)))
def constrainAngle(x):
    x = math.fmod(x + math.pi, 2 * math.pi)
    if (x < 0.0):
        x += 2 * math.pi
    return x - math.pi

def ned_from_gps(nei, cur):
    out = []
    d_lon = nei[1] - cur[1]
    d_lat = nei[0] - cur[0]
    out.append(DEG2RAD(d_lat) * EARTH_RADIUS)
    out.append(DEG2RAD(d_lon) * EARTH_RADIUS * math.cos(DEG2RAD(nei[0])))
    return out

def scale_CF(ned):
    out = []
    out.append((ned[0]-BU_world[0])/(BU_world[1]-BU_world[0])*(CF_arena[1]-CF_arena[0])+CF_arena[0])
    out.append((ned[1]-BU_world[2])/(BU_world[3]-BU_world[2])*(CF_arena[3]-CF_arena[2])+CF_arena[2])
    # out[1] = CF_arena[3]-out[1]

    if(out[0]<CF_arena[0]):
        out[0]=CF_arena[0]
    elif(out[0]>CF_arena[1]):
        out[0]=CF_arena[1]
    if(out[1]<CF_arena[2]):
        out[1]=CF_arena[2]
    elif(out[1]>CF_arena[3]):
        out[1]=CF_arena[3]
    return out

def poshold(cf, t, z):
    steps = t * 10

    for r in range(steps):
        cf.commander.send_hover_setpoint(0, 0, 0, z)
        time.sleep(0.1)

def CF_takeoff(cf, params):
    if mapCFBU[cf.link_uri]==-1:
        return
    # Number of setpoints sent per second
    fs = 10
    fsi = 1.0 / fs
    # Base altitude in meters
    base = 0.15
    position = [params['x'], params['y'], params['z'], 0.0]

    # TAKEOFF ?
    time.sleep((mapCFBU[cf.link_uri]-1)*0.25)
    rospy.loginfo("Sending base height command " + str(base))
    poshold(cf, 2, base)

    # SLOWLY REACH TARGET HEIGHT
    rospy.loginfo("Then reach target altitude " + str(position[2]))
    ramp = fs * 2
    for r in range(ramp):
        rospy.loginfo(str(mapCFBU[cf.link_uri]) + " " + str(r * (position[0] - CFpose[cf.link_uri]['x']) / ramp) + " " + str(r * (position[1] - CFpose[cf.link_uri]['y']) / ramp) + " " + str(position[3]) + " " + str(base + r * (position[2] - base) / ramp))
        cf.commander.send_hover_setpoint(r * (position[0] - CFpose[cf.link_uri]['x']) / ramp, r * (position[1] - CFpose[cf.link_uri]['y']) / ramp, position[3], base + r * (position[2] - base) / ramp)
        time.sleep(fsi)

    poshold(cf, 2, position[2])
    rospy.loginfo(str(mapCFBU[cf.link_uri])+ " is done with takeoff.")
    time.sleep((len(mapCFBU)-mapCFBU[cf.link_uri])*0.25)
    cf.param.set_value('flightmode.posSet', '1')

def CF_land(cf, params):
    # Number of setpoints sent per second
    fs = 30
    fsi = 1.0 / fs
    # Base altitude in meters
    base = 0.15

    z = params['z']
    if (z>0.25):
        params['z'] = base

        # SLOWLY LAND
        rospy.loginfo("Sending landing command")
        ramp = fs * 2
        for r in range(ramp):
            cf.commander.send_hover_setpoint(0, 0, 0,
                                            base + (ramp - r) * (z - base) / ramp)
            #print("Ramp set point: "+str(base + (ramp - r) * (z - base) / ramp))
            time.sleep(fsi)

        poshold(cf, 1, base)

    cf.commander.send_stop_setpoint()

def wait_for_position_estimator(cf):
    global CFpose
    print('Waiting for estimator to find position...')

    log_config = LogConfig(name='Kalman Variance', period_in_ms=150)
    log_config.add_variable('kalman.varPX', 'float')
    log_config.add_variable('kalman.varPY', 'float')
    log_config.add_variable('kalman.varPZ', 'float')
    log_config2 = LogConfig(name='Pose', period_in_ms=50)
    log_config2.add_variable('stateEstimate.x', 'float')
    log_config2.add_variable('stateEstimate.y', 'float')
    log_config2.add_variable('stateEstimate.z', 'float')
    log_config2.add_variable('stateEstimate.pitch', 'float')
    log_config2.add_variable('stateEstimate.roll', 'float')
#    log_config2.add_variable('stateEstimate.yaw', 'float')

    var_y_history = [1000] * 10
    var_x_history = [1000] * 10
    var_z_history = [1000] * 10

    threshold = 0.001

    with SyncLogger(cf, log_config) as logger:
        for log_entry in logger:
            data = log_entry[1]

            var_x_history.append(data['kalman.varPX'])
            var_x_history.pop(0)
            var_y_history.append(data['kalman.varPY'])
            var_y_history.pop(0)
            var_z_history.append(data['kalman.varPZ'])
            var_z_history.pop(0)

            min_x = min(var_x_history)
            max_x = max(var_x_history)
            min_y = min(var_y_history)
            max_y = max(var_y_history)
            min_z = min(var_z_history)
            max_z = max(var_z_history)

            #print("{} {} {}".format(max_x - min_x, max_y - min_y, max_z - min_z))

            if (max_x - min_x) < threshold and (max_y - min_y) < threshold and (max_z - min_z) < threshold:
                break
    rospy.loginfo("Kalman reset done")
    with SyncLogger(cf, log_config2) as logger:
        for log_entry in logger:
            data = log_entry[1]        
            if (data['stateEstimate.x'] < CF_arena[0]) or (data['stateEstimate.x'] > CF_arena[1]) or (data['stateEstimate.y'] < CF_arena[2]) or (data['stateEstimate.y'] > CF_arena[3]) or (abs(data['stateEstimate.pitch']) > 3.0) or (abs(data['stateEstimate.roll']) > 3.0):
                rospy.logerr("WRONG POSITION! ({},{},{},{})".format(data['stateEstimate.x'],data['stateEstimate.y'],data['stateEstimate.pitch'],data['stateEstimate.roll']))
                return 0
            else:
                pose = {'x':data['stateEstimate.x'], 'y':data['stateEstimate.y'], 'z':data['stateEstimate.z']}
                CFpose[cf.link_uri]=pose
                return 1
        
def wait_for_param_download(scf):
    while not scf.cf.param.is_updated:
        time.sleep(1.0)
    #print('Parameters downloaded for', scf.cf.link_uri)

def reset_estimator(scf):
    cf = scf.cf
    cf.param.set_value('kalman.resetEstimation', '1')
    time.sleep(0.1)
    cf.param.set_value('kalman.resetEstimation', '0')

    while not wait_for_position_estimator(cf):
        cf.param.set_value('kalman.resetEstimation', '1')
        time.sleep(0.1)
        cf.param.set_value('kalman.resetEstimation', '0')

    start_batt_log(cf)
    rospy.loginfo("Init done for {} ({},{})".format(cf.link_uri,CFpose[cf.link_uri]['x'],CFpose[cf.link_uri]['y']))
    CFinitialized = True
    

def log_callback(cf, timestamp, data, logconf):
    global BatArray
    BatArray[cf.link_uri].append(data['pm.vbat'])
    # print "Battery call back : average :"
    # print float(BatArray[cf.link_uri].average)
    # print cf.link_uri
    # rospy.loginfo('{}% batt = {}%'.format(cf.link_uri,data['pm.vbat']))
    # print "%s, Average: %s, id %s"%(BatArray[cf.link_uri], BatArray[cf.link_uri].average, cf.link_uri)
    if (float(BatArray[cf.link_uri].average) < 3.1 and float(params[cf.link_uri][0]['z']) > 0.5):
        rospy.logerr("{} batt too low = {}V".format(cf.link_uri,BatArray[cf.link_uri].average))
        CF_land(cf, params[cf.link_uri][0])
        #params[cf.link_uri][0]['z'] = 0.15
    else:
        if assigned:
            rospy.loginfo("{} batt = {}V".format(cf.link_uri,BatArray[cf.link_uri].average))
            #position = [params['x'], params['y'], params['z'], 0.0]
            #rospy.loginfo(str(position[0]) + "|" + str(position[1]) + "|" + str(position[2]))

    if ((abs(data['stateEstimate.roll']) > 150.0)):
        rospy.loginfo("{} ({}) Upside Down:{} {}!".format(cf.link_uri, mapCFBU[cf.link_uri], data['stateEstimate.pitch'], data['stateEstimate.roll']))
        add_death(cf.link_uri)


def add_death(id_link):
    global CF_deaths
    if mapCFBU[id_link]!=-1:
        if mapCFBU[id_link] not in CF_deaths:
            CF_deaths.append(mapCFBU[id_link])

def publish2ROS():
    global pubd, pubb, CF_deaths, mapCFBU
    pubd.publish(data=CF_deaths)
    for el in BatArray:
        if mapCFBU[el]!=-1 and BatArray[el].average>0.5:
            pubb[mapCFBU[el]].publish(percentage=volt2percent[min(volt2percent, key=lambda x:abs(x-BatArray[el].average))])

def start_batt_log(cf):
    log_conf = LogConfig(name='Battery', period_in_ms=900)
    log_conf.add_variable('pm.vbat', 'float')
    log_conf.add_variable('stateEstimate.pitch', 'float')
    log_conf.add_variable('stateEstimate.roll', 'float')

    cf.log.add_config(log_conf)
    log_conf.data_received_cb.add_callback(lambda t, d, l: log_callback(cf, t, d, l))
    log_conf.start()

def run_swarm(scf, params):
    global CFflying
    global BUflying
    cf = scf.cf

    if mapCFBU[cf.link_uri]==-1:
        return
    if cf.link_uri in CF_deaths:
        cf.commander.send_stop_setpoint()
        return

    #rospy.loginfo(str(CFflying) + " | " + str(BUflying) + " | "+str(params['z']) )

    # UPDATE POSITIONS FROM ROSBUZZ
    if not CFflying:
        if BUflying:
            CF_takeoff(cf, params)
            CFflying = True
    else:
        if not BUflying:
            CF_land(cf, params)
            CFflying = False
        elif (isIDLE and params['z'] > 0.5):
            rospy.loginfo("IDLE - Hovering")
            poshold(cf, 2, params['z'])
        elif(params['z'] > 0.5):
            position = [params['x'], params['y'], params['z'], 0.0]
            rospy.loginfo(str(position[0]) + "|" + str(position[1]) + "|" + str(position[2]))
            cf.commander.send_position_setpoint(position[0], position[1],  position[2],  position[3])

assigned = False
def callback(data):
    global params
    global mapCFBU
    global assigned
    gotgst = False

    if -1 in mapCFBU.values():
        assigned = False

    if CFinitialized:
        if not assigned:
            Hmat = []
            for uav in data.pos_neigh:
                if uav.position_covariance_type == 0:
                    gotgst = True
                    continue
                Hrow =[]
                ned = ned_from_gps([uav.latitude, uav.longitude], origin_gps)
                arena_coord = scale_CF(ned)
                keylist = []
                for key in CFpose:
                    dist = math.sqrt((arena_coord[0]-CFpose[key]['x'])*(arena_coord[0]-CFpose[key]['x'])+(arena_coord[1]-CFpose[key]['y'])*(arena_coord[1]-CFpose[key]['y']))
                    Hrow.append(dist)
                    keylist.append(key)
                #print(uav.position_covariance_type, Hrow)
                Hmat.append(Hrow)
            print(keylist)
            print(np.asarray(Hmat))
            row_ind, col_ind = linear_sum_assignment(np.asarray(Hmat))
            print(row_ind, col_ind)
            for Hungit in range(len(col_ind)):
                uav = data.pos_neigh[row_ind[Hungit]]
                if gotgst:
                    uav = data.pos_neigh[row_ind[Hungit]+1]
                key = keylist[col_ind[Hungit]]
                ned = ned_from_gps([uav.latitude, uav.longitude], origin_gps)
                arena_coord = scale_CF(ned)
                print(Hungit, key, uav.position_covariance_type)
                mapCFBU[key]=uav.position_covariance_type
                params[key][0]['x'] = arena_coord[0]
                params[key][0]['y'] = arena_coord[1]
            assigned = True
        else:
            for uav in data.pos_neigh:
                if uav.position_covariance_type == 0:
                    continue
                ned = ned_from_gps([uav.latitude, uav.longitude], origin_gps)
                arena_coord = scale_CF(ned)
                #rospy.loginfo("ID: " + str(uav.position_covariance_type) + " x :" + str(arena_coord[0]) + " | y :" + str(arena_coord[1]))
                #mapexist = False
                for key in mapCFBU:
                    if(mapCFBU[key]==uav.position_covariance_type):
                        params[key][0]['x'] = arena_coord[0]
                        params[key][0]['y'] = arena_coord[1]
                        #mapexist = True
                        continue
            #rospy.loginfo(mapCFBU)

def monitor_state(data):
    global BUflying
    global isIDLE
    if(data.data=='BARRIERWAIT'):
        return
    # if(data.data=='IDLE'):
    #     isIDLE = True
    # else:
    #     isIDLE = False
    tmp = True
    if(data.data=='TURNEDOFF' or data.data=='STOP'):
        tmp = False
    if(tmp==BUflying):
        pass
    else:
        BUflying = tmp

def listener():
    global pubd, pubb
    pubb = {}
    rospy.init_node('crazyfliesnode', anonymous=False)

    rospy.Subscriber("/robot1/neighbours_pos", neigh_pos, callback);
    rospy.Subscriber("/robot1/bvmstate", String, monitor_state);
    pubd = rospy.Publisher('deadCFs', UInt16MultiArray, queue_size=1)
    for i in range(1,7):
        print("Create publisher "+'/robot'+str(i)+'/battery')
        pubb[i] = rospy.Publisher('/robot'+str(i)+'/battery', BatteryState, queue_size=1)

if __name__ == '__main__':
    listener()
    global CFinitialized

    cflib.crtp.init_drivers(enable_debug_driver=False)

    # # Scan for Crazyflies and use the first one found
    # print('Scanning interfaces for Crazyflies...')
    # for i in alluris:
    #     add = i.split("/")
    #     print(add[5])
    #     available = cflib.crtp.scan_interfaces(int(add[5],16))
    #     if available:
    #         print "Found Crazyflie on URI [%s] with comment [%s]" % (available[0][0],available[0][1])
    #         mapCFBU[i] = -1

    # Load cf arena from the anchor pos file
    with open(os.path.expanduser("/home/rafael/ROS_WS/src/mistlab_rosbuzz_sdk/misc/arena_ets.yaml"), "r") as fd:
        for id in range(0,7):
            doc = yaml.load_all(fd.readline())
            for packet in doc:
                if not packet:
                   continue
                for id, position in packet.items():
                    x = position['x']
                    y = position['y']
                    z = position['z'] 
                if x < CF_arena[0]:
                    CF_arena[0] = x
                if x > CF_arena[1]:
                    CF_arena[1] = x
                if y < CF_arena[2]:
                    CF_arena[2] = y
                if y > CF_arena[3]:
                    CF_arena[3] = y
    # for i in range(0,4):
    #     if(CF_arena[i] <= 0):
    #         CF_arena[i] = CF_arena[i] + CF_arena_buffer[i]
    #     else:
    #         CF_arena[i] = CF_arena[i] - CF_arena_buffer[i]
    CF_arena[0] = CF_arena[0] + CF_arena_buffer[0]
    CF_arena[1] = CF_arena[1] - CF_arena_buffer[1]
    CF_arena[2] = CF_arena[2] + CF_arena_buffer[2]
    CF_arena[3] = CF_arena[3] - CF_arena_buffer[3]

    print CF_arena
    
    factory = CachedCfFactory(rw_cache='~/CFcache')
    if mapCFBU:
        with Swarm(mapCFBU, factory=factory) as swarm:
            swarm.parallel(reset_estimator)
            swarm.parallel(wait_for_param_download)
            CFinitialized = True
            r = rospy.Rate(25)
            while not rospy.is_shutdown():
                swarm.parallel(run_swarm, args_dict=params)
                publish2ROS()
                r.sleep()
