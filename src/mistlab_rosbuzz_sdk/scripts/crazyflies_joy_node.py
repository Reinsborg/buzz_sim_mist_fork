#!/usr/bin/env python

import rospy
from std_msgs.msg import UInt16MultiArray
from sensor_msgs.msg import BatteryState
from sensor_msgs.msg import Joy
import time
from collections import deque
import yaml
import os

import cflib.crtp
from cflib.crazyflie.swarm import CachedCfFactory
from cflib.crazyflie.swarm import Swarm
from cflib.crazyflie.syncLogger import SyncLogger
from cflib.crazyflie.log import LogConfig

# Xmin Xmax Ymin Ymax
CF_arena = [10.0, 0.0, 10.0, 0.0]
CF_arena_buffer = [0.25,0.8,0.4,0.1]

CFflying = False
CF_deaths= []
volt2percent = {3.8:1.0, 3.73:0.9, 3.66:0.8, 3.59:0.7, 3.52:0.6, 3.45:0.5, 3.38:0.4, 3.21:0.3, 3.14:0.2, 3.07:0.1, 3.0:0.0}

# Commands from the joystick controller
joystick = {
    'trust': 0.0,
    'yaw': 0.0,
    'roll': 0.0,
    'pitch': 0.0,
    'takeoff': False,
    'land': False,
    'previous': False,
    'next': False
}
	
# Change uris according to your setup
URI1 = 'radio://0/80/2M/E7E7E7E701'
URI2 = 'radio://0/80/2M/E7E7E7E702'
URI3 = 'radio://0/80/2M/E7E7E7E703'
URI4 = 'radio://0/80/2M/E7E7E7E704'
URI5 = 'radio://0/80/2M/E7E7E7E705'
URI6 = 'radio://0/80/2M/E7E7E7E706'
URI7 = 'radio://0/80/2M/E7E7E7E707'
URI8 = 'radio://0/80/2M/E7E7E7E710'
URI9 = 'radio://0/80/2M/E7E7E7E711'
URI10 = 'radio://0/80/2M/E7E7E7E70A'
URI11 = 'radio://0/80/2M/E7E7E7E70B'
URI12 = 'radio://0/80/2M/E7E7E7E70C'

# Used to keep track of reference altitude
params1 = {'x': 1.5, 'y': 1.5, 'z': 0.8}
params2 = {'x': 1.5, 'y': 1.5, 'z': 0.8}
params3 = {'x': 1.5, 'y': 1.5, 'z': 0.8}
params4 = {'x': 2.5, 'y': 1.5, 'z': 0.8}
params5 = {'x': 2.0, 'y': 2.25, 'z': 0.8}
params6 = {'x': 1.0, 'y': 2.25, 'z': 0.8}
params7 = {'x': 1.5, 'y': 2.25, 'z': 0.8}
params8 = {'x': 1.0, 'y': 2.25, 'z': 0.8}
params9 = {'x': 1.75, 'y': 2.25, 'z': 0.8}
params10 = {'x': 1.0, 'y': 2.25, 'z': 0.8}
params11 = {'x': 1.0, 'y': 2.25, 'z': 0.8}
params12 = {'x': 1.0, 'y': 2.25, 'z': 0.8}

params = {
    URI1: [params1],
    URI2: [params2],
    URI3: [params3],
    URI4: [params4],
    URI5: [params5],
    URI6: [params6],
    URI7: [params7],
    URI8: [params8],
    URI9: [params9],
    URI10: [params10],
    URI11: [params11],
    URI12: [params12]
}

# List of URIs, comment the one you do not want to fly
mapCFBU = {
    # URI1: -1,
    URI2: -1,
    URI3: -1
}

# Assign an id for each URI
cid = 1
for uri in mapCFBU:
    mapCFBU[uri] = cid
    cid += 1

# The drone which is being controlled
controlled = 1

# Home position, filled after the kalman filter stabilizes
home = {}

class CircularBuffer(deque):
     def __init__(self, size=0):
        super(CircularBuffer, self).__init__(maxlen=size)
     @property
     def average(self):
        if len(self) ==0:
            return 0
        else:
            return sum(self)/len(self)
BatArray = {}
for key in mapCFBU:
    BatArray[key] = CircularBuffer(size=10)


def poshold(cf, t, z):
    """
    Hold position for a number of seconds

    Args:
        cf: a Crazyflie instance
        t: time to hold position, in seconds
        z: absolute altitude in meters
    """
    steps = t * 10
    for r in range(steps):
        cf.commander.send_hover_setpoint(0, 0, 0, z)
        time.sleep(0.1)


def CF_takeoff(cf, z):
    """
    Execute takeoff sequence

    Args:
        cf: a Crazyflie instance
        z: target altitude in meters
    """
    drone_id = mapCFBU[cf.link_uri]

    # Number of setpoints sent per second
    fs = 10
    fsi = 1.0 / fs
    # Base altitude in meters
    base = 0.15

    # TAKEOFF
    time.sleep((drone_id-1) * 0.25)
    rospy.loginfo("Sending base height command {}".format(base))
    poshold(cf, 2, base)

    # SLOWLY REACH TARGET HEIGHT
    rospy.loginfo("Then reach target altitude {}".format(z))
    ramp = fs * 2
    for r in range(ramp):
        cf.commander.send_hover_setpoint(0.0, 0.0, 0.0, base + r * (z - base) / ramp)
        time.sleep(fsi)

    poshold(cf, 2, z)
    rospy.loginfo("{} is done with takeoff.".format(drone_id))
    time.sleep((len(mapCFBU) - drone_id) * 0.25)
    cf.param.set_value('flightmode.posSet', '1')


def CF_land(cf, z):
    """
    Execute landing sequence

    Args:
        cf: a Crazyflie instance
        z: current reference altitude
    """
    # Number of setpoints sent per second
    fs = 30
    fsi = 1.0 / fs
    # Base altitude in meters
    base = 0.15

    if z > 0.25:

        # SLOWLY LAND
        rospy.loginfo("Sending landing command")
        ramp = fs * 2
        for r in range(ramp):
            cf.commander.send_hover_setpoint(0, 0, 0, base + (ramp - r) * (z - base) / ramp)
            time.sleep(fsi)

        poshold(cf, 1, base)

    cf.commander.send_stop_setpoint()


def wait_for_position_estimator(cf):
    """
    Keep track of recent position estimates and return only when estimation is stable
    """
    global home

    print('Waiting for estimator to find position...')

    log_config = LogConfig(name='Kalman Variance', period_in_ms=150)
    log_config.add_variable('kalman.varPX', 'float')
    log_config.add_variable('kalman.varPY', 'float')
    log_config.add_variable('kalman.varPZ', 'float')
    log_config2 = LogConfig(name='Pose', period_in_ms=50)
    log_config2.add_variable('stateEstimate.x', 'float')
    log_config2.add_variable('stateEstimate.y', 'float')
    log_config2.add_variable('stateEstimate.z', 'float')
    log_config2.add_variable('stateEstimate.pitch', 'float')
    log_config2.add_variable('stateEstimate.roll', 'float')
    # log_config2.add_variable('stateEstimate.yaw', 'float')

    var_y_history = [1000] * 10
    var_x_history = [1000] * 10
    var_z_history = [1000] * 10

    threshold = 0.001

    with SyncLogger(cf, log_config) as logger:
        for log_entry in logger:
            data = log_entry[1]

            var_x_history.append(data['kalman.varPX'])
            var_x_history.pop(0)
            var_y_history.append(data['kalman.varPY'])
            var_y_history.pop(0)
            var_z_history.append(data['kalman.varPZ'])
            var_z_history.pop(0)

            min_x = min(var_x_history)
            max_x = max(var_x_history)
            min_y = min(var_y_history)
            max_y = max(var_y_history)
            min_z = min(var_z_history)
            max_z = max(var_z_history)

            # print("{} {} {}".
            #       format(max_x - min_x, max_y - min_y, max_z - min_z))

            if (max_x - min_x) < threshold and (max_y - min_y) < threshold and (max_z - min_z) < threshold:
                break

    rospy.loginfo("Kalman reset done")
    with SyncLogger(cf, log_config2) as logger:
        for log_entry in logger:
            data = log_entry[1]        
            if (data['stateEstimate.x'] < CF_arena[0]) or (data['stateEstimate.x'] > CF_arena[1]) or (data['stateEstimate.y'] < CF_arena[2]) or (data['stateEstimate.y'] > CF_arena[3]) or (abs(data['stateEstimate.pitch']) > 3.0) or (abs(data['stateEstimate.roll']) > 3.0):
                rospy.logerr(
                    "WRONG POSITION! ({},{},{},{})".format(
                        data['stateEstimate.x'],
                        data['stateEstimate.y'],
                        data['stateEstimate.pitch'],
                        data['stateEstimate.roll']
                    ))
                return 0
            else:
                pose = {'x':data['stateEstimate.x'], 'y':data['stateEstimate.y'], 'z':data['stateEstimate.z']}
                home[cf.link_uri] = pose
                return 1
        

def wait_for_param_download(scf):
    """
    Return only when all the params are finished downloading
    """
    while not scf.cf.param.is_updated:
        time.sleep(1.0)
    rospy.loginfo('Parameters downloaded for {}'.format(scf.cf.link_uri))


def reset_estimator(scf):
    """
    Reset the Kalman estimator until it has converged
    """
    cf = scf.cf
    cf.param.set_value('kalman.resetEstimation', '1')
    time.sleep(0.1)
    cf.param.set_value('kalman.resetEstimation', '0')

    while not wait_for_position_estimator(cf):
        cf.param.set_value('kalman.resetEstimation', '1')
        time.sleep(0.1)
        cf.param.set_value('kalman.resetEstimation', '0')

    start_batt_log(cf)
    rospy.loginfo("Init done for {} ({},{})".format(cf.link_uri, home[cf.link_uri]['x'], home[cf.link_uri]['y']))

def start_batt_log(cf):
    """
    Start logging battery data
    """
    log_conf = LogConfig(name='Battery', period_in_ms=900)
    log_conf.add_variable('pm.vbat', 'float')
    log_conf.add_variable('stateEstimate.pitch', 'float')
    log_conf.add_variable('stateEstimate.roll', 'float')

    cf.log.add_config(log_conf)
    log_conf.data_received_cb.add_callback(lambda t, d, l: log_callback(cf, t, d, l))
    log_conf.start()
    

def log_callback(cf, timestamp, data, logconf):
    """
    This is called from the battery logger when new data is available
    """
    global BatArray
    BatArray[cf.link_uri].append(data['pm.vbat'])
    # print "Battery call back : average :"
    # print float(BatArray[cf.link_uri].average)
    # print cf.link_uri
    # rospy.loginfo('{}% batt = {}%'.format(cf.link_uri,data['pm.vbat']))
    # print "%s, Average: %s, id %s"%(BatArray[cf.link_uri], BatArray[cf.link_uri].average, cf.link_uri)
    if (float(BatArray[cf.link_uri].average) < 3.1 and float(params[cf.link_uri][0]['z']) > 0.5):
        rospy.logerr("{} batt too low = {}V".format(cf.link_uri,BatArray[cf.link_uri].average))
        CF_land(cf, params[cf.link_uri][0]['z'])
        #params[cf.link_uri][0]['z'] = 0.15
    else:
        if assigned:
            rospy.loginfo("{} batt = {}V".format(cf.link_uri,BatArray[cf.link_uri].average))
            #position = [params['x'], params['y'], params['z'], 0.0]
            #rospy.loginfo(str(position[0]) + "|" + str(position[1]) + "|" + str(position[2]))

    if ((abs(data['stateEstimate.roll']) > 150.0)):
        rospy.loginfo("{} ({}) Upside Down:{} {}!".format(cf.link_uri, mapCFBU[cf.link_uri], data['stateEstimate.pitch'], data['stateEstimate.roll']))
        add_death(cf.link_uri)


def add_death(id_link):
    """
    Add a crazyflie to the deaths list
    """
    global CF_deaths
    if mapCFBU[id_link] != -1:
        if mapCFBU[id_link] not in CF_deaths:
            CF_deaths.append(mapCFBU[id_link])


def publish2ROS():
    """
    Publish deaths and battery data to ROS
    """
    global pubd, pubb, CF_deaths, mapCFBU
    pubd.publish(data=CF_deaths)
    for el in BatArray:
        if mapCFBU[el]!=-1 and BatArray[el].average>0.5:
            pubb[mapCFBU[el]].publish(percentage=volt2percent[min(volt2percent, key=lambda x:abs(x-BatArray[el].average))])


def joy_callback(msg):
    """
    Called from the ROS subscriber when a new joystick message arrives
    """
    global joystick
    global CFflying
    global controlled
    
    # Button 3 makes the drones take off
    if msg.buttons[3] and not CFflying and not joystick['takeoff']:
        joystick['takeoff'] = True

    # Button 1 makes the drones land
    if msg.buttons[1] and CFflying and not joystick['land']:
        joystick['land'] = True

    # Button 0 switches control to previous drone
    if not joystick['previous'] and msg.buttons[0]:
        if controlled - 1 > 0:
            controlled -= 1
        else:
            controlled = len(mapCFBU)
        rospy.loginfo("Control switched to crazyflie {}".format(controlled))
        joystick['previous'] = True

    if joystick['previous'] and msg.buttons[0]:
        joystick['previous'] = False

    # Button 2 switches control to next drone
    if not joystick['next'] and msg.buttons[2]:
        if controlled + 1 <= len(mapCFBU):
            controlled += 1
        else:
            controlled = 1
        rospy.loginfo("Control switched to crazyflie {}".format(controlled))
        joystick['next'] = True

    if joystick['next'] and msg.buttons[2]:
        joystick['next'] = False
    
    # The joysticks control the drone like a bigger quadrotor
    joystick['thrust'] = msg.axes[1] * 0.01
    joystick['yaw'] = msg.axes[0] * 90
    joystick['roll'] = msg.axes[3]
    joystick['pitch'] = msg.axes[4]


def run_swarm(scf, params):
    """
    Main loop for the swarm
    """
    global CFflying
    global joystick

    cf = scf.cf

    if cf.link_uri in CF_deaths:
        cf.commander.send_stop_setpoint()
        return

    # Move the drones according to current situation
    if not CFflying:
        if joystick['takeoff']:
            rospy.loginfo("Taking off")
            CF_takeoff(cf, params['z'])
            CFflying = True
            joystick['takeoff'] = False
    else:
        if joystick['land']:
            rospy.loginfo("Landing")
            CF_land(cf, params['z'])
            CFflying = False
            joystick['land'] = False
        else:
            if mapCFBU[cf.link_uri] == controlled:
                params['z'] += joystick['thrust']
                cf.commander.send_hover_setpoint(
                    joystick['pitch'], joystick['roll'], joystick['yaw'], params['z'])
            else:
                cf.commander.send_hover_setpoint(0.0, 0.0, 0.0, params['z'])
    

def load_arena():
    """
    Load cf arena from the anchor pos file
    """
    with open(os.path.expanduser("/home/rafael/ROS_WS/src/mistlab_rosbuzz_sdk/misc/arena_ets.yaml"), "r") as fd:
        for id in range(0,7):
            doc = yaml.load_all(fd.readline())
            for packet in doc:
                if not packet:
                   continue
                for id, position in packet.items():
                    x = position['x']
                    y = position['y']
                    z = position['z'] 
                if x < CF_arena[0]:
                    CF_arena[0] = x
                if x > CF_arena[1]:
                    CF_arena[1] = x
                if y < CF_arena[2]:
                    CF_arena[2] = y
                if y > CF_arena[3]:
                    CF_arena[3] = y
    CF_arena[0] = CF_arena[0] + CF_arena_buffer[0]
    CF_arena[1] = CF_arena[1] - CF_arena_buffer[1]
    CF_arena[2] = CF_arena[2] + CF_arena_buffer[2]
    CF_arena[3] = CF_arena[3] - CF_arena_buffer[3]
    print("Arena initialized:")
    print CF_arena


if __name__ == '__main__':
    global controlled

    # Initialize ROS node
    rospy.init_node('crazyfliesnode', anonymous=False)

    # Initialize publishers and subscribers
    rospy.Subscriber('joy', Joy, joy_callback)
    global pubd, pubb
    pubd = rospy.Publisher('deadCFs', UInt16MultiArray, queue_size=1)
    pubb = {}
    for i in range(1,7):
        print("Create publisher "+'/robot'+str(i)+'/battery')
        pubb[i] = rospy.Publisher('/robot'+str(i)+'/battery', BatteryState, queue_size=1)
    
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)

    # Load arena dimensions
    load_arena()
    
    # Swarm and ROS loop
    factory = CachedCfFactory(rw_cache='~/CFcache')
    if mapCFBU:
        with Swarm(mapCFBU, factory=factory) as swarm:
            swarm.parallel(reset_estimator)
            swarm.parallel(wait_for_param_download)
            rospy.loginfo("Crazyflie {} is being controlled.".format(controlled))
            r = rospy.Rate(25)
            while not rospy.is_shutdown():
                swarm.parallel(run_swarm, args_dict=params)
                publish2ROS()
                r.sleep()
