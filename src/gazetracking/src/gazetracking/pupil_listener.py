#!/usr/bin/env python
import zmq
from msgpack import loads
import json
import rospy
from gazetracking.msg import PupilInfo, GazeInfo
from sensor_msgs.msg import CompressedImage
#from cv_bridge import CvBridge, CvBridgeError
import numpy as np

import cv2


class pupil_rosbridge:

	def __init__(self):

		context = zmq.Context()
		# open a req port to talk to pupil
		addr = '192.168.2.19'  # remote ip or localhost
		#addr = '192.168.2.109'
		req_port = "50020"  # same as in the pupil remote gui
		#req_port = "61098"
		self.req = context.socket(zmq.REQ)
		rospy.loginfo("Waiting for connection...")
		self.req.connect("tcp://{}:{}".format(addr, req_port))
		# ask for the sub port
		self.req.send_string('SUB_PORT')
		sub_port = self.req.recv_string()

		# open a sub port to listen to pupil
		self.sub = context.socket(zmq.SUB)
		self.sub.connect("tcp://{}:{}".format(addr, sub_port))
		self.sub.setsockopt_string(zmq.SUBSCRIBE, u'')
		
		rospy.loginfo("Socket connected.")

		# Create publishers
		self.pub_pupil = rospy.Publisher('/pupil_info', PupilInfo, queue_size=10)
		self.pub_gaze = rospy.Publisher('/gaze_info', GazeInfo, queue_size=10)

		self.ic = image_converter()

	def parse_pupil_pos(self,msg):
		''' Parse pupil_position into a PupilInfo message.

		Return PupilInfo message, or -1 if msg argument was empty.
		'''
		#msg may be a list of pupil positions
		if len(msg) < 1: return -1

		outmsg = PupilInfo()

		# Parse message data
		outmsg.timestamp = msg['timestamp']
		#outmsg.index = m['index']
		outmsg.confidence = msg['confidence']
		outmsg.norm_pos = msg['norm_pos']
		outmsg.diameter = msg['diameter']
		outmsg.method = msg['method']
		outmsg.luminance = self.ic.luminance
		

		# Parse optional data
		if 'ellipse' in msg:
			outmsg.ellipse_center = msg['ellipse']['center']
			outmsg.ellipse_axis = msg['ellipse']['axes']
			outmsg.ellipse_angle = msg['ellipse']['angle']

		# Warn that 3D data hasn't been parsed
		# TODO: parse 3D data
		if 'method' == '3d c++':
			rospy.logwarn("3D information parser not yet implemented,\
				3D data from JSON message not included in ROS message.")

		return outmsg

	def parse_gaze_pos(self,msg):
		''' Parse gaze_positions into a GazeInfo message.

		Return GazeInfo message, or -1 if msg argument was empty.
		'''
		#msg may be a list of gaze positions
		if len(msg) < 1: return -1
		
		outmsg = GazeInfo()

		# Parse message data
		outmsg.timestamp = msg['timestamp']
		#outmsg.index = m['index']
		outmsg.confidence = msg['confidence']
		outmsg.norm_pos = msg['norm_pos']

		return outmsg

	# Helper function that prints topic and message in a readable format
	def prettyprint(self,topic, msg):
		string = "\n\n" + str(topic) + ":\n" + str(msg)
		return string

	def run_bridge(self):
		# Receive message from socket, convert it to Python dict
		#print("Getting msg from pupil")
		topic, msgstr = self.sub.recv_multipart()
		msg = loads(msgstr)
		#print "" + str(topic) + ": " + str(msg)

		# Convert message to ROS message
		if "pupil" in topic:
			#rospy.logdebug("Reading pupil position: \n" + self.prettyprint(topic, msg))
			
			# Parse and publish
			outmsg = self.parse_pupil_pos(msg)
			if not outmsg == -1: self.pub_pupil.publish(outmsg)

		elif topic == "gaze.2d.0.":
			#rospy.logdebug("Reading pupil position: \n" + self.prettyprint(topic, msg))
			
			# Parse and publish
			outmsg = self.parse_gaze_pos(msg)
			if not outmsg == -1: self.pub_gaze.publish(outmsg)

		elif topic == "dt":
			pass #rospy.logdebug("Ignoring dt: " + str(msg))

		else:
			pass #rospy.logerr("Unrecognized topic from socket: " + topic)

class image_converter:

  def __init__(self):
	  #self.bridge = CvBridge()
	  self.image_sub = rospy.Subscriber("/world/image/compressed",CompressedImage,self.callback,queue_size = 1)
	  self.luminance = 0.0

  def callback(self,data):
    np_arr = np.fromstring(data.data, np.uint8)
    cv_image = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
	
	# Convert BGR to HSV
    hsv_img = cv2.cvtColor(cv_image, cv2.COLOR_BGR2HSV)
    self.luminance = np.mean(hsv_img[:,:,2])

    #cv2.putText(cv_image, "{:.4f}".format(self.luminance), (5,700), cv2.FONT_HERSHEY_SCRIPT_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)

    #cv2.imshow("Image window", cv_image)
    #cv2.waitKey(3)

if __name__ == "__main__":

	rospy.loginfo("Starting pupil listener.")
	print("Starting pupil listener")
	rospy.init_node('pupillistener')

	pb = pupil_rosbridge()

	print("listening for socket message....")
	while not rospy.is_shutdown():
		pb.run_bridge()

	cv2.destroyAllWindows()

