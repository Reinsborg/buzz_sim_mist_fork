#pragma once
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sstream>
#include <mavros_msgs/Mavlink.h>
#include <iostream>
//#define ROBOTS_NUM 10
#define MAX_BUZZ_MSG_SIZE_XBEE 250
#define MAX_BUZZ_MSG_SIZE_HEAVEN 3000
using namespace std;

class sub_stream{
public:
	sub_stream();
	~sub_stream();
	void Initialize(ros::NodeHandle& n_c,const char* name);
	uint64_t* get_payload();
	int get_payload_id();
	void clear_payload();
	void run();
	int get_payloadsize();
private:
	ros::Subscriber m_sub;
	void collect_payload(const mavros_msgs::Mavlink::ConstPtr& msg);
	void insert_msg(uint64_t* in_msg,int msg_id, int msg_size);
	uint64_t* cur_msg;
	uint64_t* next_msg;
	int cur_msgid,next_msgid,cur_msgSize=0,next_msgSize=0;
};

class hub{
public:
	hub(ros::NodeHandle& n_c, int ROBOTS, std::string rosbuzz_namespace_prefix,
	std::string in_name, std::string out_name, float packet_drop_prob);
	~hub();
	void run();
    void Initialize_pub_sub(ros::NodeHandle& n_c, int ROBOTS, int MIN_ROBOT_ID, std::string net_name);
	
private:
	int bernoulli();
	std::vector<ros::Publisher> m_publisher_vec;   
	std::vector<sub_stream> m_subscriber_vec; 
	std::string  base_in_topic, base_out_topic;
	std::string  base_node;
	int m_robots;
	float m_packet_drop_prob;
	int max_buzzmsg_size = ceil ( (double)MAX_BUZZ_MSG_SIZE_HEAVEN/(double)sizeof(uint64_t) );
};

uint16_t* u64_cvt_u16(uint64_t u64);

