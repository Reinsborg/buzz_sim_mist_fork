/** @file      main.cpp
 *  @version   1.0
 *  @date      04.17.2018
 *  @brief     Communication Medium Implementation as a node in ROS.
 *  @author    Vivek Shankar Varadharajan and David St-Onge
 *  @copyright 2018 MistLab. All rights reserved.
 */

#include "hub.h"



int main(int argc, char **argv)
{
	ros::init(argc, argv, "rosbuzz_com_hub");
	ros::NodeHandle m_nh;
	ros::NodeHandle m_n_local("~");
	int ROBOTS_NUM, MIN_ROBOT_ID;
	float packet_drop_prob;
	std::string rosbuzz_namespace_prefix,in_name,out_name, net_name;
	if(m_n_local.getParam("number_of_robots", ROBOTS_NUM));
	else {ROS_ERROR("Provide number of robots in Launch file"); system("rosnode kill rosbuzz_com_hub");}
	if(m_n_local.getParam("min_robot_id", MIN_ROBOT_ID));
	else {ROS_ERROR("Provide min robot id in Launch file"); system("rosnode kill rosbuzz_com_hub");}
	if(m_n_local.getParam("packet_drop_probability", packet_drop_prob)){
		if(packet_drop_prob<0 || packet_drop_prob>1){
			ROS_ERROR("Provide a packet drop probability between 0 to 1"); 
			system("rosnode kill rosbuzz_com_hub");
		} 
	}
	else {ROS_ERROR("Provide a packet drop to simulate in Launch file"); system("rosnode kill rosbuzz_com_hub");}
	if(m_n_local.getParam("rosbuzz_prefix", rosbuzz_namespace_prefix));
	else {ROS_ERROR("Provide rosbuzz_namespace prefix in Launch file"); system("rosnode kill rosbuzz_com_hub");}
	if(m_n_local.getParam("in_payload", in_name));
	else {ROS_ERROR("Provide in_payload name sufix in Launch file"); system("rosnode kill rosbuzz_com_hub");}
	if(m_n_local.getParam("out_payload", out_name));
	else {ROS_ERROR("Provide out_payload name sufix in Launch file"); system("rosnode kill rosbuzz_com_hub");}
	if(m_n_local.getParam("network", net_name));
	else {ROS_ERROR("Provide network type in Launch file"); system("rosnode kill rosbuzz_com_hub");}
    hub com_hub(m_nh,ROBOTS_NUM,rosbuzz_namespace_prefix,in_name,out_name,packet_drop_prob);
	com_hub.Initialize_pub_sub(m_nh,ROBOTS_NUM,MIN_ROBOT_ID,net_name);
	while (ros::ok())
	{
		com_hub.run();
		/*run once*/
		ros::spinOnce();
		/*loop rate of ros*/
		ros::Rate loop_rate(100);
		loop_rate.sleep();
	}


	return 0;
}
