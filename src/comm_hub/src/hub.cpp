/** @file      hub.cpp
 *  @version   1.0
 *  @date      04.17.2018
 *  @brief     Communication Medium Implementation as a node in ROS.
 *  @author    Vivek Shankar Varadharajan and David St-Onge
 *  @copyright 2018 MistLab. All rights reserved.
 */

#include "hub.h"

	

	/*---------------
	/Constructor
	---------------*/
	hub::hub(ros::NodeHandle& n_c, int ROBOTS, std::string rosbuzz_namespace_prefix,
		std::string in_name, std::string out_name, float packet_drop_prob): base_in_topic(in_name.c_str()),
							    base_out_topic(out_name.c_str()),
							    base_node(rosbuzz_namespace_prefix.c_str()),
							    m_publisher_vec(ROBOTS+1,ros::Publisher()),
							    m_subscriber_vec(ROBOTS+1,sub_stream()),
								m_packet_drop_prob(packet_drop_prob)	
	{
		ROS_INFO("rosbuzz_com_hub");
	}

	/*---------------------
	/Destructor
	---------------------*/
	hub::~hub(){}

	void hub::Initialize_pub_sub(ros::NodeHandle& n_c,int ROBOTS, int MIN_ROBOT_ID, std::string net_name){
		m_robots=ROBOTS+1; //to count for the GS
		int robot_id_ctr = MIN_ROBOT_ID;
		stringstream pub_name;
		stringstream sub_name;
		// Create the pub/sub for GS
		pub_name<<"/"<<base_out_topic;//+1
		ROS_INFO(pub_name.str().c_str());
		m_publisher_vec[0]=n_c.advertise<mavros_msgs::Mavlink>(pub_name.str().c_str(), 1000);
		sub_name<<"/"<<base_in_topic;//+1
		ROS_INFO(sub_name.str().c_str());
		m_subscriber_vec[0].Initialize(n_c,sub_name.str().c_str());
		// Create the pub/sub for each drone
		for(int i=1;i<m_robots;i++){
			pub_name.str(std::string());
			sub_name.str(std::string());
			pub_name<<"/"<<base_node<<robot_id_ctr<<"/"<<base_out_topic;//+1
			ROS_INFO(pub_name.str().c_str());
			m_publisher_vec[i]=n_c.advertise<mavros_msgs::Mavlink>(pub_name.str().c_str(), 1000);
			sub_name<<"/"<<base_node<<robot_id_ctr<<"/"<<base_in_topic;//+1
			ROS_INFO(sub_name.str().c_str());
			m_subscriber_vec[i].Initialize(n_c,sub_name.str().c_str());
			robot_id_ctr++;
		}
		if(net_name == "xbee")
			max_buzzmsg_size = ceil ( (double)MAX_BUZZ_MSG_SIZE_XBEE/(double)sizeof(uint64_t) );
		else if(net_name == "heaven")
			max_buzzmsg_size = ceil ( (double)MAX_BUZZ_MSG_SIZE_HEAVEN/(double)sizeof(uint64_t) );
	}
	void hub::run(){

		for(int i=0;i<m_robots;i++){
			m_subscriber_vec[i].run();
			uint64_t* cur_payload = m_subscriber_vec[i].get_payload();
			if(cur_payload){
				int payload_size = m_subscriber_vec[i].get_payloadsize(); 
				mavros_msgs::Mavlink payload_out;
				for(int cp=0; cp<payload_size;cp++){	
					payload_out.payload64.push_back(cur_payload[cp]);
				}
				payload_out.sysid=i;//+1;
				payload_out.msgid=m_subscriber_vec[i].get_payload_id();
				//std::copy(cur_payload, cur_payload+payload_size, payload_out.payload64);
				if(payload_size > max_buzzmsg_size)
					ROS_ERROR("[HUB] robot%i sent payload_size too large: %i/%i",i,payload_size,max_buzzmsg_size);
				else
					;//ROS_INFO("[HUB] robot%i sent payload_size: %i/%i",i,payload_size,max_buzzmsg_size);
				for(int j=0;j<m_robots;j++){
					if(i != j){	
						if(payload_size > max_buzzmsg_size){
							;//m_publisher_vec[j].publish(payload_out);
						}
						else if(bernoulli()){
							m_publisher_vec[j].publish(payload_out);	// send the payload to all except the current robot

						}
						
					}		
					
				}
				m_subscriber_vec[i].clear_payload();
			}
		}

	}
	int hub::bernoulli(){
		float x = (float)rand()/(float)(RAND_MAX/1);
		if(m_packet_drop_prob < x) return 1;
		return 0;
	}
	/********************************/
	/*Constructor*/
	/********************************/
	sub_stream::sub_stream():m_sub(),cur_msg(NULL),next_msg(NULL){

	}
	/********************************/
	/*Destructor*/
	/********************************/
	sub_stream::~sub_stream(){}
	
	void sub_stream::Initialize(ros::NodeHandle& n_c,const char* name){	
		m_sub=n_c.subscribe(name, 1000, &sub_stream::collect_payload,this);
	}
	void sub_stream::run(){
		if(!cur_msg && next_msg){
			cur_msg=next_msg;
			cur_msgid=next_msgid;
			cur_msgSize=next_msgSize;
			next_msg=NULL;
		}
	}

	void sub_stream::collect_payload(const mavros_msgs::Mavlink::ConstPtr& msg){
		uint64_t* in_msg = new uint64_t[msg->payload64.size()];
		std::copy(msg->payload64.begin(), msg->payload64.end(), in_msg);	
		insert_msg(in_msg,msg->msgid,msg->payload64.size());
		//msg->payload64.size()	
		//uint16_t* out = buzz_utility::u64_cvt_u16((uint64_t)*(message_obt+3));
	}
	void sub_stream::insert_msg(uint64_t* in_msg, int msg_id, int msg_size){
		if(!cur_msg && !next_msg){
			next_msg=in_msg;
			next_msgid=msg_id;
			next_msgSize=msg_size;
		}
		else if(!cur_msg && next_msg){
			cur_msg=next_msg;
			cur_msgid=next_msgid;
			cur_msgSize=next_msgSize;
			next_msg=in_msg;
			next_msgid=msg_id;
			next_msgSize=msg_size;
		}
		else if(cur_msg && !next_msg){
			next_msg=in_msg;
			next_msgid=msg_id;
			next_msgSize=msg_size;
		}
		else if(cur_msg && next_msg){
			delete[] cur_msg;
			cur_msg = next_msg;
			cur_msgid = next_msgid;
			cur_msgSize= next_msgSize;
			next_msg=in_msg;
			next_msgid= msg_id;
			next_msgSize=msg_size;
		}
	}
	int sub_stream::get_payloadsize(){
	return cur_msgSize;
	}
	uint64_t* sub_stream::get_payload(){
	return cur_msg;
	}
	int sub_stream::get_payload_id(){
		return cur_msgid;
	}
	void sub_stream::clear_payload(){
		delete[] cur_msg;
		cur_msg=NULL;
		cur_msgid=0;
		cur_msgSize=0;
	}
	
