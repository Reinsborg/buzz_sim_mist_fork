var NAMESPACE = '/robot0';
var myid = 0;

var TIME_INTERVAL_BETWEEN_FETCH = 250; // In ms
var cmdService, topicssrv, ros;
var listener, listenerPos, listenerNei, listenerBVM, listenerBat;
var BVMstate = "TURNEDOFF", battery = 0.0;
var target_topic = "/";
var autoupdate_timer;
var isFlying = false;
var isArm = false;
var centralform = false;
var start_freq = new Date().getTime();


/**
* Setup all visualization elements when the page is loaded. 
**/
function init() {

   	ros = new ROSLIB.Ros({
		url : 'ws://' + location.host + ':9090'
	});

   	document.getElementById('connecting').style.display = 'block';
	//document.getElementById('connected').style.display = 'block'; 
   	document.getElementById('connected').style.display = 'none';
   	document.getElementById('error').style.display = 'none';

   	ros.on('connection', function() {
   		console.log('Connected to websocket server.');
	   	document.getElementById('connecting').style.display = 'none';
	   	document.getElementById('connected').style.display = 'block';
	   	document.getElementById('error').style.display = 'none';
   	});

   	ros.on('error', function(error) {
   		console.log('Error connecting to websocket server: ', error);
	   	document.getElementById('connecting').style.display = 'none';
	   	document.getElementById('connected').style.display = 'none';
	 	document.getElementById('error').style.display = 'block';

		var indentedMsgOutput = JSON.stringify(error, null, 4);
		document.getElementById('errorMsg').innerHTML = indentedMsgOutput;
   	});

   	ros.on('close', function() {
   		console.log('Connection to websocket server closed.');
   	});

    console.dir(ros);

	populateActionlist();

	initMap();

	initEditor();

	topicssrv = new ROSLIB.Service({
			ros : ros,
			name : '/rosapi/node_details',
			serviceType : '/rosapi/Node_Details'
	});

   	// Subscribe to the position topic
   	listenerPos = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/local_position/pose',
   		messageType : 'geometry_msgs/PoseStamped'
   	});

   	listenerPos.subscribe(function(msgPosition) {
		   var pose = "Position[m]      " + 
   			" X:" + msgPosition.pose.position.x.toFixed(2) + 
   			"   Y:" + msgPosition.pose.position.y.toFixed(2) +
   			"   Z:" + msgPosition.pose.position.z.toFixed(2);

   			var qx, qy, qz ,qw;
   			qx =   msgPosition.pose.orientation.x;
   			qy =  msgPosition.pose.orientation.y;
   			qz =    msgPosition.pose.orientation.z;
   			qw =    msgPosition.pose.orientation.w;

   			var abridge = pose + "<br/>Orientation [quaternion]:" +
   			" [" + qx.toFixed(3) +
   			" : " + qy.toFixed(3) +
   			" : " + qz.toFixed(3) + " : " + qw.toFixed(3) + "]";

			document.getElementById('curposition').innerHTML = abridge;
	});

	listenerGPos = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/global_position/global',
   		messageType : 'sensor_msgs/NavSatFix'
   	});

   	listenerGPos.subscribe(function(msgPosition) {
		features.forEach(function(feature, index, fetarray) {
				if(feature.id==myid) {
					fetarray[index].lat=msgPosition.latitude;
					fetarray[index].lg=msgPosition.longitude;
				}
			});
		if(centralform && (new Date().getTime() - start_freq) > 2000){
			start_freq = new Date().getTime();
			centralformation();
		}
	});

	listener = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/extended_state',
   		messageType : 'mavros_msgs/ExtendedState'
   	});

   	listener.subscribe(function(msgstate) {
   			var abridge = "State: " + BVMstate + " [";
			if(msgstate.landed_state == 1)
				abridge = abridge + "LANDED]";
			if(msgstate.landed_state == 2)
				abridge = abridge + "FLYING]";
			abridge = abridge + "<br/>Battery level: " + battery.toFixed(3) + "%";
			document.getElementById('curstate').innerHTML = abridge;
   	});

	listenerBVM = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/bvmstate',
   		messageType : 'std_msgs/String'
   	});

   	listenerBVM.subscribe(function(msgstate) {
   			BVMstate = msgstate.data;

			var newflyst = isFlying;
			if(BVMstate.localeCompare('TURNEDOFF')==0 || BVMstate.localeCompare('STOP')==0)
				newflyst = false;
			else
				newflyst = true;
			if(newflyst!=isFlying){
				isFlying = newflyst;
				populateActionlist();
			}
   	});

	listenerBat = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/battery',
   		messageType : 'sensor_msgs/BatteryState'
   	});

   	listenerBat.subscribe(function(msgbattery) {
   			battery = msgbattery.percentage;
   	});

	listenerNeiGPS = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/neighbours_pos',
   		messageType : 'rosbuzz/neigh_pos'
   	});

   	listenerNeiGPS.subscribe(function(msgneighbors) {
   			var abridge = '<table class="alt"><thead><tr><th>Robot ID</th><th>Latitude</th><th>Longitude</th></tr></thead><tbody>';
			abridge = abridge + '<tr><td style="color:red">' + features[0].id + '</td><td style="color:red">' + features[0].lat.toFixed(6) + '</td><td style="color:red">' + features[0].lg.toFixed(6) + "</td></tr>";
			var results = msgneighbors.pos_neigh;
			for(var i in results) {
				abridge = abridge + "<tr><td>" + results[i].position_covariance_type.toFixed(0) + "</td><td>" + results[i].latitude.toFixed(6) + "</td><td>" + results[i].longitude.toFixed(6) + "</td></tr>";
				var exist = false;
				features.forEach(function(feature, index, fetarray) {
					if(feature.id==results[i].position_covariance_type) {
						fetarray[index].lat=results[i].latitude;
						fetarray[index].lg=results[i].longitude;
						exist = true;
					}
				});
				if(!exist) {
					features.push({id: results[i].position_covariance_type,
							lat: results[i].latitude,
							lg: results[i].longitude,
							type: 'other',
							marker: null});
				}
        	}
			abridge = abridge + "</tbody></table>";
			document.getElementById('neighbors').innerHTML = abridge;
			updateMap();
   	});
    
    cmdService = new ROSLIB.Service({
        ros : ros,
        name : NAMESPACE+'/buzzcmd',
        serviceType : 'mavros_msgs/CommandLong'
    });

	updateTopicsList();

}

/**
 Does actions related to ROS topics/publish
 **/

 function populateActionlist(){
	 var Alist = '';
	 if(!isFlying){
		Alist = '<li><a class="button special icon fa-download" onClick="sendCommand(22)">Take Off</a></li> <li><a class="button special icon fa-download" onClick="sendCommand(400)">Arm/Disarm</a></li>';
	 }else{
		Alist = '<li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(21)">Land</a></li> <li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(20)">Home</a></li> <li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(900)">Allocate</a></li> <li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(901)">Pursuit</a></li> <li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(902)">Aggregate</a></li> <li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(903)">Formation</a></li>';
	 }
	 document.getElementById('actionlist').innerHTML = Alist;
 }

 function setBehavior(){
	// We fetch the behavior
	var target_behavior = document.getElementById('behaviorslist').value;
	console.log('Send behavior: ' + target_behavior);
 }

 function sendPositionA(lat, lng) {
	 sendpositionmsg(features[0].id, lat, lng, parseFloat($( "#Z" ).val()));
	 sendpositionmsg(features[0].id, lat, lng, parseFloat($( "#Z" ).val()));
	 centralformation(lat, lng);
 }

 function sendPosition() {
	 sendpositionmsg(features[0].id, parseFloat($( "#X" ).val()), parseFloat($( "#Y" ).val()), parseFloat($( "#Z" ).val()));
	 sendpositionmsg(features[0].id, parseFloat($( "#X" ).val()), parseFloat($( "#Y" ).val()), parseFloat($( "#Z" ).val()));
	 centralformation(parseFloat($( "#X" ).val()), parseFloat($( "#Y" ).val()));
 }


 function centralformation(lat, lng) {
	var formlist = [gpsfromvec(-3, -4, lat, lng),gpsfromvec(3, -4, lat, lng),gpsfromvec(-5, -8, lat, lng),gpsfromvec(0, -8, lat, lng),gpsfromvec(5, -8, lat, lng)];

	 features.forEach(function(feature, index, fetarray) {
		if(index!=0) {
	 		sleep(200);
			sendpositionmsg(feature.id, formlist[index-1][0], formlist[index-1][1], parseFloat($( "#Z" ).val()));
			sendpositionmsg(feature.id, formlist[index-1][0], formlist[index-1][1], parseFloat($( "#Z" ).val()));
		}
	 });
 }

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

 function gpsfromvec(dx, dy, lat, lg) {
	var d_lat = (dx / 6371000)*180.0/Math.PI;
	Lgoal_lat = d_lat + lat;
	var d_lon = (dy / (6371000 * Math.cos((Lgoal_lat)*Math.PI/180.0)))*180.0/Math.PI;
	Lgoal_long = d_lon + lg;
	/*var Vrange = Math.sqrt(dx*dx+dy*dy)
	var Vbearing = Math.atan(dy, dx)
	var latR = lat*Math.PI/180.0;
	var lonR = lg*Math.PI/180.0;
	var target_lat = Math.asin(Math.sin(latR) * Math.cos(Vrange/6371000.0) + Math.cos(latR) * Math.sin(Vrange/6371000.0) * Math.cos(Vbearing));
	var target_lon = lonR + Math.atan(Math.sin(Vbearing) * Math.sin(Vrange/6371000.0) * Math.cos(latR), Math.cos(Vrange/6371000.0) - Math.sin(latR) * Math.sin(target_lat));
	Lgoal_lat = target_lat*180.0/Math.PI;
	Lgoal_long = target_lon*180.0/Math.PI;*/

	return [Lgoal_lat, Lgoal_long]
 }

 function sendpositionmsg(id, la, lo, al) {
	var request = new ROSLIB.ServiceRequest({
 		broadcast: false,
        command: 16,
        confirmation: 0,
        param1: id,
        param2: 0.0,
        param3: 0.0,
        param4: 0.0,
        param5: la,
        param6: lo,
        param7: al
 	});
 	console.log(request);
 	cmdService.callService(request, function(result) {
        console.log('Result for service call on '
        + cmdService.name
        + ': '
        + result.success);
    });
}
 function sendCommand(cmdID) {
	 var p1 = 0.0;
	 if(cmdID == 400){
		 if(!isArm)
	 		p1 = 1.0;
		 isArm = !isArm;
	 }
	 if(cmdID == 903)
	 	centralform = true;
	else {
		var request = new ROSLIB.ServiceRequest({
			broadcast: false,
			command: cmdID,
			confirmation: 0,
			param1: p1,
			param2: 0.0,
			param3: 0.0,
			param4: 0.0,
			param5: 0.0,
			param6: 0.0,
			param7: 0.0
		});
		console.log(request);
		cmdService.callService(request, function(result) {
			console.log('Result for service call on '
			+ cmdService.name
			+ ': '
			+ result.success);
		});
	}
 }

 function setTargetTopic(){
	// We fetch the message type with is name
	target_topic = document.getElementById('topics').value;
	console.log('Name: ' + target_topic);

	// We unsubscribe from the old topic, if it has been define
	if (typeof listener !== 'undefros.getTopicsined')
		listener.unsubscribe();

	// Subscribe to new topic
	subscribeTargetTopic();
}


function subscribeTargetTopic(){
	ros.getTopicType(target_topic, 
		function(type){
			//console.log(type);
			console.log("Update topic message");
			listener = new ROSLIB.Topic({
				ros : ros,
				name : target_topic,
				messageType : type,
				throttle_rate: TIME_INTERVAL_BETWEEN_FETCH, // Time interval between fecth in ms
				queue_size: 1
			});

			listener.subscribe(function(message) {
				//We check that it's not an image before converting it to text
				if(type.match("/Image$")){
					document.getElementById('topic-out').innerHTML = "Use the image stream on the left for displaying image";
				}
				else{
					var indentedMsgOutput = JSON.stringify(message, null, 4);
					document.getElementById('topic-out').innerHTML = indentedMsgOutput;
				}
			});
		});
}

function updateTopicsList(topics){
	console.log("Refreshing topics")
	var request = new ROSLIB.ServiceRequest({node: NAMESPACE+'/rosbuzz_node'});

	var selectlist = document.getElementById('topics');
	if (selectlist.length > 0) {
		selectlist.remove(selectlist.length-1);
	}
	var option = {};

	topicssrv.callService(request, function(result) {
		console.log(result)
		for (topic in result.publishing) {
			//console.log(topic.desc)
			option = document.createElement("option");
			option.text = result.publishing[topic];
			selectlist.add(option);
		}
		for (topic in result.subscribing) {
			//console.log(topic.desc)
			option = document.createElement("option");
			option.text = result.subscribing[topic];
			selectlist.add(option);
		}
	});
}

/**
 Functions related to Buzz script editor
 **/
var editor = ace.require("ace/editor");
var EditSession = ace.require("ace/edit_session").EditSession;
var UndoManager = ace.require("ace/undomanager").UndoManager;
var net = ace.require("ace/lib/net");
var docs = {
    "main": "../rosbuzz/buzz_scripts/main.bzz",
	"testLJ": "../rosbuzz/buzz_scripts/testLJ.bzz",
	"minimal": "../rosbuzz/buzz_scripts/minimal.bzz",
	"include/states": "../rosbuzz/buzz_scripts/include/act/states.bzz",
	"include/barrier": "../rosbuzz/buzz_scripts/include/act/barrier.bzz"
};

function initEditor(){
		editor = ace.edit("editor",{maxLines:60, minLines:10});
		editor.setTheme("ace/theme/xcode");
		//var filePath = "../rosbuzz/buzz_scripts/main.bzz";
		loadDoc("main", function(session) {
			if (session) {
				editor.setSession(session);
			}
		});
		editor.saveButton = document.getElementById("saveButton");
		editor.saveButton.editor = editor;
		editor.saveButton.onclick = function() {
			name = document.getElementById('scripts').value;
			upload(docs[name], editor.getValue());
		};
		updateScriptsList();

}

function updateScriptsList(){
	console.log("Refreshing Scripts list")
	var list = (Object.keys(docs))
	var selectlist = document.getElementById('scripts');
	if (selectlist.length > 0) {
		selectlist.remove(selectlist.length-1);
	}
	var option = {};
	for (doc in docs) {
		option = document.createElement("option");
		option.text = doc;
        selectlist.add(option);
    }	
}

function setTargetScript(){
	// We fetch the message type with is name
	name = document.getElementById('scripts').value;
	console.log('Loading: ' + name);

	loadDoc(name, function(session) {
		if (session) {
			editor.setSession(session);
		}
	});
}

function loadDoc(name, callback) {
	var path = docs[name];
    net.get(path, function(x) {
        var session = new EditSession(x);
		session.setUndoManager(new UndoManager());
		session.name = "test";
		session.setMode("ace/mode/python");
		session.setTabSize(2);
		callback(session);
    });
}

function upload(url, data) {
    var absUrl = net.qualifyURL(url);
    if (/^file:/.test(absUrl))
        absUrl = "http://localhost:8888/" + url;
    url = absUrl;
    if (!/^https?:/.test(url))
        return ;
    var xhr = new XMLHttpRequest();
    xhr.open("PUT", url, true);
	xhr.setRequestHeader('Content-type','text/plain; charset=utf-8');
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(!/^2../.test(xhr.status));
        }
    };
    xhr.send(data);
}

/**
 Functions related to Map API
 **/
 var features = [{
					id: myid,
					lat: 45.508488,
					lg: -73.154101,
					type: 'stake',
					marker: null
          		}];
var stakeIcon = L.icon({
				iconUrl: 'images/uavlogo.png',
				//shadowUrl: 'images/uavlogo.png',

				iconSize:     [20, 20], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});
var otherIcon = L.icon({
				iconUrl: 'images/uavlogo2.png',
				//shadowUrl: 'images/uavlogo2.png',

				iconSize:     [20, 20], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});

var mymap = {};

var popup = L.popup();

function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("<p style='text-align:center;'>Target <br/>( " + e.latlng.lat.toFixed(6) + " , " + e.latlng.lng.toFixed(6) + " )</p>")
        .openOn(mymap);
	
	sendPositionA(e.latlng.lat,e.latlng.lng);
}

function initMap() {
	var myLatLng = {lat: 45.510358, lng: -73.609384};

	mymap = L.map('mapid').setView([myLatLng.lat, myLatLng.lng], 18);

	// create the tile layer with correct attribution
	//var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmUrl='maps/{z}/{x}/{y}.png';
	var osmAttrib='Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
	var osm = new L.TileLayer(osmUrl, {minZoom: 14, maxZoom: 21, attribution: osmAttrib});	
	mymap.addLayer(osm);

	// Create markers.
	features.forEach(function(feature, index, fetarray) {
		fetarray[index].marker = L.marker([feature.lat, feature.lg], {icon: stakeIcon}).addTo(mymap);
	});

	mymap.on('click', onMapClick);
}

function updateMap() {
	// Create markers.
	//console.log(features.length)
	var mlat = 0, mlg = 0;
	features.forEach(function(feature, index, fetarray) {
		if(feature.marker!=null)
			fetarray[index].marker.setLatLng([feature.lat, feature.lg]);
		else {
			fetarray[index].marker = L.marker([feature.lat, feature.lg], {icon: otherIcon}).addTo(mymap);
		}
		if(mlat==0) {
			mlat = feature.lat;
			mlg = feature.lg;
		} else {
			mlat = mlat + feature.lat;
			mlg = mlg + feature.lg;
		}
	});
	mlat = mlat / features.length;
	mlg = mlg / features.length;
	//mymap.setView([features[0].lat, features[0].lg], 18);
}
