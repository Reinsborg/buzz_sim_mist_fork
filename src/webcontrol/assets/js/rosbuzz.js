var NAMESPACE = ''; // Empty if using groundstation, 'mavros' if running on Spiri or Solo, 'robotX' if using a robot in SITL
var myid = 0;	// Network id of the drone or groundstation running this webcontrol

// Settings for the groundstation
var GS = 1;	// 0 = not using a groundstation - directly connected to a drone apache server
/*/ CF study GS
var mylat = 29.020883;
var mylng = -13.715417;
// Pangeae first site
var mylat = 29.020752;
var mylng = -13.714749;
// Pangeae final site
var mylat = 29.0676;
var mylng = -13.663019;*/
// Cepsum field
var mylat = 45.510369;
var mylng = -73.609415;

// Control switch: waypoint (1), swarm deployment (0)
var WP = 0;

var popmenu = 1


var TIME_INTERVAL_BETWEEN_FETCH = 250; // In ms
var cmdService, topicssrv, ros, presstopic;
var listener, listenerPos, listenerNei, listenerBVM, listenerBat;
var BVMstate = "TURNEDOFF", battery = 0.0;
var target_topic = "/";
var autoupdate_timer;
var isFlying = false;
var isArm = false;
var start_freq = new Date().getTime();

/**
* Setup all visualization elements when the page is loaded. 
**/
function init() {
	console.log(window.location.hostname);

   	ros = new ROSLIB.Ros({
		url : 'ws://'+window.location.hostname+':9090'	//'ws://192.168.12.100:9090'
	});

   	document.getElementById('connecting').style.display = 'block';
	//document.getElementById('connected').style.display = 'block'; 
   	document.getElementById('connected').style.display = 'none';
   	document.getElementById('error').style.display = 'none';

   	ros.on('connection', function() {
   		console.log('Connected to websocket server.');
	   	document.getElementById('connecting').style.display = 'none';
	   	document.getElementById('connected').style.display = 'block';
	   	document.getElementById('error').style.display = 'none';
   	});

   	ros.on('error', function(error) {
   		console.log('Error connecting to websocket server: ', error);
	   	document.getElementById('connecting').style.display = 'none';
	   	document.getElementById('connected').style.display = 'none';
	 	document.getElementById('error').style.display = 'block';

		var indentedMsgOutput = JSON.stringify(error, null, 4);
		document.getElementById('errorMsg').innerHTML = indentedMsgOutput;
   	});

   	ros.on('close', function() {
   		console.log('Connection to websocket server closed.');
   	});

    console.dir(ros);

	populateActionlist();
	if(popmenu)
		populateActionlist2();

	initMap();

	topicssrv = new ROSLIB.Service({
			ros : ros,
			name : '/rosapi/node_details',
			serviceType : '/rosapi/Node_Details'
	});

   	// Subscribe to the position topic
    listenerBVM = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/bvmstate',
   		messageType : 'std_msgs/String'
   	});

   	listenerBVM.subscribe(function(msgstate) {
   			BVMstate = msgstate.data;

			var newflyst = isFlying;
			if(BVMstate.localeCompare('TURNEDOFF')==0 || BVMstate.localeCompare('STOP')==0)
				newflyst = false;
			else
				newflyst = true;
			if(newflyst!=isFlying){
				isFlying = newflyst;
				populateActionlist();
				if(popmenu)
					populateActionlist2();
			}
			if(BVMstate.localeCompare('DEPLOY')==0 && WP==1)
				WP = 0;
			else if(BVMstate.localeCompare('WAYPOINT')==0 && WP==0)
				WP = 1;
			else if(BVMstate.localeCompare('CHAIN_FORMATION')==0 && WP==0)
				WP = 0;


			features.forEach(function(feature, index, featarray) {
                if(feature.id==myid) {
                    featarray[index].state=msgstate.data;
                }
            });
   	});

	listenerBat = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/battery',
   		messageType : 'sensor_msgs/BatteryState'
   	});

   	listenerBat.subscribe(function(msgbattery) {
   			//battery = msgbattery.percentage;
            features.forEach(function(feature, index, featarray) {
                if(feature.id==myid) {
                    featarray[index].bat=msgbattery.percentage*100;
                }
            });
   	});

	listenerCFs = new ROSLIB.Topic({
   		ros : ros,
   		name : '/deadCFs',
   		messageType : 'std_msgs/UInt16MultiArray'
   	});

   	listenerCFs.subscribe(function(msgCFs) {
   			var results = msgCFs.data;
   			//console.log('Got dead CFs: ' + results);
            for(var i in results) {
				features.forEach(function(feature, index, featarray) {
					if(feature.id==results[i]) {
						featarray[index].active = 0;
					}
				});
			}
   	});

	listenerNei = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/neighbours_pos',
   		messageType : 'rosbuzz/neigh_pos'
   	});

   	listenerNei.subscribe(function(msgneighbors) {
   			var results = msgneighbors.pos_neigh;
			var updatedindex = [];
			for(var i in results) {
				updatedindex.push(results[i].position_covariance_type);
				var exist = false;
				//console.log(results[i].position_covariance_type)
				features.forEach(function(feature, index, fetarray) {
					if(feature.id==results[i].position_covariance_type){
						if(feature.active) {
							fetarray[index].lat=results[i].latitude;
							fetarray[index].lng=results[i].longitude;
						}
						//fetarray[index].active = 0;
						exist = true;
					}
				});
				if(!exist) {
					features.push({id: results[i].position_covariance_type,
							lat: results[i].latitude,
							lng: results[i].longitude,
							bat: 100,
							active: 1,
							state:	'TURNEDOFF',
							type: 'other',
							marker: null});
				}
        	}

			/*features.forEach(function(feature, index, fetarray) {
				var res = updatedindex.find(Ui => Ui === feature.id);
				if(typeof(res)=="undefined" || res.length < 1){
					fetarray.splice(index, 1);
					console.log("Removing neighbor "+feature.id);
					Amarkers.eachLayer(function (layer) {
						if(feature.id==parseInt(layer._icon.id))
							Amarkers.removeLayer(layer);
					});
					fetarray[index].active = 0;
				}
			});*/


			updateNeiGUI();
			
			updateMap();
   	});

	listenerNeiState = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/fleet_status',
   		messageType : 'mavros_msgs/Mavlink'
   	});

	listenerNeiState.subscribe(function(msgneighbors) {
		var results = msgneighbors.payload64;
		var nb = results.length/5;
		for(var i=0; i<nb; i++) {
			var exist = false;
			features.forEach(function(feature, index, fetarray) {
				if(feature.id==results[i*5]) {
					//console.log(feature.id + "batt" + results[i*5+2]);
					fetarray[index].al=results[i*5+1];
					fetarray[index].bat=results[i*5+2];
					fetarray[index].state=i2s(results[i*5+4]);
					exist = true;
				}
			});
			/*if(!exist) {
				features.push({id: results[i*5],
						lat: features[0].lat,
						lng: features[0].lng,
						bat: results[i*5+2],
						state:	i2s(results[i*5+4]),
						type: 'other',
						marker: null});
			}*/
		}
	});

	listenerTargets = new ROSLIB.Topic({
   		ros : ros,
   		name : NAMESPACE+'/targets_found',
   		messageType : 'rosbuzz/neigh_pos'
   	});

   	listenerTargets.subscribe(function(msgtargets) {
   			var results = msgtargets.pos_neigh;
			for(var i in results) {
				var exist = false;
				if(isEmpty(featuresT)) {
					featuresT = [{
						id: results[i].position_covariance_type,
						lat: results[i].latitude,
						lng: results[i].longitude,
						type: 'other',
						marker: null
					}];
					exist = true;
				} else {
					featuresT.forEach(function(feature, index, fetarray) {
						if(feature.id==results[i].position_covariance_type) {
							fetarray[index].lat=results[i].latitude;
							fetarray[index].lng=results[i].longitude;
							exist = true;
						}
					});
				}
				if(!exist) {
					featuresT.push({id: results[i].position_covariance_type,
							lat: results[i].latitude,
							lng: results[i].longitude,
							type: 'other',
							marker: null});
				}
        	}
			updateNeiGUI();
			
			updateMap();
   	});
    
    cmdService = new ROSLIB.Service({
        ros : ros,
        name : NAMESPACE+'/buzzcmd',
        serviceType : 'mavros_msgs/CommandLong'
    });

	presstopic = new ROSLIB.Topic({
       ros : ros,
       name : '/pressed',
       messageType : 'std_msgs/UInt32'
     });

	//updateTopicsList();

}

/**
 Does actions related to ROS topics/publish
 **/

 function updateNeiGUI(){
	 var abridge = '<table class="alt"><thead><tr><th align="center">Robot ID</th><th align="center">Latitude</th><th align="center">Longitude</th><th align="center">Altitude</th><th align="center">Battery</th><th align="center">State</th></tr></thead><tbody>';

	 features.forEach(function(feature, index, fetarray) {
		 if(feature.id==myid && GS)
		 	abridge = abridge + '<tr><td style="color:#000000">GroundStation</td><td style="color:#000000">' + feature.lat.toFixed(6) + '</td><td style="color:#000000">' + feature.lng.toFixed(6) + '</td><td style="color:#000000">' + feature.al + '</td><td style="color:#000000">' + feature.bat.toFixed(2) + ' %</td><td style="color:#000000">' + feature.state + "</td></tr>";
		 else if(feature.active==0)
		 	abridge = abridge + '<tr><td style="color:#D3D3D3">' + feature.id + '</td><td style="color:#D3D3D3">' + feature.lat.toFixed(6) + '</td><td style="color:#D3D3D3">' + feature.lng.toFixed(6) + '</td><td style="color:#D3D3D3">' + feature.al + '</td><td style="color:#D3D3D3">' + feature.bat.toFixed(2) + ' %</td><td style="color:#D3D3D3">' + feature.state + "</td></tr>";
		 else
		 	abridge = abridge + '<tr><td>' + feature.id + '</td><td>' + feature.lat.toFixed(6) + '</td><td>' + feature.lng.toFixed(6) + '</td><td>' + feature.al + '</td><td>' + feature.bat.toFixed(2) + ' %</td><td>' + feature.state + "</td></tr>";
	 });
	 abridge = abridge + "</tbody></table>";
	document.getElementById('neighbors').innerHTML = abridge;
 }

function i2s(value){
	if(value==0){
	return "CHAIN_FORMATION"
	}
	else if(value==1){
	return "IDLE"
	}
	else if(value==2){
	return "DEPLOY"
	}
	else if(value==3){
	return "STOP"
	}
	else if(value==4){
	return "TURNEDOFF"
	}
	else if(value==5){
	return "BARRIERWAIT"
	}
  else if(value==6){
	return "WAYPOINT"
	}
  else if(value==7){
	return "GOHOME"
	}
  else if(value==8){
	return "LAUNCH"
	}
  else if(value==9){
	return "TASK_ALLOCATE"
	}
  else {
	return "UNDETERMINED"
	}
}

 function populateActionlist(){
	 var Alist = '';
	 if(!isFlying){
		Alist = '<li><a class="button special icon fa-download" onClick="sendCommand(22)">Take Off</a></li>';// <li><a class="button special icon fa-download" onClick="sendCommand(400)">Arm/Disarm</a></li>';
	 }else{
		//Alist = '<li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(21)">Land</a></li> <li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(20)">Home</a></li>';
		Alist = '<li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(20)">Home</a></li> <li><a class="button icon fa-chevron-down scrolly" onClick="sendCommand(21)">Land</a></li>';
	 }
	 document.getElementById('actionlist').innerHTML = Alist;
 }

 function setWP(i) {
	WP = i;
	if(popmenu)
		populateActionlist2();
 }

function dropdown() {
	var e = document.getElementById("stateselect");
	var selection = e.options[e.selectedIndex].value;
	console.log('Send behavior: ' + selection);
	if(selection==1){
		WP = 1
		sendCommand(902)
	}else if(selection==2){
		WP = 0
		sendCommand(901)
	}else if(selection==3){
		WP = -1
		sendCommand(900)
	} else if(selection==4){
		WP = 0
		sendCommand(903)
	}
}

function populateActionlist2(){
	var Alist = '';
	 /*if(!isFlying){
		 Alist = 'Waiting for takeoff';
	}else{*/
		if(WP)
			Alist = '<select id="stateselect" onChange="dropdown()" style="width:130px;display:inline;"><option value="1" selected="selected">Waypoint</option><option value="2">Deploy</option><option value="3">Task allocate</option><option value="4">Chain Formation</option></select>';
		else
			Alist = '<select id="stateselect" onChange="dropdown()" style="width:130px;display:inline;"><option value="1">Waypoint</option><option value="2" selected="selected">Deploy</option><option value="3">Task allocate</option><option value="4">Chain Formation</option></select>';
	 //}
	 document.getElementById('stateselectdiv').innerHTML = Alist;
 }

 function setBehavior(){
	// We fetch the behavior
	var target_behavior = document.getElementById('behaviorslist').value;
	console.log('Send behavior: ' + target_behavior);
 }

 function sendPositionA(lat, lng) {
	 // Here should be various manual formation shapes...
	 centralformation(lat, lng);
 }

 function sendPosition() {
	 sendpositionmsg(features[0].id, parseFloat($( "#X" ).val()), parseFloat($( "#Y" ).val()), 5.0);
	 sendpositionmsg(features[0].id, parseFloat($( "#X" ).val()), parseFloat($( "#Y" ).val()), 5.0);
	 centralformation(parseFloat($( "#X" ).val()), parseFloat($( "#Y" ).val()));
 }

 function sendMarkers() {
	 if(WP){
		if(features[0].state!="WAYPOINT"){	
			sendCommand(902);
			sleep(250);
		}
		Amarkers.eachLayer(function (layer) {
			var position = layer.getLatLng();
			if(parseInt(layer._icon.id) < 50) {
				sleep(210);
				sendpositionmsg(parseInt(layer._icon.id), position.lat, position.lng, 5.0);
				sendpositionmsg(parseInt(layer._icon.id), position.lat, position.lng, 5.0);
			}
		});
	 } else {
		 if(features[0].state!="DEPLOY"){
			//sendCommand(901);	//deploy=901, potential=903
			sleep(250);
	 	 }
		 var i = 0;
		 Amarkers.eachLayer(function (layer) {
			var position = layer.getLatLng();
			sleep(210);
			//console.log('Send WP: ' + (parseInt(layer._icon.id)+i))
			sendpositionmsg(parseInt(layer._icon.id)+i, position.lat, position.lng, 5.0);
			sendpositionmsg(parseInt(layer._icon.id)+i, position.lat, position.lng, 5.0);
			i++;
		});
		i = 0;
		Rmarkers.eachLayer(function (layer) {
			var position = layer.getLatLng();
			sleep(210);
			sendpositionmsg(parseInt(layer._icon.id)+i, position.lat, position.lng, 5.0);
			sendpositionmsg(parseInt(layer._icon.id)+i, position.lat, position.lng, 5.0);
			i++;
		});
	 }
 }

 function centralformation(lat, lng) {
	var formlist = [[lat, lng], gpsfromvec(-3, -4, lat, lng),gpsfromvec(3, -4, lat, lng),gpsfromvec(-5, -8, lat, lng),gpsfromvec(0, -8, lat, lng),gpsfromvec(5, -8, lat, lng)];
	var it = 0;
	 features.forEach(function(feature, index, fetarray) {
		if(feature.id!=myid || !GS) {
	 		sleep(210);
			sendpositionmsg(feature.id, formlist[it][0], formlist[it][1], 5.0);
			sendpositionmsg(feature.id, formlist[it][0], formlist[it][1], 5.0);
			it++;
		}
	 });
 }

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

 function gpsfromvec(dx, dy, lat, lg) {
	var d_lat = (dx / 6371000)*180.0/Math.PI;
	Lgoal_lat = d_lat + lat;
	var d_lon = (dy / (6371000 * Math.cos((Lgoal_lat)*Math.PI/180.0)))*180.0/Math.PI;
	Lgoal_long = d_lon + lg;
	/*var Vrange = Math.sqrt(dx*dx+dy*dy)
	var Vbearing = Math.atan(dy, dx)
	var latR = lat*Math.PI/180.0;
	var lonR = lg*Math.PI/180.0;
	var target_lat = Math.asin(Math.sin(latR) * Math.cos(Vrange/6371000.0) + Math.cos(latR) * Math.sin(Vrange/6371000.0) * Math.cos(Vbearing));
	var target_lon = lonR + Math.atan(Math.sin(Vbearing) * Math.sin(Vrange/6371000.0) * Math.cos(latR), Math.cos(Vrange/6371000.0) - Math.sin(latR) * Math.sin(target_lat));
	Lgoal_lat = target_lat*180.0/Math.PI;
	Lgoal_long = target_lon*180.0/Math.PI;*/

	return [Lgoal_lat, Lgoal_long]
 }

 function sendpositionmsg(id, la, lo, al) {
	 pressed_toROS(id)
	var request = new ROSLIB.ServiceRequest({
 		broadcast: false,
        command: 16,
        confirmation: 0,
        param1: id,
        param2: 0.0,
        param3: 0.0,
        param4: 0.0,
        param5: la,
        param6: lo,
        param7: al
 	});
 	console.log(request);
 	cmdService.callService(request, function(result) {
        console.log('Result for service call on '
        + cmdService.name
        + ': '
        + result.success);
    });
}
 function sendCommand(cmdID) {
	 pressed_toROS(cmdID)
	 var p1 = 0.0;
	 if(cmdID == 400){
		 if(!isArm)
	 		p1 = 1.0;
		 isArm = !isArm;
	 }

	var request = new ROSLIB.ServiceRequest({
		broadcast: false,
		command: cmdID,
		confirmation: 0,
		param1: p1,
		param2: 0.0,
		param3: 0.0,
		param4: 0.0,
		param5: 0.0,
		param6: 0.0,
		param7: 0.0
	});
	console.log(request);
	cmdService.callService(request, function(result) {
		console.log('Result for service call on '
		+ cmdService.name
		+ ': '
		+ result.success);
	});
 }

 function setTargetTopic(){
	// We fetch the message type with is name
	target_topic = document.getElementById('topics').value;
	console.log('Name: ' + target_topic);

	// We unsubscribe from the old topic, if it has been define
	if (typeof listener !== 'undefros.getTopicsined')
		listener.unsubscribe();

	// Subscribe to new topic
	subscribeTargetTopic();
}


function subscribeTargetTopic(){
	ros.getTopicType(target_topic, 
		function(type){
			//console.log(type);
			console.log("Update topic message");
			listener = new ROSLIB.Topic({
				ros : ros,
				name : target_topic,
				messageType : type,
				throttle_rate: TIME_INTERVAL_BETWEEN_FETCH, // Time interval between fecth in ms
				queue_size: 1
			});

			listener.subscribe(function(message) {
				//We check that it's not an image before converting it to text
				if(type.match("/Image$")){
					document.getElementById('topic-out').innerHTML = "Use the image stream on the left for displaying image";
				}
				else{
					var indentedMsgOutput = JSON.stringify(message, null, 4);
					document.getElementById('topic-out').innerHTML = indentedMsgOutput;
				}
			});
		});
}

function updateTopicsList(topics){
	console.log("Refreshing topics")
	var request = new ROSLIB.ServiceRequest({node: NAMESPACE+'/rosbuzz_node'});

	var selectlist = document.getElementById('topics');
	if (selectlist.length > 0) {
		selectlist.remove(selectlist.length-1);
	}
	var option = {};

	topicssrv.callService(request, function(result) {
		console.log(result)
		for (topic in result.publishing) {
			//console.log(topic.desc)
			option = document.createElement("option");
			option.text = result.publishing[topic];
			selectlist.add(option);
		}
		for (topic in result.subscribing) {
			//console.log(topic.desc)
			option = document.createElement("option");
			option.text = result.subscribing[topic];
			selectlist.add(option);
		}
	});
}


function pressed_toROS(but_id) {
	var presmsg = new ROSLIB.Message({
  	  data: but_id
    });
    presstopic.publish(presmsg);
}

/**
 Functions related to Map API
 **/
 var features = [{
					id: myid,
					lat: mylat,
					lng: mylng,
					al: 0,
					bat: 0,
					active: 1,
					state: 'TURNEDOFF',
					type: 'stake',
					marker: null
          		}];
var featuresT = {};
var stakeIcon = L.icon({
				iconUrl: 'images/GS.png',
				//shadowUrl: 'images/uavlogo.png',

				iconSize:     [30, 30], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});
var redIcon = L.icon({
				iconUrl: 'images/uavlogoRedN.png',
				//shadowUrl: 'images/uavlogo2.png',

				iconSize:     [35, 35], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});
var yellowIcon = L.icon({
				iconUrl: 'images/uavlogoYellowN.png',
				//shadowUrl: 'images/uavlogo2.png',

				iconSize:     [35, 35], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});
var greenIcon = L.icon({
				iconUrl: 'images/uavlogoGreenN.png',
				//shadowUrl: 'images/uavlogo2.png',

				iconSize:     [35, 35], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});
var greyIcon = L.icon({
				iconUrl: 'images/uavlogoGreyN.png',
				//shadowUrl: 'images/uavlogo2.png',

				iconSize:     [35, 35], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});
var targetIcon = L.icon({
				iconUrl: 'images/target.png',
				//shadowUrl: 'images/target.png',

				iconSize:     [20, 20], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});
var hereIcon = L.icon({
				iconUrl: 'images/wpblue.png',
				//shadowUrl: 'images/target.png',

				iconSize:     [15, 20], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-13, -6] // point from which the popup should open relative to the iconAnchor
			});
var hereIconY = L.icon({
				iconUrl: 'images/wpyellow.png',
				//shadowUrl: 'images/target.png',

				iconSize:     [15, 20], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-13, -6] // point from which the popup should open relative to the iconAnchor
			});
var dangerIcon = L.icon({
				iconUrl: 'images/danger.png',
				//shadowUrl: 'images/target.png',

				iconSize:     [20, 20], // size of the icon
				//shadowSize:   [5, 5], // size of the shadow
				iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
				//shadowAnchor: [4, 4],  // the same for the shadow
				popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
			});
var mymap = {};

var popup = L.popup();
var WPmarker = {};
var Amarkers = L.layerGroup();
var Rmarkers = L.layerGroup();

function onMapRClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("<p style='text-align:center;'>Target <br/>( " + e.latlng.lat.toFixed(6) + " , " + e.latlng.lng.toFixed(6) + " )</p>")
        .openOn(mymap);
}

function clearMarkers() {
	if(!WP) {
		Rmarkers.clearLayers();
	}
	Amarkers.clearLayers();
	sendCommand(904);
}

function onMapLClick(e) {
	if(!isFlying)
		return;
	if(WP) {
		/*if(isEmpty(WPmarker))
			WPmarker = L.marker(e.latlng, {icon:hereIcon}).addTo(mymap);
		else
			WPmarker.setLatLng(e.latlng);*/
		var container = $('<div />');
		var htmlcode = "";
		features.forEach(function(feature, index, fetarray) {
			if(feature.id!=myid || !GS) {
				container.on('click', '#button_'+feature.id, function() {
					Amarkers.eachLayer(function (layer) {
						if(feature.id==parseInt(layer._icon.id))
							Amarkers.removeLayer(layer);
					});
					Amarker = L.marker(e.latlng, {icon:hereIcon}).on('click',onWPClick).addTo(mymap);
					Amarker._icon.id = feature.id;
					Amarkers.addLayer(Amarker).addTo(mymap);
				});
			htmlcode = htmlcode + "<input id='button_" + feature.id + "' type='button' value='" + feature.id + "' width='20' height='20'/>";
			}
		});
		container.html(htmlcode);
		popup
			.setLatLng(e.latlng)
			.setContent(container[0])
			.openOn(mymap);
		
	} else {
		/*var container = $('<div />');
		container.on('click', '#button_att', function() {*/
			Amarker = L.marker(e.latlng, {icon:hereIconY}).addTo(mymap);
			if(Amarkers.getLayers().length > 1){
				Amarkers.eachLayer(function (layer) {
			 			if(parseInt(layer._icon.id)>50){
							layer._icon.id = parseInt(layer._icon.id) - 1;
						 } else
							Amarkers.removeLayer(layer);
				});
			}
			Amarker._icon.id = 50 + Amarkers.getLayers().length;
			// Amarkers.eachLayer(function (layer) {
			// 			if(50==parseInt(layer._icon.id))
			// 				Amarkers.removeLayer(layer);
			// 		});
			Amarkers.addLayer(Amarker).addTo(mymap);
		/*});
		container.on('click', '#button_rep', function() {
			Rmarker = L.marker(e.latlng, {icon:dangerIcon}).addTo(mymap);
			Amarker._icon.id = 100;
			Rmarkers.addLayer(Rmarker).addTo(mymap);
		});
		container.html("<p align='left' style='display:inline;padding-right:15px'><input id='button_att' type='image' src='images/wp.png' width='20' height='20'/></p>");//<p align='right' style='display:inline'><input id='button_rep' type='image' src='images/danger.png' width='20' height='20'/></p>");
		popup
			.setLatLng(e.latlng)
			.setContent(container[0])
			.openOn(mymap);*/
	}
}

function onFeatureClick(e) {
	/*lay = e.target;
	var container = $('<div/>'); 
	var tempimg = '<div id="dialog" style="display: none;"><img id="image" src=""/></div><div class="myImage"><img src="images/rock_' + lay._icon.id + '.jpg" alt="myimage" width="120" height="90"/></div>';
	// Upon clicking the .myImage Class in the container (which is the thumbnail) Open a jQueryUI Dialog...
	container.on('click', '.myImage', function() { $('#dialog').dialog(
	//...which upon when it's opened...  
	{open: function(){
	///... assign a variable that can acess the div id 'image'...
	imageTag = $('#image'); 
	///... and set the image src in #image (note that it'll be the fullsize this time)...
	imageTag.attr("src","images/rock_" + lay._icon.id + ".jpg");
	imageTag.attr("width","600");
	},closeOnEscape: true});
	});
	container.html(tempimg);
	popup
		.setLatLng(e.latlng)
		.setContent(container[0])
		.openOn(mymap);*/
}

function onDroneClick(e) {
	lay = e.target;
	popup
		.setLatLng(e.latlng)
		.setContent('<p style="color:black;font-size:15px;font-weight:bold;">[' + lay._icon.id + "]: " + features[lay._icon.id-1].bat + "% </p>")
		.openOn(mymap);
}

function onWPClick(e) {
	lay = e.target;
	popup
		.setLatLng(e.latlng)
		.setContent('<p style="color:black;font-size:15px;font-weight:bold;">' + lay._icon.id + "</p>")
		.openOn(mymap);
}

function onGSClick(e) {
	lay = e.target;
	popup
		.setLatLng(e.latlng)
		.setContent("Groundstation")
		.openOn(mymap);
}

function initMap() {

	mymap = L.map('mapid').setView([features[0].lat, features[0].lng], 19);
	//Disable Zoom (CF exp)
	//mymap.touchZoom.disable();
	//mymap.doubleClickZoom.disable();
	//mymap.scrollWheelZoom.disable();
	mymap.boxZoom.disable();
	mymap.keyboard.disable();
	$(".leaflet-control-zoom").css("visibility", "hidden");

	// create the tile layer with correct attribution
	//var osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmUrl='maps/{z}/{x}/{y}.png';
	var osmAttrib='';//Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
	var osm = new L.TileLayer(osmUrl, {minZoom: 14, maxZoom: 21, attribution: osmAttrib});	
	mymap.addLayer(osm);

	// Create markers.
	features.forEach(function(feature, index, fetarray) {
		if(GS)
			fetarray[index].marker = L.marker([feature.lat, feature.lng], {icon: stakeIcon}).on('click', onGSClick).addTo(mymap);
		else
			fetarray[index].marker = L.marker([feature.lat, feature.lng], {icon: greenIcon}).on('click', onDroneClick).addTo(mymap);
		fetarray[index].marker._icon.id = feature.id;
	});

	mymap.on('click', onMapLClick);
	mymap.on('contextmenu',onMapRClick);

	// Convex polygon for geofence reference on the GUI
	// Pangeae final site
	/*var polygon = L.polygon([
		[29.067746, -13.663315],
		[29.068724, -13.662634],
		[29.068113, -13.661427],
		[29.067014, -13.661564]
	],{fillOpacity: 0.0}).addTo(mymap);*/

	// Pangeae first site
	/*var polygon = L.polygon([
		[29.021055, -13.715155],
		[29.021055, -13.714132],
		[29.019470, -13.714132],
		[29.019470, -13.715240]
	],{fillOpacity: 0.0}).addTo(mymap);*/

	// CEPSUM site
	var polygon = L.polygon([
		[45.510400, -73.610421],
		[45.510896, -73.608731],
		[45.510355, -73.608404],
		[45.509840, -73.610072]
	],{fillOpacity: 0.0}).addTo(mymap);
/*	var pl1 = L.polyline([
		[29.021055, -13.715155],
		[29.021055, -13.71565]], {
		color: 'blue',
		weight: 2,
		smoothFactor: 1
	}).addTo(mymap);
	var pl2 = L.polyline([
		[29.021055, -13.71565],
		[29.02074, -13.71565]], {
		color: 'red',
		weight: 5,
		smoothFactor: 1
	}).addTo(mymap);
	var pl3 = L.polyline([
		[29.02074, -13.71565],
		[29.02074, -13.715155]], {
		color: 'blue',
		weight: 2,
		smoothFactor: 1
	}).addTo(mymap);
	var pl4 = L.polyline([
		[29.02074, -13.715155],
		[29.021055, -13.715155]], {
		color: 'green',
		weight: 5,
		smoothFactor: 1
	}).addTo(mymap);*/
}

function updateMap() {
	// Create markers.
	//console.log(features.length)
	var mlat = 0, mlg = 0;
	features.forEach(function(feature, index, fetarray) {
		if(feature.marker!=null) {
			fetarray[index].marker.setLatLng([feature.lat, feature.lng]);
			if(feature.id!=myid || !GS) {
				if(!feature.active)
					fetarray[index].marker.setIcon(greyIcon);
				else if(feature.bat > 40)
					fetarray[index].marker.setIcon(greenIcon);
				else if(feature.bat > 20)
					fetarray[index].marker.setIcon(yellowIcon);
				else
					fetarray[index].marker.setIcon(redIcon);
			}
		} else {
			fetarray[index].marker = L.marker([feature.lat, feature.lng], {icon: greenIcon}).on('click', onDroneClick).addTo(mymap);
			fetarray[index].marker._icon.id = feature.id;
		}
		if(mlat==0) {
			mlat = feature.lat;
			mlg = feature.lg;
		} else {
			mlat = mlat + feature.lat;
			mlg = mlg + feature.lg;
		}
	});

	if(!isEmpty(featuresT)) {
		featuresT.forEach(function(feature, index, fetarray) {
			if(feature.marker!=null) {
				fetarray[index].marker.setLatLng([feature.lat, feature.lng]);
				fetarray[index].marker._icon.id = index;
			} else {
				fetarray[index].marker = L.marker([feature.lat, feature.lng], {icon: targetIcon}).on('click', onFeatureClick).addTo(mymap);
			}
		});
	}

	mlat = mlat / features.length;
	mlg = mlg / features.length;
	//mymap.setView([features[0].lat, features[0].lng], 18);
}

function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
