import math
import requests
import os

class GoogleMapDownloader:

    def __init__(self, lat, lng, zoom=12):
        self._lat = lat
        self._lng = lng
        self._zoom = zoom

    def getXY(self):

        tile_size = 256

        # Use a left shift to get the power of 2
        # i.e. a zoom level of 2 will have 2^2 = 4 tiles
        numTiles = 1 << self._zoom

        # Find the x_point given the longitude
        point_x = (tile_size/ 2 + self._lng * tile_size / 360.0) * numTiles // tile_size

        # Convert the latitude to radians and take the sine
        sin_y = math.sin(self._lat * (math.pi / 180.0))

        # Calulate the y coorindate
        point_y = ((tile_size / 2) + 0.5 * math.log((1+sin_y)/(1-sin_y)) * -(tile_size / (2 * math.pi))) * numTiles // tile_size

        return int(point_x), int(point_y)

    def readimg(self, url):
        import urllib.request
        with urllib.request.urlopen(url) as response:
            return response.read()

    def gettiles(self):
        IMGFOLDER = ''

        for z in range(14, 22):
            self._zoom = z
            start_x, start_y = self.getXY()
            for x in range(-6, 6):
                for y in range(-6, 6) :
                    imgurl = 'https://mt1.google.com/vt/lyrs=y&x='+str(start_x+x)+'&y='+str(start_y+y)+'&z='+str(self._zoom)
#                imgurl = 'https://mt0.google.com/vt?x='+str(start_x+x)+'&y='+str(start_y+y)+'&z='+str(self._zoom)
                    print(imgurl)
                    current_tile = str(start_y+y)+'.png'
                    IMGFOLDER = 'maps/' + str(self._zoom) + '/' + str(start_x+x) + '/'
                    if not os.path.exists(IMGFOLDER):
                        os.makedirs(IMGFOLDER)
                    with open(IMGFOLDER + current_tile, 'wb') as f:
                        f.write(self.readimg(imgurl))
#                    f.write(requests.get(imgurl).content)

def main():
    # Create a new instance of GoogleMap Downloader
    gmd = GoogleMapDownloader(29.020971, -13.714859, 15)

    print("The tile coordinates are {}".format(gmd.getXY()))

    gmd.gettiles()

if __name__ == '__main__':  main()
