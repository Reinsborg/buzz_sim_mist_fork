## ROSBuzz Web Control Interface

Inspired from Fractal by HTML5 UP html5up.net | @ajlkn.

# Installation
You need first to have a fully working ROS/Gazebo/Buzz environment. Since this will be used as a webpage server, you need to install `apache2` and either redirect apache server to where you cloned this repository *or* copy these files under /var/www/html/. We will detail here the first option.
First install apache2 (on ubuntu):
```
sudo apt-get install apache2
```
To redirect Apache2 DocumentRoot, edit the file `001-rosbuzz.conf` in this repository with the location of your `webcontrol` folder and copy `001-rosbuzz.conf` to your Apache2 system files: `/etc/apache2/sites-available/` and `/etc/apache2/sites-enabled/`. You may also need to edit `/etc/apache2/sites-available/ssl-default.conf` variable `DocumentRoot` and change the default server location `/etc/apache2/sites-enabled/000-default.conf` (either remove the file or change the :80 port).

To protect the fleet control we use `webdav` (https://www.techrepublic.com/article/how-to-enable-webdav-on-your-ubuntu-server/):
```
sudo a2enmod dav
sudo a2enmod dav_fs
sudo service apache2 restart
sudo mkdir -p /var/www/webdav
sudo htpasswd -c /var/www/webdav/passwd.dav USER
sudo chown root:www-data /var/www/webdav/passwd.dav
sudo chmod 640 /var/www/webdav/passwd.dav
sudo service apache2 restart
```
If not installed already, you will also need to install rosbridge:
```
sudo apt-get install ros-melodic-rosbridge-server
```
and to launch it:
```
roslaunch rosbridge_server rosbridge_websocket.launch
```
or add it to your launch file:
```
<include file="$(find rosbridge_server)/launch/rosbridge_websocket.launch" > 
 <arg name="port" value="9090"/>
</include>
```
or use the [groundstation launch file](https://git.mistlab.ca/dasto/drones/blob/ros_drones_ws/src/mistlab_rosbuzz_sdk/launch_robot/groundstation.launch).

# Usage
To connect to the interface, just open your favorite browser (tested on with Chrome and Chromium) to `http://localhost` or use your computer network IP, but make sure to use the same IP has defined by your ROS environment variable `ROS_IP` (usually in `~/.bashrc`).

Do not forget to manually set the variables in `assets/js/rosbuzz.js`:
- var NAMESPACE = 'robot1'; // Empty if using groundstation, mavros if running on Spiri or Solo.
- var myid = 1;	// Network id of the drone or groundstation running this webcontrol
- var GS = 0;	// 0 = not using a groundstation - directly connected to a drone apache server
- var mylat = 29.020971 and var mylng = -13.714859 //Approximate center of the flying area to speed up map load

If you are using the [groundstation launch file](https://git.mistlab.ca/dasto/drones/blob/ros_drones_ws/src/mistlab_rosbuzz_sdk/launch_robot/groundstation.launch), change the default GPS value of the groundstation in this file too.

To help using the interface we prepared demo videos
- the expert mode with script editor (index_expert.html): https://youtu.be/oY1XP5C_D4Q
- the simplified mode (used in PANGAEA-X 2018 traning): 

If you get a lot of errors related to yield of a None value by the webhandler, apply this patch:
https://github.com/RobotWebTools/rosbridge_suite/pull/395/files#diff-97b232078347d549c4e1c3d4d1adc7d5R42
