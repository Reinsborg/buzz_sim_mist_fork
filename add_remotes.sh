git config http.sslVerify false

git remote add xbeemav https://github.com/MISTLab/XbeeMav.git
#git remote add rosbuzz https://github.com/MISTLab/ROSBuzz.git
git remote add rosbuzz http://git.mistlab.ca/dasto/ROSBuzz.git
git remote add dji_ros_sdk https://github.com/dji-sdk/Onboard-SDK-ROS.git
git remote add dji_sdk_mistlab http://git.mistlab.ca/dasto/dji_mav_mist.git
git remote add comm_hub http://git.mistlab.ca/vvaradharajan/ROS_com_hub.git
git remote add hector_localization https://github.com/tu-darmstadt-ros-pkg/hector_localization.git
git remote add hector https://github.com/tu-darmstadt-ros-pkg/hector_quadrotor.git
